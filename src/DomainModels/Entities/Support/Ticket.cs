using DomainModels.Entities.HumanResource;
using DomainModels.Entities.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace DomainModels.Entities.Support 
{
	[Table("Ticket", Schema = "Support")]
	public class Ticket : BaseEntity 
	{
        [Column(Order = 6)]
        [DisplayName("Subject")]
        [Required(ErrorMessage = "{0} is required")]
        [StringLength(256, ErrorMessage = "Maximum length of {0} is {1}")]
        public virtual string Subject { get; set; }


        [Column(Order = 8)]
		[DisplayName("Message")]
		[Required(ErrorMessage= "{0} is required")]
		[StringLength(2048, ErrorMessage="Maximum length of {0} is {1}")]
		public virtual string Message { get; set; }

		[Column(Order = 12)]
		[DisplayName("Is open ?")]
		[Required(ErrorMessage= "{0} is required")]
		public virtual bool IsOpen { get; set; }

        [Column(Order = 14)]
        public virtual Guid RowGuid { get; set; }

        [Column(Order = 16)]
		[DisplayName("Created on")]
		[Required(ErrorMessage= "{0} is required")]
		public virtual DateTime CreatedOn { get; set; }


		#region Navigation Properties

		// ----- User
		[Column(Order = 504)]
		public virtual long SenderId { get; set; }
		[ForeignKey("SenderId")]
		public virtual User Sender { get; set; }


        // ----- TicketReply
        public virtual ICollection<TicketReply> TicketReply { get; set; }


		#endregion
	}
}