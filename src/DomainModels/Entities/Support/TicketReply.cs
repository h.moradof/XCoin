using DomainModels.Entities.HumanResource;
using DomainModels.Entities.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace DomainModels.Entities.Support 
{
	[Table("TicketReply", Schema = "Support")]
	public class TicketReply : BaseEntity 
	{

		[Column(Order = 8)]
		[DisplayName("Message")]
		[Required(ErrorMessage= "{0} is required")]
		[StringLength(2048, ErrorMessage="Maximum length of {0} is {1}")]
		public virtual string Message { get; set; }


        [Column(Order = 10)]
        public virtual bool SenderIsAdmin { get; set; }

        [Column(Order = 12)]
		[DisplayName("Created on")]
		[Required(ErrorMessage= "{0} is required")]
		public virtual DateTime CreatedOn { get; set; }


		#region Navigation Properties

		// ----- User
		[Column(Order = 504)]
		public virtual long SenderId { get; set; }
		[ForeignKey("SenderId")]
		public virtual User Sender { get; set; }


		// ----- Ticket
		[Column(Order = 508)]
		public virtual long TicketId { get; set; }
		[ForeignKey("TicketId")]
		public virtual Ticket Ticket { get; set; }


		#endregion
	}
}