using DomainModels.Entities.Base;
using DomainModels.Entities.Culture;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace DomainModels.Entities.Setting 
{
	[Table("SeoSetting", Schema = "Setting")]
	public class SeoSetting : BaseEntity 
	{


		[Column(Order = 8)]
		[DisplayName("Title")]
		[Required(ErrorMessage= "{0} is required")]
		[StringLength(128, ErrorMessage="Maximum length of {0} is {1}")]
		public virtual string Title { get; set; }

		[Column(Order = 12)]
		[DisplayName("Meta keywords")]
		[Required(ErrorMessage= "{0} is required")]
		[StringLength(256, ErrorMessage="Maximum length of {0} is {1}")]
		public virtual string MetaKeywords { get; set; }

		[Column(Order = 16)]
		[DisplayName("Meta description")]
		[Required(ErrorMessage= "{0} is required")]
		[StringLength(256, ErrorMessage="Maximum length of {0} is {1}")]
		public virtual string MetaDescription { get; set; }


        #region Navigation Properties

        // ----- Language
        [Column(Order = 1000)]
        public virtual long LanguageId { get; set; }
        [ForeignKey("LanguageId")]
        public virtual Language Language { get; set; }

        #endregion
    }
}