﻿namespace DomainModels.Entities.Base
{
    public enum AcceptStatusName : byte
    {
        Unknown = 1,
        Pending,
        Accepted,
        Declined
    }
}
