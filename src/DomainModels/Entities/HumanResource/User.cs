using DomainModels.Entities.Trade;
using DomainModels.Entities.Support;
using DomainModels.Entities.Access;
using DomainModels.Entities.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace DomainModels.Entities.HumanResource 
{
	[Table("User", Schema = "HumanResource")]
	public class User : BaseEntity
	{

        [Column(Order = 2)]
        [DisplayName("First name")]
        [Required(ErrorMessage = "{0} is required")]
        [StringLength(128, ErrorMessage = "Maximum length of {0} is {1}")]
        public virtual string FirstName { get; set; }


        [Column(Order = 3)]
        [DisplayName("Last name")]
        [Required(ErrorMessage = "{0} is required")]
        [StringLength(128, ErrorMessage = "Maximum length of {0} is {1}")]
        public virtual string LastName { get; set; }


        [Column(Order = 4)]
        [DisplayName("Fullname")]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public string FullName
        {
            get { return FirstName + " " + LastName; }
            private set
            {
                //Just need this here to trick EF
            }
        }


        [Column(Order = 5)]
        [DisplayName("Username")]
        [Required(ErrorMessage = "{0} is required")]
        [StringLength(128, ErrorMessage = "Maximum length of {0} is {1}")]
        [RegularExpression(@"^[0-9a-z]+$", ErrorMessage = "Just lower letters(a-z) and numbers(0-9) are valid")] // just number and 
        [Index(IsUnique = true)]
        public virtual string Username { get; set; }


        [Column(Order = 6)]
        [DisplayName("Password")]
        [Required(ErrorMessage = "{0} is required")]
        [StringLength(2048, ErrorMessage = "Maximum length of {0} is {1}")]
        [DataType(DataType.Password)]
        public virtual string Password { get; set; }


        [Column(Order = 7)]
        [DisplayName("Account status")]
        [Required(ErrorMessage = "{0} is required")]
        public virtual AccountStatus AccountStatus { get; set; }

        [Column(Order = 8)]
        [DisplayName("Ban reason")]
        [StringLength(256, ErrorMessage = "Maximum length of {0} is {1}")]
        public virtual string BanReason { get; set; }

        [Column(Order = 9)]
        [ScaffoldColumn(false)]
        public virtual Guid ResetPasswordCode { get; set; }


        [Column(Order = 10)]
        [ScaffoldColumn(false)]
        public virtual Guid RowGuid { get; set; }

        [Column(Order = 20)]
		[DisplayName("Mobile number")]
		[StringLength(128, ErrorMessage="Maximum length of {0} is {1}")]
		public virtual string MobileNumber { get; set; }


		[Column(Order = 24)]
		[DisplayName("Email address")]
		[Required(ErrorMessage= "{0} is required")]
		[StringLength(128, ErrorMessage="Maximum length of {0} is {1}")]
		[EmailAddress(ErrorMessage = "Email address format is not valid")]
        [Index(IsUnique = true)]
		public virtual string EmailAddress { get; set; }

		
		[Column(Order = 40)]
		[DisplayName("Rating")]
		[Required(ErrorMessage= "{0} is required")]
		public virtual decimal Rating { get; set; }
        

		[Column(Order = 52)]
		[DisplayName("Google Authenticator key")]
		[Required(ErrorMessage= "{0} is required")]
		[StringLength(64, ErrorMessage="Maximum length of {0} is {1}")]
		public virtual string GoogleAuthenticatorKey { get; set; }


		[Column(Order = 56)]
		[DisplayName("Identification card photo")]
		public virtual string IdentificationCardPhoto { get; set; } // it's bas64 photo content


        [Column(Order = 60)]
		[DisplayName("bank card photo")]
		public virtual string BankCardPhoto { get; set; } // it's bas64 photo content
        

        [Column(Order = 1000)]
        [DisplayName("Created on")]
        [Required(ErrorMessage = "{0} is required")]
        public virtual DateTime CreatedOn { get; set; }


        #region Navigation Properties

        // ----- Accept Status
        [Column(Order = 500)]
        [DisplayName("Identification card accept status")]
        public virtual long IdentificationCardAcceptStatusId { get; set; }
        [ForeignKey("IdentificationCardAcceptStatusId")]
        public virtual AcceptStatus IdentificationCardAcceptStatus { get; set; }

        [Column(Order = 501)]
        [DisplayName("Bank card accept status")]
        [Required(ErrorMessage = "{0} is required")]
        public virtual long BankCardAcceptStatusId { get; set; }
        [ForeignKey("BankCardAcceptStatusId")]
        public virtual AcceptStatus BankCardAcceptStatus { get; set; }

        // ----- Role
        [Column(Order = 504)]
        public virtual long RoleId { get; set; }
        [ForeignKey("RoleId")]
        public virtual Role Role { get; set; }


        // ----- Deal
        [InverseProperty("Saler")]
        public virtual ICollection<Deal> SaleDeals { get; set; }


        // ----- Deal
        [InverseProperty("Buyer")]
        public virtual ICollection<Deal> BuyDeals { get; set; }


		// ----- Ads
		public virtual ICollection<Ads> Adses { get; set; }


		// ----- Offer
		public virtual ICollection<Offer> Offers { get; set; }


		// ----- GoogleAuthenticatorLog
		public virtual ICollection<GoogleAuthenticatorLog> GoogleAuthenticatorLogs { get; set; }


		// ----- Chat
		public virtual ICollection<Chat> Chats { get; set; }


		// ----- Ticket
		public virtual ICollection<Ticket> Tickets { get; set; }


		// ----- TicketReply
		public virtual ICollection<TicketReply> TicketReplies { get; set; }


		// ----- AccountChargeTransaction
		public virtual ICollection<AccountChargeTransaction> AccountChargeTransactions { get; set; }


		// ----- UserBallance
		public virtual ICollection<UserBalance> UserBallances { get; set; }


		#endregion

	}
}