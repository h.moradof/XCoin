using DomainModels.Entities.Base;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace DomainModels.Entities.HumanResource 
{
	[Table("GoogleAuthenticatorLog", Schema = "HumanResource")]
	public class GoogleAuthenticatorLog : BaseEntity 
	{
        [Column(Order = 9)]
        [DisplayName("Temporary password")]
        [Required(ErrorMessage = "{0} is required")]
        [StringLength(32, ErrorMessage = "Maximum length of {0} is {1}")]
        public virtual string TemporaryPassword { get; set; }

        [Column(Order = 1000)]
		[DisplayName("Created on")]
		[Required(ErrorMessage= "{0} is required")]
		public virtual DateTime CreatedOn { get; set; }


		#region Navigation Properties

		// ----- User
		[Column(Order = 504)]
		public virtual long UserId { get; set; }
		[ForeignKey("UserId")]
		public virtual User User { get; set; }


		#endregion
	}
}