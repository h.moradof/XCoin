﻿using DomainModels.Entities.Base;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DomainModels.Entities.HumanResource
{
    [Table("AcceptStatus", Schema = "HumanResource")]
    public class AcceptStatus : BaseEntity
    {
        [Column(Order = 2)]
        public virtual AcceptStatusName Name { get; set; }


        [Column(Order = 3)]
        [DisplayName("En display name")]
        [Required(ErrorMessage = "{0} is required")]
        [StringLength(128, ErrorMessage = "Maximum length of {0} is {1}")]
        public virtual string DisplayName { get; set; }


        [Column(Order = 4)]
        [DisplayName("Chinese display name")]
        [Required(ErrorMessage = "{0} is required")]
        [StringLength(128, ErrorMessage = "Maximum length of {0} is {1}")]
        public virtual string ZhDisplayName { get; set; }


        [Column(Order = 10, TypeName = "varchar")]
        [Required(ErrorMessage = "{0} is required")]
        [StringLength(128, ErrorMessage = "Maximum length of {0} is {1}")]
        [Index(IsUnique = true)]
        [ScaffoldColumn(false)]
        public virtual string IdentityName { get; set; } // used for procedures
    }
}
