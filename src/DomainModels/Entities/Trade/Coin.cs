using DomainModels.Entities.Base;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace DomainModels.Entities.Trade 
{
	[Table("Coin", Schema = "Trade")]
	public class Coin : BaseEntity
    {

        [Column(Order = 2)]
        public virtual CoinName Name { get; set; }


        [Column(Order = 3)]
        [Display(Name = "Coin_Name", ResourceType = typeof(WebResources.Prop))]
        [Required(ErrorMessage = "{0} is required")]
        [StringLength(128, ErrorMessage = "Maximum length of {0} is {1}")]
        public virtual string DisplayName { get; set; }
        

        [Column(Order = 10, TypeName = "varchar")]
        [Required(ErrorMessage = "{0} is required")]
        [StringLength(128, ErrorMessage = "Maximum length of {0} is {1}")]
        [Index(IsUnique = true)]
        [ScaffoldColumn(false)]
        public virtual string IdentityName { get; set; } // used for procedures


        [Column(Order = 20)]
		[DisplayName("Fee percent")]
		[Required(ErrorMessage= "{0} is required")]
		public virtual decimal FeePercent { get; set; }


        [Column(Order = 24, TypeName = "varchar")]
        [DisplayName("Slug")]
        [Required(ErrorMessage = "{0} is required")]
        [StringLength(128, ErrorMessage = "Maximum length of {0} is {1}")]
        public virtual string Slug { get; set; }


        #region Navigation Properties

        // ----- Deal
        public virtual ICollection<Deal> Deal { get; set; }


		// ----- Ads
		public virtual ICollection<Ads> Ads { get; set; }


		// ----- AccountChargeTransaction
		public virtual ICollection<AccountChargeTransaction> AccountChargeTransaction { get; set; }


		// ----- UserBallance
		public virtual ICollection<UserBalance> UserBallance { get; set; }


		#endregion
	}
}