using DomainModels.Entities.HumanResource;
using DomainModels.Entities.Base;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace DomainModels.Entities.Trade 
{
	[Table("Offer", Schema = "Trade")]
	public class Offer : BaseEntity 
	{

		[Column(Order = 8)]
		[DisplayName("Amount")]
		[Required(ErrorMessage= "{0} is required")]
		public virtual decimal Amount { get; set; }

		[Column(Order = 12)]
		[DisplayName("Created on")]
		[Required(ErrorMessage= "{0} is required")]
		public virtual DateTime CreatedOn { get; set; }


        #region Navigation Properties

        // ----- Ads
        [Column(Order = 504)]
		public virtual long AdsId { get; set; }
		[ForeignKey("AdsId")]
		public virtual Ads Ads { get; set; }


		// ----- User
		[Column(Order = 508)]
		public virtual long UserId { get; set; } // a person who register offer
		[ForeignKey("UserId")]
		public virtual User User { get; set; }


        #endregion

    }
}