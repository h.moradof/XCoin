using DomainModels.Entities.Base;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace DomainModels.Entities.Trade 
{
	[Table("DealStatus", Schema = "Trade")]
	public class DealStatus : BaseEntity
    {
        [Column(Order = 2)]
        public virtual DealStatusName Name { get; set; }


        [Column(Order = 3)]
        [Display(Name = "DealStatus_Name", ResourceType = typeof(WebResources.Prop))]
        [Required(ErrorMessage = "{0} is required")]
        [StringLength(128, ErrorMessage = "Maximum length of {0} is {1}")]
        public virtual string DisplayName { get; set; }


        [Column(Order = 4)]
        [Display(Name = "DealStatus_Name", ResourceType = typeof(WebResources.Prop))]
        [Required(ErrorMessage = "{0} is required")]
        [StringLength(128, ErrorMessage = "Maximum length of {0} is {1}")]
        public virtual string ZhDisplayName { get; set; }


        [Column(Order = 10, TypeName = "varchar")]
        [Required(ErrorMessage = "{0} is required")]
        [StringLength(128, ErrorMessage = "Maximum length of {0} is {1}")]
        [Index(IsUnique = true)]
        [ScaffoldColumn(false)]
        public virtual string IdentityName { get; set; } // used for procedures


        #region Navigation Properties

        // ----- Deal
        public virtual ICollection<Deal> Deal { get; set; }


		#endregion
	}
}