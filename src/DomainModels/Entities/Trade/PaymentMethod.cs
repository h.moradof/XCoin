using DomainModels.Entities.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace DomainModels.Entities.Trade 
{
	[Table("PaymentMethod", Schema = "Trade")]
	public class PaymentMethod : BaseEntity 
	{

		[Column(Order = 8)]
		[Display(Name = "PaymentMethod_Name", ResourceType = typeof(WebResources.Prop))]
		[StringLength(128, ErrorMessage="Maximum length of {0} is {1}")]
		public virtual string EnName { get; set; }


        [Column(Order = 12)]
        [Display(Name = "PaymentMethod_Name", ResourceType = typeof(WebResources.Prop))]
        [StringLength(128, ErrorMessage = "Maximum length of {0} is {1}")]
        public virtual string ZhName { get; set; }


        [Column(Order = 4, TypeName = "varchar")]
        [DisplayName("Slug")]
        [Required(ErrorMessage = "{0} is required")]
        [StringLength(128, ErrorMessage = "Maximum length of {0} is {1}")]
        public virtual string Slug { get; set; }



        #region Navigation Properties

        // ----- Ads
        public virtual ICollection<Ads> Ads { get; set; }


        // ----- Deal
        public virtual ICollection<Deal> Deals { get; set; }

        #endregion

    }
}