﻿namespace DomainModels.Entities.Trade
{
    public enum DealStatusName : byte
    {
        Doing = 1,
        Done,
        Canceled,
        Judge
    }
}
