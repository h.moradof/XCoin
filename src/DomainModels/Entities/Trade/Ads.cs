using DomainModels.Entities.HumanResource;
using DomainModels.Entities.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace DomainModels.Entities.Trade 
{
	[Table("Ads", Schema = "Trade")]
	public class Ads : BaseEntity 
	{

		[Column(Order = 8)]
		[DisplayName("Minimum currency price")]
		[Required(ErrorMessage= "{0} is required")]
		public virtual decimal MinimumCurrencyPrice { get; set; }

		[Column(Order = 12)]
		[DisplayName("Maximum currency price")]
		[Required(ErrorMessage= "{0} is required")]
		public virtual decimal MaximumCurrencyPrice { get; set; }

		[Column(Order = 16)]
		[DisplayName("Currency unit price of coin")]
		[Required(ErrorMessage= "{0} is required")]
		public virtual decimal CurrencyUnitPriceOfCoin { get; set; }

		[Column(Order = 20)]
		[DisplayName("Description")]
		[StringLength(512, ErrorMessage="Maximum length of {0} is {1}")]
		public virtual string Description { get; set; }

		[Column(Order = 24)]
		[DisplayName("Is sell ?")]
		[Required(ErrorMessage= "{0} is required")]
		public virtual bool IsSell { get; set; }

		[Column(Order = 28)]
		[DisplayName("Is active ?")]
		[Required(ErrorMessage= "{0} is required")]
		public virtual bool IsActive { get; set; }

		[Column(Order = 32)]
		[DisplayName("Created on")]
		[Required(ErrorMessage= "{0} is required")]
		public virtual DateTime CreatedOn { get; set; }


		#region Navigation Properties

		// ----- Coin
		[Column(Order = 504)]
		public virtual long CoinId { get; set; }
		[ForeignKey("CoinId")]
		public virtual Coin Coin { get; set; }


		// ----- Currency
		[Column(Order = 508)]
		public virtual long CurrencyId { get; set; }
		[ForeignKey("CurrencyId")]
		public virtual Currency Currency { get; set; }


		// ----- Country
		[Column(Order = 512)]
		public virtual long CountryId { get; set; }
		[ForeignKey("CountryId")]
		public virtual Country Country { get; set; }


		// ----- PaymentMethod
		[Column(Order = 516)]
		public virtual long PaymentMethodId { get; set; }
		[ForeignKey("PaymentMethodId")]
		public virtual PaymentMethod PaymentMethod { get; set; }


		// ----- User
		[Column(Order = 520)]
		public virtual long UserId { get; set; }
		[ForeignKey("UserId")]
		public virtual User User { get; set; }


		// ----- Offer
		public virtual ICollection<Offer> Offer { get; set; }


		#endregion

	}
}