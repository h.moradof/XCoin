using DomainModels.Entities.HumanResource;
using DomainModels.Entities.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace DomainModels.Entities.Trade 
{
	[Table("Deal", Schema = "Trade")]
	public class Deal : BaseEntity 
	{

        [Column(Order = 8)]
        [DisplayName("Fee")]
        [Required(ErrorMessage = "{0} is required")]
        public virtual decimal FeePercent { get; set; }

        [Column(Order = 12)]
        [DisplayName("Fee amount")]
        [Required(ErrorMessage = "{0} is required")]
        public virtual decimal FeeAmount { get; set; }

        [Column(Order = 16)]
		[DisplayName("Coin amount")]
		[Required(ErrorMessage= "{0} is required")]
		public virtual decimal CoinAmount { get; set; }

		[Column(Order = 20)]
		[DisplayName("Unit price")]
		[Required(ErrorMessage= "{0} is required")]
		public virtual decimal CurrencyUnitPriceOfCoin { get; set; }

		[Column(Order = 22)]
		[DisplayName("You must to pay")]
		[Required(ErrorMessage= "{0} is required")]
		public virtual decimal TotalCurrencyPrice { get; set; }

        [Column(Order = 24)]
        [DisplayName("You'll get")]
        [Required(ErrorMessage = "{0} is required")]
        public virtual decimal BuyerReceivedAmount { get; set; }

        [Column(Order = 26)]
        public virtual Guid? JudgeTicketRowGuid { get; set; }

        #region phase 2
        //[Column(Order = 20)]
        //[DisplayName("Buyer rating")]
        //public virtual byte? BuyerRating { get; set; }

        //[Column(Order = 24)]
        //[DisplayName("Saler rating")]
        //public virtual byte? SalerRating { get; set; }

        //[Column(Order = 28)]
        //[DisplayName("Buyer rating comment")]
        //[StringLength(512, ErrorMessage = "Maximum length of {0} is {1}")]
        //public virtual string BuyerRatingComment { get; set; }

        //[Column(Order = 32)]
        //[DisplayName("Saler rating comment")]
        //[StringLength(512, ErrorMessage = "Maximum length of {0} is {1}")]
        //public virtual string SalerRatingComment { get; set; }
        #endregion


        [Column(Order = 170)]
        public virtual bool IsBuyerPaidCurrency { get; set; }

        [Column(Order = 172)]
        public virtual DateTime? PaidDate { get; set; }

        [Column(Order = 175)]
        public virtual DateTime? ReleasedDate { get; set; }

        [Column(Order = 180)]
        public virtual bool IsSalerReleasedCoin { get; set; }


        [Column(Order = 190)]
        public virtual Guid RowGuid { get; set; }

        [Column(Order = 200)]
		[DisplayName("Created on")]
		[Required(ErrorMessage= "{0} is required")]
		public virtual DateTime CreatedOn { get; set; }


		#region Navigation Properties

		// ----- Coin
		[Column(Order = 504)]
		public virtual long CoinId { get; set; }
		[ForeignKey("CoinId")]
		public virtual Coin Coin { get; set; }


        // ----- Currency
        [Column(Order = 508)]
        public virtual long CurrencyId { get; set; }
        [ForeignKey("CurrencyId")]
        public virtual Currency Currency { get; set; }


        // ----- Country
        [Column(Order = 512)]
        public virtual long CountryId { get; set; }
        [ForeignKey("CountryId")]
        public virtual Country Country { get; set; }


        // ----- PaymentMethod
        [Column(Order = 516)]
        public virtual long PaymentMethodId { get; set; }
        [ForeignKey("PaymentMethodId")]
        public virtual PaymentMethod PaymentMethod { get; set; }


        // ----- User (saler)
        [Column(Order = 520)]
		public virtual long SalerId { get; set; }
		[ForeignKey("SalerId")]
		public virtual User Saler { get; set; }


        // ----- User (buyer)
        [Column(Order = 524)]
		public virtual long BuyerId { get; set; }
		[ForeignKey("BuyerId")]
		public virtual User Buyer { get; set; }


		// ----- DealStatus
		[Column(Order = 532)]
		public virtual long DealStatusId { get; set; }
		[ForeignKey("DealStatusId")]
		public virtual DealStatus DealStatus { get; set; }


		// ----- Chat
		public virtual ICollection<Chat> Chat { get; set; }


		// ----- DealTransaction
		public virtual ICollection<DealTransaction> DealTransaction { get; set; }


        #endregion



    }
}