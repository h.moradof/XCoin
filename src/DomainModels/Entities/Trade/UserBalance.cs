using DomainModels.Entities.HumanResource;
using DomainModels.Entities.Base;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace DomainModels.Entities.Trade 
{
	[Table("UserBalance", Schema = "Trade")]
	public class UserBalance : BaseEntity 
	{
        [Column(Order = 8)]
		[DisplayName("Amount")]
		[Required(ErrorMessage= "{0} is required")]
		public virtual decimal Amount { get; set; }


		#region Navigation Properties

		// ----- User
		[Column(Order = 504)]
		public virtual long UserId { get; set; }
		[ForeignKey("UserId")]
		public virtual User User { get; set; }


		// ----- Coin
		[Column(Order = 508)]
		public virtual long CoinId { get; set; }
		[ForeignKey("CoinId")]
		public virtual Coin Coin { get; set; }


		#endregion
	}
}