using DomainModels.Entities.HumanResource;
using DomainModels.Entities.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace DomainModels.Entities.Trade 
{
	[Table("Chat", Schema = "Trade")]
	public class Chat : BaseEntity 
	{

		[Column(Order = 8)]
		[DisplayName("Message")]
		[Required(ErrorMessage= "{0} is required")]
		[StringLength(1024, ErrorMessage="Maximum length of {0} is {1}")]
		public virtual string Message { get; set; }


        [Column(Order = 10, TypeName = "varchar")]
        [DisplayName("Attachment File")]
        [StringLength(128, ErrorMessage = "Maximum length of {0} is {1}")]
        public virtual string AttachmentFileName{ get; set; }


        [Column(Order = 12)]
		[DisplayName("Created on")]
		[Required(ErrorMessage= "{0} is required")]
		public virtual DateTime CreatedOn { get; set; }


		#region Navigation Properties

		// ----- Deal
		[Column(Order = 504)]
		public virtual long DealId { get; set; }
		[ForeignKey("DealId")]
		public virtual Deal Deal { get; set; }


		// ----- User
		[Column(Order = 508)]
		public virtual long SenderId { get; set; }
		[ForeignKey("SenderId")]
		public virtual User User { get; set; }


		#endregion

	}
}