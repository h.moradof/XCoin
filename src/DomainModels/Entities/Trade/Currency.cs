using DomainModels.Entities.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace DomainModels.Entities.Trade 
{
	[Table("Currency", Schema = "Trade")]
	public class Currency : BaseEntity 
	{


		[Column(Order = 8)]
		[Display(Name = "Currency_Name", ResourceType = typeof(WebResources.Prop))]
		[Required(ErrorMessage= "{0} is required")]
		[StringLength(128, ErrorMessage="Maximum length of {0} is {1}")]
		public virtual string Name { get; set; }

		[Column(Order = 12)]
		[DisplayName("Symbol")]
		[Required(ErrorMessage= "{0} is required")]
		[StringLength(8, ErrorMessage="Maximum length of {0} is {1}")]
		public virtual string Symbol { get; set; }


        [Column(Order = 24, TypeName = "varchar")]
        [DisplayName("Slug")]
        [Required(ErrorMessage = "{0} is required")]
        [StringLength(128, ErrorMessage = "Maximum length of {0} is {1}")]
        public virtual string Slug { get; set; }




        #region Navigation Properties

        // ----- Ads
        public virtual ICollection<Ads> Adses { get; set; }


        // ----- Deal
        public virtual ICollection<Deal> Deals { get; set; }

        #endregion
    }
}