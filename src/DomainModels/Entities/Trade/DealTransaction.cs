using DomainModels.Entities.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace DomainModels.Entities.Trade 
{
	[Table("DealTransaction", Schema = "Trade")]
	public class DealTransaction : BaseEntity 
	{

		[Column(Order = 8)]
		[DisplayName("Amount")]
		[Required(ErrorMessage= "{0} is required")]
		public virtual decimal Amount { get; set; }

		[Column(Order = 12)]
		[DisplayName("Destination wallet address")]
		[Required(ErrorMessage= "{0} is required")]
		[StringLength(128, ErrorMessage="Maximum length of {0} is {1}")]
		public virtual string DestinationWalletAddress { get; set; }

		[Column(Order = 16)]
		[DisplayName("Is confirmed ?")]
		[Required(ErrorMessage= "{0} is required")]
		public virtual bool IsConfirmed { get; set; }

		[Column(Order = 20)]
		[DisplayName("Created on")]
		[Required(ErrorMessage= "{0} is required")]
		public virtual DateTime CreatedOn { get; set; }


		#region Navigation Properties

		// ----- Deal
		[Column(Order = 504)]
		public virtual long DealId { get; set; }
		[ForeignKey("DealId")]
		public virtual Deal Deal { get; set; }


		#endregion
	}
}