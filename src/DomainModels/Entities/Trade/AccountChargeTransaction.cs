using DomainModels.Entities.HumanResource;
using DomainModels.Entities.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace DomainModels.Entities.Trade 
{
	[Table("AccountChargeTransaction", Schema = "Trade")]
	public class AccountChargeTransaction : BaseEntity 
	{

		[Column(Order = 8)]
		[DisplayName("Coin Amount")]
		[Required(ErrorMessage= "{0} is required")]
		public virtual decimal CoinAmount { get; set; }

		[Column(Order = 12)]
		[DisplayName("Destination wallet address")]
		[Required(ErrorMessage= "{0} is required")]
		[StringLength(128, ErrorMessage="Maximum length of {0} is {1}")]
		public virtual string DestinationWalletAddress { get; set; }

		[Column(Order = 16)]
		[DisplayName("Fee percent")]
		[Required(ErrorMessage= "{0} is required")]
		public virtual decimal FeePercent { get; set; }

		[Column(Order = 20)]
		[DisplayName("Fee amount")]
		[Required(ErrorMessage= "{0} is required")]
		public virtual decimal FeeAmount { get; set; }

		[Column(Order = 24)]
		[DisplayName("Is confirmed ?")]
		[Required(ErrorMessage= "{0} is required")]
		public virtual bool IsConfirmed { get; set; }

		[Column(Order = 28)]
		[DisplayName("Created on")]
		[Required(ErrorMessage= "{0} is required")]
		public virtual DateTime CreatedOn { get; set; }


		#region Navigation Properties

		// ----- Coin
		[Column(Order = 504)]
		public virtual long CoinId { get; set; }
		[ForeignKey("CoinId")]
		public virtual Coin Coin { get; set; }


		// ----- User
		[Column(Order = 508)]
		public virtual long PayerId { get; set; }
		[ForeignKey("PayerId")]
		public virtual User User { get; set; }


        #endregion


        public AccountChargeTransaction()
        {
            FeeAmount = 0; // not yet, maybe next phase
            FeePercent = 0; // not yet, maybe next phase
        }
	}
}