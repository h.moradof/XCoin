﻿using DomainModels.Entities.Base;
using DomainModels.Entities.Setting;
using DomainModels.Entities.Site;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DomainModels.Entities.Culture
{
    [Table("Language", Schema = "Culture")]
    public class Language : BaseEntity
    {

        [Column(Order = 3)]
        [DisplayName("Display name")]
        [Required(ErrorMessage = "{0} is required")]
        [StringLength(128, ErrorMessage = "Maximum length of {0} is {1}")]
        public virtual string DisplayName { get; set; }
        

        [Column(Order = 4, TypeName = "varchar")]
        [Required(ErrorMessage = "{0} is required")]
        [StringLength(128, ErrorMessage = "Maximum length of {0} is {1}")]
        [Index(IsUnique = true)]
        [ScaffoldColumn(false)]
        public virtual string IdentityName { get; set; } // used for procedures



        #region Navigation Properties

        // ----- SeoSetting
        public virtual ICollection<SeoSetting> SeoSettings { get; set; }

        // ----- SitePage
        public virtual ICollection<SitePage> SitePages { get; set; }

        #endregion
    }
}
