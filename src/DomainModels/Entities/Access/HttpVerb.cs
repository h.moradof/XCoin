﻿namespace DomainModels.Entities.Access
{
    public enum HttpVerb : byte
    {
        NULL = 0,
        Get,
        Post,
        Put,
        Delete,
        Option
    }
}
