using DomainModels.Entities.HumanResource;
using DomainModels.Entities.Base;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace DomainModels.Entities.Access 
{
	[Table("Role", Schema = "Access")]
	public class Role : BaseEntity
    {
        [Column(Order = 2)]
        public virtual RoleName Name { get; set; }


        [Column(Order = 3)]
        [DisplayName("Display name")]
        [Required(ErrorMessage = "{0} is required")]
        [StringLength(128, ErrorMessage = "Maximum length of {0} is {1}")]
        public virtual string DisplayName { get; set; }


        [Column(Order = 4, TypeName = "varchar")]
        [Required(ErrorMessage = "{0} is required")]
        [StringLength(128, ErrorMessage = "Maximum length of {0} is {1}")]
        [Index(IsUnique = true)]
        [ScaffoldColumn(false)]
        public virtual string IdentityName { get; set; } // used for procedures


        #region Navigation Properties

        // ----- RolePermission
        public virtual ICollection<RolePermission> RolePermissions { get; set; }


		// ----- User
		public virtual ICollection<User> Users { get; set; }


		#endregion
	}
}