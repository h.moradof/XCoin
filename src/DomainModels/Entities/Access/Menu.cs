using DomainModels.Entities.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace DomainModels.Entities.Access 
{
	[Table("Menu", Schema = "Access")]
	public class Menu : BaseEntity 
	{

		[Column(Order = 8)]
		[DisplayName("نام")]
		[Required(ErrorMessage= "{0} is required")]
		[StringLength(128, ErrorMessage="Maximum length of {0} is {1}")]
		public virtual string Name { get; set; }

		[Column(Order = 12)]
		[DisplayName("مسیر")]
		[Required(ErrorMessage= "{0} is required")]
		[StringLength(128, ErrorMessage="Maximum length of {0} is {1}")]
		public virtual string Url { get; set; }

		[Column(Order = 16)]
		[DisplayName("ردیف سفارش")]
		public virtual int? RowOrder { get; set; }


		#region Navigation Properties

		// ----- SubMenu
		public virtual ICollection<SubMenu> SubMenu { get; set; }


		#endregion
	}
}