using DomainModels.Entities.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace DomainModels.Entities.Access 
{
	[Table("PermissionSubMenu", Schema = "Access")]
	public class PermissionSubMenu : BaseEntity 
	{


		#region Navigation Properties

		// ----- SubMenu
		[Column(Order = 504)]
		public virtual long SubMenuId { get; set; }
		[ForeignKey("SubMenuId")]
		public virtual SubMenu SubMenu { get; set; }


		// ----- Permission
		[Column(Order = 508)]
		public virtual long PermissionId { get; set; }
		[ForeignKey("PermissionId")]
		public virtual Permission Permission { get; set; }


		#endregion
	}
}