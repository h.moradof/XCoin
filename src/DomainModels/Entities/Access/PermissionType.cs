﻿
namespace DomainModels.Entities.Access
{
    public enum PermissionType : byte
    {
        Site = 1,
        API
    }
}
