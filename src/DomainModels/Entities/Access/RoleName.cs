﻿namespace DomainModels.Entities.Access
{
    public enum RoleName : byte
    {
        Administrators = 1,
        Employees,
        VerifiedUsers,
        RegisteredUsers,
        AnonymousUsers
    }
}
