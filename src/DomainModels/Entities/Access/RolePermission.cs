using DomainModels.Entities.Base;
using System.ComponentModel.DataAnnotations.Schema;


namespace DomainModels.Entities.Access 
{
	[Table("RolePermission", Schema = "Access")]
	public class RolePermission : BaseEntity 
	{

		#region Navigation Properties

		// ----- Role
		[Column(Order = 504)]
		public virtual long RoleId { get; set; }
		[ForeignKey("RoleId")]
		public virtual Role Role { get; set; }


		// ----- Permission
		[Column(Order = 508)]
		public virtual long PermissionId { get; set; }
		[ForeignKey("PermissionId")]
		public virtual Permission Permission { get; set; }


		#endregion
	}
}