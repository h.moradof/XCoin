using DomainModels.Entities.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace DomainModels.Entities.Access 
{
	[Table("Permission", Schema = "Access")]
	public class Permission : BaseEntity 
	{

		[Column(Order = 8)]
		[DisplayName("نام")]
		[StringLength(256, ErrorMessage="Maximum length of {0} is {1}")]
		public virtual string Name { get; set; }

		[Column(Order = 12)]
		[DisplayName("")]
		[Required(ErrorMessage= "{0} is required")]
		[StringLength(256, ErrorMessage="Maximum length of {0} is {1}")]
		public virtual string Path { get; set; }

		[Column(Order = 16)]
		[DisplayName("")]
		[Required(ErrorMessage= "{0} is required")]
		public virtual HttpVerb HttpVerb { get; set; }

		[Column(Order = 20)]
		[DisplayName("شرح")]
		[StringLength(256, ErrorMessage="Maximum length of {0} is {1}")]
		public virtual string Description { get; set; }

		[Column(Order = 24)]
		[DisplayName("")]
		[Required(ErrorMessage= "{0} is required")]
		public virtual bool IsDefault { get; set; }

		[Column(Order = 28)]
		[DisplayName("")]
		[Required(ErrorMessage= "{0} is required")]
		public virtual PermissionType PermissionType { get; set; }

		[Column(Order = 32)]
		[DisplayName("ردیف سفارش")]
		public virtual int? RowOrder { get; set; }


		#region Navigation Properties

		// ----- RolePermission
		public virtual ICollection<RolePermission> RolePermissions { get; set; }


		// ----- PermissionSubMenu
		public virtual ICollection<PermissionSubMenu> PermissionSubMenu { get; set; }


		#endregion
	}
}