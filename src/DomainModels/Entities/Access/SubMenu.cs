using DomainModels.Entities.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace DomainModels.Entities.Access 
{
	[Table("SubMenu", Schema = "Access")]
	public class SubMenu : BaseEntity 
	{

		[Column(Order = 8)]
		[DisplayName("نام")]
		[Required(ErrorMessage= "{0} is required")]
		[StringLength(128, ErrorMessage="Maximum length of {0} is {1}")]
		public virtual string Name { get; set; }

		[Column(Order = 12)]
		[DisplayName("مسیر")]
		[Required(ErrorMessage= "{0} is required")]
		[StringLength(128, ErrorMessage="Maximum length of {0} is {1}")]
		public virtual string Url { get; set; }

		[Column(Order = 16)]
		[DisplayName("ردیف سفارش")]
		public virtual int? RowOrder { get; set; }


		#region Navigation Properties

		// ----- Menu
		[Column(Order = 504)]
		public virtual long MenuId { get; set; }
		[ForeignKey("MenuId")]
		public virtual Menu Menu { get; set; }


		// ----- PermissionSubMenu
		public virtual ICollection<PermissionSubMenu> PermissionSubMenu { get; set; }


		#endregion
	}
}