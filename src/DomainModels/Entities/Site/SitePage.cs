﻿using DomainModels.Entities.Base;
using DomainModels.Entities.Culture;
using System.ComponentModel.DataAnnotations.Schema;

namespace DomainModels.Entities.Site
{
    [Table("SitePage", Schema = "Site")]
    public class SitePage : BaseContent
    {


        #region Navigation Properties

        // ----- Language
        [Column(Order = 1000)]
        public virtual long LanguageId { get; set; }
        [ForeignKey("LanguageId")]
        public virtual Language Language { get; set; }

        #endregion
    }
}
