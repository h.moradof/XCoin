using DomainModels.Entities.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace DomainModels.Entities.Site 
{
	[Table("SiteVisit", Schema = "Site")]
	public class SiteVisit : BaseEntity 
	{


		[Column(Order = 8)]
		[DisplayName("Ip")]
		[Required(ErrorMessage= "{0} is required")]
		public virtual string Ip { get; set; }

		[Column(Order = 12)]
		[DisplayName("Visit date")]
		[Required(ErrorMessage= "{0} is required")]
		public virtual DateTime VisitDate { get; set; }

		[Column(Order = 16)]
		[DisplayName("Number")]
		[Required(ErrorMessage= "{0} is required")]
		public virtual int Number { get; set; }


    }
}