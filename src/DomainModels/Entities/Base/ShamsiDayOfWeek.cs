﻿using System.ComponentModel;

namespace DomainModels.Entities.Base
{
    public enum ShamsiDayOfWeek : int
    {
        [Description("شنبه")]
        Saturday = 1,
        [Description("یکشنبه")]
        Sunday,
        [Description("دوشنبه")]
        Monday,
        [Description("سه‌شنبه")]
        TuesDay,
        [Description("چهارشنبه")]
        WednesDay,
        [Description("پنج شنبه")]
        ThurseDay,
        [Description("جمعه")]
        Friday
    }

}
