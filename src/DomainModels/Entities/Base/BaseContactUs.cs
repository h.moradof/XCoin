﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DomainModels.Entities.Base
{
    public abstract  class BaseContactUs : BaseEntity
    {

        [Column(Order = 1)]
        [DisplayName("Fullname")]
        [Required(ErrorMessage = "{0} is required")]
        [StringLength(128, ErrorMessage = "Maximum length of {0} is {1}")]
        public virtual string FullName { get; set; }

        [Column(Order = 2)]
        [DisplayName("Email")]
        [Required(ErrorMessage = "{0} is required")]
        [StringLength(32, ErrorMessage = "Maximum length of {0} is {1}")]
        [EmailAddress(ErrorMessage = "Email's Format is invalid")]
        public virtual string Email { get; set; }

        [Column(Order = 3)]
        [DisplayName("Phone number")]
        [StringLength(32, ErrorMessage = "Maximum length of {0} is {1}")]
        public virtual string PhoneNumber { get; set; }

        [Column(Order = 4)]
        [DisplayName("پیام")]
        [Required(ErrorMessage = "{0} is required")]
        [StringLength(1024, ErrorMessage = "Maximum length of {0} is {1}")]
        public virtual string Message { get; set; }

        [Column(Order = 5)]
        [DisplayName("Ip")]
        [Required(ErrorMessage = "{0} is required")]
        [StringLength(16, ErrorMessage = "Maximum length of {0} is {1}")]
        public virtual string Ip { get; set; }

        [Column(Order = 6)]
        public virtual bool IsRead { get; set; }


        public BaseContactUs()
        {
            IsRead = false;
        }
    }
}
