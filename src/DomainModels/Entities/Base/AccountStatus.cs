﻿using System.ComponentModel;

namespace DomainModels.Entities.HumanResource
{
    public enum AccountStatus : byte
    {
        [Description("غیرفعال")]
        Deactive = 1,
        [Description("فعال")]
        Active,
        [Description("بن شده")]
        Baned
    }
}