﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DomainModels.Entities.Base
{
    public abstract class BaseEntity 
    {
        [Column(Order = 1)]
        [DisplayName("Id")]
        [Required(ErrorMessage = "{0} is required")]
        public virtual long Id { get; set; }
    }
}
