﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace DomainModels.Entities.Base
{
    public abstract class BaseContent : BaseEntity
    {
        [Column(Order = 2), DisplayName("Title")]
        [Required(ErrorMessage = "{0} is required")]
        [StringLength(128, ErrorMessage = "Maximum length of {0} is {1}")]
        public virtual string Title { get; set; }


        [Column(Order = 4), DisplayName("Slug")]
        [StringLength(256, ErrorMessage = "Maximum length of {0} is {1}")]
        public virtual string Slug { get; set; }


        [Column(Order = 8, TypeName = "varchar"), DisplayName("Photo name")]
        [StringLength(128, ErrorMessage = "Maximum length of {0} is {1}")]
        public virtual string PhotoName { get; set; }


        [Column(Order = 16), DisplayName("Short description")]
        [StringLength(1024, ErrorMessage = "Maximum length of {0} is {1}")]
        public virtual string ShortDescription { get; set; }


        [Column(Order = 20), DisplayName("Description")]
        [AllowHtml]
        public virtual string Description { get; set; }
        

        [Column(Order = 28), DisplayName("Meta keywords")]
        [StringLength(256, ErrorMessage = "Maximum length of {0} is {1}")]
        public virtual string MetaKeywords { get; set; }


        [Column(Order = 32), DisplayName("Meta description")]
        [StringLength(256, ErrorMessage = "Maximum length of {0} is {1}")]
        public virtual string MetaDescription { get; set; }


        [Column(Order = 36), DisplayName("Created on")]
        public virtual DateTime CreatedOn { get; set; }


        public BaseContent()
        {
            CreatedOn = DateTime.Now;
        }
    }

}
