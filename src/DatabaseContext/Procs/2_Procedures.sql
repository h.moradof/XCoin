﻿USE [XCoin]
GO

-- ****************************************************** sp_GetPanelMenu **********************************************
CREATE PROC [dbo].[sp_GetPanelMenu](@UserId INT)
AS
BEGIN


	DECLARE @Menus TABLE
	(
		Id BIGINT IDENTITY(1,1) NOT NULL,
		MenuId BIGINT NOT NULL,
		Name NVARCHAR(128) NOT NULL,
		Url NVARCHAR(128) NOT NULL,
		Icon NVARCHAR(128) NULL,
		RowOrder INT NULL
	)

	INSERT @Menus
	SELECT Distinct M.Id, M.Name, M.Url,'fa-chevron-right' AS Icon, M.RowOrder
	FROM [HumanResource].[User] U
	INNER JOIN [Access].[Role] R ON U.RoleId = R.Id
	INNER JOIN [Access].[RolePermission] RP ON R.Id = RP.RoleId
	INNER JOIN [Access].[Permission] P ON RP.PermissionId = P.Id
	INNER JOIN [Access].[PermissionSubMenu] PSM ON P.Id = PSM.PermissionId
	INNER JOIN [Access].[SubMenu] SM ON PSM.SubMenuId = SM.Id
	INNER JOIN [Access].[Menu] M ON SM.MenuId = M.Id
	WHERE U.Id = @UserId AND M.DeletedOn IS NULL
	ORDER By RowOrder
	

	DECLARE @SubMenus TABLE
	(
		Id BIGINT IDENTITY(1,1) NOT NULL,
		Name NVARCHAR(128) NOT NULL,
		Url NVARCHAR(128) NOT NULL
	)

	DECLARE @MenuHtml NVARCHAR(MAX) = '';
	DECLARE @i INT= 1;
	DECLARE @j INT = 1;
	DECLARE @LastMenusId BIGINT = (SELECT TOP 1 Id FROM @Menus ORDER BY Id DESC);
	DECLARE @LastSubMenusId BIGINT;
	DECLARE @SubMenusCount INT = 0;

	DECLARE @menuItemName NVARCHAR(128);
	DECLARE @menuItemUrl NVARCHAR(128);
	DECLARE @menuItemMenuIcon NVARCHAR(128);
	DECLARE @menuItemMenuId BIGINT;

	DECLARE @subMenuItemName NVARCHAR(128);
	DECLARE @subMenuItemUrl NVARCHAR(128);
	

	WHILE(@i <= @LastMenusId)
	BEGIN
		
		SET @menuItemName =		(SELECT Name   FROM @Menus WHERE Id = @i);
		SET @menuItemUrl =		(SELECT Url    FROM @Menus WHERE Id = @i);
		SET @menuItemMenuIcon = (SELECT Icon   FROM @Menus WHERE Id = @i);
		SET @menuItemMenuId =	(SELECT MenuId FROM @Menus WHERE Id = @i);

		SET @MenuHtml += '<li>';
		SET @MenuHtml += '<a href="' + @menuItemUrl + '"><i class="fa ' + @menuItemMenuIcon + ' fa-fw fa-big"></i>' + @menuItemName + ' <span class="fa arrow"></span></a>'

		-- ------------------- start sub menu
		INSERT @SubMenus
		SELECT SM.Name, SM.Url
		FROM [Access].[SubMenu] SM
		WHERE SM.MenuId = @menuItemMenuId AND SM.DeletedOn IS NULL
		ORDER By RowOrder

		SET @LastSubMenusId = (SELECT TOP 1 Id FROM @SubMenus ORDER BY Id DESC);
		SET @SubMenusCount = (SELECT COUNT(Id) FROM @SubMenus);
		
		IF(@SubMenusCount > 0)
		BEGIN

			SET @MenuHtml += '<ul class="nav nav-second-level">';
		
			WHILE(@j <= @LastSubMenusId)
			BEGIN
			
				SET @subMenuItemName =	(SELECT Name FROM @SubMenus WHERE Id = @j);
				SET @subMenuItemUrl =	(SELECT Url  FROM @SubMenus WHERE Id = @j);

				SET @MenuHtml += '<li><a href="' + @subMenuItemUrl + '">' + @subMenuItemName + '</a></li>';

				SET @j = @j + 1;
			END

			SET @MenuHtml += '</ul>';
			DELETE FROM @SubMenus
		END

		-- ------------------------------ end sub menu

		SET @MenuHtml += '</li>';
		SET @i = @i + 1;
	END


	SELECT @MenuHtml as 'MenuHtml'

END
GO

-- ****************************************************** sp_GetMonthlyVisits **********************************************
CREATE PROC sp_GetMonthlyVisits
(
	@Month_1_StartDate DATE, @Month_1_EndDate DATE, 
	@Month_2_StartDate DATE, @Month_2_EndDate DATE,
	@Month_3_StartDate DATE, @Month_3_EndDate DATE,
	@Month_4_StartDate DATE, @Month_4_EndDate DATE,
	@Month_5_StartDate DATE, @Month_5_EndDate DATE,
	@Month_6_StartDate DATE, @Month_6_EndDate DATE,
	@Month_7_StartDate DATE, @Month_7_EndDate DATE,
	@Month_8_StartDate DATE, @Month_8_EndDate DATE,
	@Month_9_StartDate DATE, @Month_9_EndDate DATE,
	@Month_10_StartDate DATE, @Month_10_EndDate DATE,
	@Month_11_StartDate DATE, @Month_11_EndDate DATE,
	@Month_12_StartDate DATE, @Month_12_EndDate DATE
)
AS
BEGIN

	SELECT ISNULL(SUM(Visit.Number), 0) AS VisitCount FROM [Site].SiteVisit Visit
	WHERE Visit.VisitDate BETWEEN @Month_1_StartDate AND @Month_1_EndDate
	UNION ALL
	SELECT ISNULL(SUM(Visit.Number), 0) AS VisitCount FROM [Site].SiteVisit Visit
	WHERE Visit.VisitDate BETWEEN @Month_2_StartDate AND @Month_2_EndDate
	UNION ALL
	SELECT ISNULL(SUM(Visit.Number), 0) AS VisitCount FROM [Site].SiteVisit Visit
	WHERE Visit.VisitDate BETWEEN @Month_3_StartDate AND @Month_3_EndDate
	UNION ALL
	SELECT ISNULL(SUM(Visit.Number), 0) AS VisitCount FROM [Site].SiteVisit Visit
	WHERE Visit.VisitDate BETWEEN @Month_4_StartDate AND @Month_4_EndDate
	UNION ALL
	SELECT ISNULL(SUM(Visit.Number), 0) AS VisitCount FROM [Site].SiteVisit Visit
	WHERE Visit.VisitDate BETWEEN @Month_5_StartDate AND @Month_5_EndDate
	UNION ALL
	SELECT ISNULL(SUM(Visit.Number), 0) AS VisitCount FROM [Site].SiteVisit Visit
	WHERE Visit.VisitDate BETWEEN @Month_6_StartDate AND @Month_6_EndDate
	UNION ALL
	SELECT ISNULL(SUM(Visit.Number), 0) AS VisitCount FROM [Site].SiteVisit Visit
	WHERE Visit.VisitDate BETWEEN @Month_7_StartDate AND @Month_7_EndDate
	UNION ALL
	SELECT ISNULL(SUM(Visit.Number), 0) AS VisitCount FROM [Site].SiteVisit Visit
	WHERE Visit.VisitDate BETWEEN @Month_8_StartDate AND @Month_8_EndDate
	UNION ALL
	SELECT ISNULL(SUM(Visit.Number), 0) AS VisitCount FROM [Site].SiteVisit Visit
	WHERE Visit.VisitDate BETWEEN @Month_9_StartDate AND @Month_9_EndDate
	UNION ALL
	SELECT ISNULL(SUM(Visit.Number), 0) AS VisitCount FROM [Site].SiteVisit Visit
	WHERE Visit.VisitDate BETWEEN @Month_10_StartDate AND @Month_10_EndDate
	UNION ALL
	SELECT ISNULL(SUM(Visit.Number), 0) AS VisitCount FROM [Site].SiteVisit Visit
	WHERE Visit.VisitDate BETWEEN @Month_11_StartDate AND @Month_11_EndDate
	UNION ALL
	SELECT ISNULL(SUM(Visit.Number), 0) AS VisitCount FROM [Site].SiteVisit Visit
	WHERE Visit.VisitDate BETWEEN @Month_12_StartDate AND @Month_12_EndDate   

END
GO

