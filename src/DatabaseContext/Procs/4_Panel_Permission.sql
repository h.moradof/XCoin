﻿USE [XCoin]
GO

Delete [Access].RolePermission
Delete [Access].PermissionSubMenu
Delete [Access].Permission
Delete [Access].SubMenu
Delete [Access].Menu
-- ================================================= [Menu] =========================================================
IF (NOT EXISTS(SELECT * FROM [Access].[Menu]))
BEGIN
	SET IDENTITY_INSERT [Access].[Menu] ON;

	INSERT INTO [Access].[Menu](Id, Name, Url, RowOrder) VALUES (1, N'', N'javascript:;', 1)

	SET IDENTITY_INSERT [Access].[Menu] OFF;
END
GO


-- ================================================= [SubMenu] =========================================================
IF (NOT EXISTS(SELECT * FROM [Access].[SubMenu]))
BEGIN
	SET IDENTITY_INSERT [Access].[SubMenu] ON;

	INSERT INTO [Access].[SubMenu](Id, Name, Url, MenuId, RowOrder) VALUES (1, N'', N'/Admin/', 1, 1)
	

	SET IDENTITY_INSERT [Access].[SubMenu] OFF;
END
GO



-- ================================================= [Permission] =========================================================
IF (NOT EXISTS(SELECT * FROM [Access].[Permission]))
BEGIN
	SET IDENTITY_INSERT [Access].[Permission] ON;

	-- ------------- start variables -----------
	declare @administratorsRoleId BIGINT = 1;
	declare @registeredUsersRoleId BIGINT = 2;
	declare @anonymouseUsersRoleId BIGINT = 3;
	declare @HttpMethod_NULL int = 0;
	-- ------------- end variables -------------

	-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	-- ++++++++++++++++++++++++++++++++++++ Admin panel ++++++++++++++++++++++++++++++++++++
	-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	-- /Dashboard
	INSERT INTO [Access].[Permission](Id, Name, [Path], HttpVerb, PermissionType, IsDefault, RowOrder) VALUES (1, N'Dashboard', N'/Admin/Dashboard/Show', @HttpMethod_NULL, 0, 1, 1) -- default
	
	-- /Dashboard (partial views)
	INSERT INTO [Access].[Permission](Id, Name, [Path], HttpVerb, PermissionType, IsDefault, RowOrder) VALUES (10, N'Visit statistics', N'/Admin/Dashboard/_SiteVisitReport', @HttpMethod_NULL, 0, 0, 11)
	INSERT INTO [Access].[Permission](Id, Name, [Path], HttpVerb, PermissionType, IsDefault, RowOrder) VALUES (11, N'See SiteVisit Chart Report', N'/Admin/Dashboard/_SiteVisitChartReport', @HttpMethod_NULL, 0, 0, 11)

    -- /Layout (partial views)
	INSERT INTO [Access].[Permission](Id, Name, [Path], HttpVerb, PermissionType, IsDefault, RowOrder) VALUES (20, N'Top Menu', N'/Admin/Partial/_TopMenu', @HttpMethod_NULL, 0, 1, 222) -- default
	INSERT INTO [Access].[Permission](Id, Name, [Path], HttpVerb, PermissionType, IsDefault, RowOrder) VALUES (21, N'Right Menu', N'/Admin/Partial/_RightMenu', @HttpMethod_NULL, 0, 1, 222) -- default
	INSERT INTO [Access].[Permission](Id, Name, [Path], HttpVerb, PermissionType, IsDefault, RowOrder) VALUES (22, N'Panel Title', N'/Admin/Partial/_DashboardTitle', @HttpMethod_NULL, 0, 1, 222) -- default

	-- /Profile
	INSERT INTO [Access].[Permission](Id, Name, [Path], HttpVerb, PermissionType, IsDefault, RowOrder) VALUES (30, N'User Profile', N'/Admin/Profile/Edit', @HttpMethod_NULL, 0, 1, 233) -- default

	-- /SeoSetting *
	INSERT INTO [Access].[Permission](Id, Name, [Path], HttpVerb, PermissionType, IsDefault, RowOrder) VALUES (40, N'Seo Setting', N'/Admin/SeoSetting/Edit', @HttpMethod_NULL, 0, 0, 277)

	-- /Message
	INSERT INTO [Access].[Permission](Id, Name, [Path], HttpVerb, PermissionType, IsDefault, RowOrder) VALUES (50, N'Message', N'/Admin/Message/Show', @HttpMethod_NULL, 0, 1, 288) -- default

	-- /Role
	INSERT INTO [Access].[Permission](Id, Name, [Path], HttpVerb, PermissionType, IsDefault, RowOrder) VALUES (60, N'See roles list', N'/Admin/Role/Index', @HttpMethod_NULL, 0, 0, 99)
	INSERT INTO [Access].[Permission](Id, Name, [Path], HttpVerb, PermissionType, IsDefault, RowOrder) VALUES (61, N'Create a new role', N'/Admin/Role/Create', @HttpMethod_NULL, 0, 0, 99)
	INSERT INTO [Access].[Permission](Id, Name, [Path], HttpVerb, PermissionType, IsDefault, RowOrder) VALUES (62, N'Edit a role', N'/Admin/Role/Edit', @HttpMethod_NULL, 0, 0, 99)
	INSERT INTO [Access].[Permission](Id, Name, [Path], HttpVerb, PermissionType, IsDefault, RowOrder) VALUES (63, N'Delete a role', N'/Admin/Role/Delete', @HttpMethod_NULL, 0, 0, 99)
	INSERT INTO [Access].[PermissionSubMenu](PermissionId, SubMenuId) VALUES(81, 16)
	INSERT INTO [Access].[PermissionSubMenu](PermissionId, SubMenuId) VALUES(82, 15)

	-- /User
	INSERT INTO [Access].[Permission](Id, Name, [Path], HttpVerb, PermissionType, IsDefault, RowOrder) VALUES (70, N'See user''s list', N'/Admin/User/Index', @HttpMethod_NULL, 0, 0,  111)
	INSERT INTO [Access].[Permission](Id, Name, [Path], HttpVerb, PermissionType, IsDefault, RowOrder) VALUES (71, N'Create new user(staff)', N'/Admin/User/Create', @HttpMethod_NULL, 0, 0,    111)
	INSERT INTO [Access].[Permission](Id, Name, [Path], HttpVerb, PermissionType, IsDefault, RowOrder) VALUES (72, N'See user''s detail', N'/Admin/User/Detail', @HttpMethod_NULL, 0, 0, 111)
	INSERT INTO [Access].[Permission](Id, Name, [Path], HttpVerb, PermissionType, IsDefault, RowOrder) VALUES (73, N'Edit an user', N'/Admin/User/Edit', @HttpMethod_NULL, 0, 0, 111)
	INSERT INTO [Access].[Permission](Id, Name, [Path], HttpVerb, PermissionType, IsDefault, RowOrder) VALUES (74, N'Delete an user', N'/Admin/User/Delete', @HttpMethod_NULL, 0, 0, 111)
	INSERT INTO [Access].[PermissionSubMenu](PermissionId, SubMenuId) VALUES(70, 18)
	INSERT INTO [Access].[PermissionSubMenu](PermissionId, SubMenuId) VALUES(71, 17)


	-- /

	SET IDENTITY_INSERT [Access].[Permission] OFF;
END
GO

-- ================================================= [RolePermission] =========================================================
IF (NOT EXISTS(SELECT * FROM [Access].[RolePermission]))
BEGIN

	-- ------------- start variables -----------
	declare @administratorsRoleId BIGINT = 1;
	declare @registeredUsersRoleId BIGINT = 2;
	declare @anonymouseUsersRoleId BIGINT = 3;
	-- ------------- end variables -----------
	
	INSERT INTO  [Access].[RolePermission](RoleId, PermissionId, CreatedOn)
	SELECT @administratorsRoleId, Id, GETDATE()
	FROM [Access].[Permission]

END