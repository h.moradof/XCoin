namespace DatabaseContext.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<DatabaseContext.Context.DefaultDatabaseContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(DatabaseContext.Context.DefaultDatabaseContext context)
        {
            context.Database.ExecuteSqlCommand("ALTER TABLE [HumanResource].[User] DROP COLUMN FullName");
            context.Database.ExecuteSqlCommand("ALTER TABLE [HumanResource].[User] ADD FullName AS (FirstName + ' ' + LastName)");
        }
    }
}
