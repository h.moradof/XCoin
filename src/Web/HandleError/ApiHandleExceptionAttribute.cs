﻿using Infrastructure.Exceptions;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http.Filters;
using ViewModels.Base;

namespace Web.HandleError
{
    public class ApiHandleExceptionAttribute : ExceptionFilterAttribute, IExceptionFilter
    {
        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            var returnData = new ApiResultViewModel { HasError = true };

            if (actionExecutedContext.Exception is CustomException ||
                actionExecutedContext.Exception is ArgumentOutOfRangeException ||
                actionExecutedContext.Exception is ArgumentNullException)
            {
                returnData.Message = actionExecutedContext.Exception.Message;
            }
            else
            {
                returnData.Message = "خطایی در سرور رخ داده است";
            }

            actionExecutedContext.Response = actionExecutedContext.Request.CreateResponse(HttpStatusCode.OK, returnData);
        }
    }
}