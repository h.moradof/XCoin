﻿using System;
using System.IO;
using System.Web.Hosting;

namespace Web.HandleError
{
    public static class ScheduleErrorHandler
    {
        public static void Log(string scheduleName, string errorMessage)
        {
            using (var writer = new StreamWriter(HostingEnvironment.MapPath("~/App_Data/err.txt"), true, System.Text.Encoding.UTF8))
            {
                writer.WriteLine(string.Format("----------{0} ----> {1}------\r\n{2}\r\n", scheduleName, DateTime.Now.ToString("yyyy/MM/dd hh:mm"), errorMessage));
            }
        }
    }
}