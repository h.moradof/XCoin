﻿using System.Web.Mvc;
using Elmah;
using System.Web;

namespace Web.Security.HandleError
{
    public class ElmahHandledErrorLoggerAttribute : IExceptionFilter
    {
        public void OnException(ExceptionContext context)
        {
            if (context.ExceptionHandled)
                ErrorSignal.FromCurrentContext().Raise(context.Exception);

            if (context.Exception is HttpRequestValidationException)
                ErrorLog.GetDefault(HttpContext.Current).Log(new Error(context.Exception));

            // all other exceptions will be caught by ELMAH anyway
        }
    }

}