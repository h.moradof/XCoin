﻿using Infrastructure.Cache.Interfaces;
using Infrastructure.Cache.Model;
using Services.Interfaces;
using Web.DependencyResolution;
using System.Configuration;
using System;


namespace Web.App_Start
{
    public static class CacheRegisteration
    {
        public static void Register()
        {
            // cache application settings (web.config)
            var applicationSettingCacheManager = StructureMapObjectFactory.Container.GetInstance<IApplicationSettingCacheManager>();
            var applicationSetting = new ApplicationSetting();
            applicationSetting.WebsiteDomain = ConfigurationManager.AppSettings["WebsiteDomin"].ToString();
            applicationSetting.DefaultPicName = ConfigurationManager.AppSettings["DefaultPicName"].ToString();
            applicationSetting.AllowNotAjaxRequests = Convert.ToBoolean(ConfigurationManager.AppSettings["AllowNotAjaxRequests"]);
           
            // mail setting
            var mailSetting = new MailSetting
            {
                From = ConfigurationManager.AppSettings["MailSetting_From"].ToString(),
                Host = ConfigurationManager.AppSettings["MailSetting_Host"].ToString(),
                Password = ConfigurationManager.AppSettings["MailSetting_Password"].ToString(),
                PortNumber = int.Parse(ConfigurationManager.AppSettings["MailSetting_PortNumber"].ToString()),
                UseSsl = bool.Parse(ConfigurationManager.AppSettings["MailSetting_UseSsl"].ToString())
            };
            applicationSetting.MailSetting = mailSetting;

            // sms setting
            var smsSetting = new SmsSetting
            {
                Username = ConfigurationManager.AppSettings["SMSSetting_Username"].ToString(),
                Password = ConfigurationManager.AppSettings["SMSSetting_Password"].ToString(),
                SenderPhoneNumber = ConfigurationManager.AppSettings["SMSSetting_SenderPhoneNumber"].ToString(),
                ApiKey = ConfigurationManager.AppSettings["SMSSetting_ApiKey"].ToString()
            };
            applicationSetting.SmsSetting = smsSetting;

            applicationSettingCacheManager.Cache(applicationSetting);

            // cache site seo setting
            var siteSeoSettingService = StructureMapObjectFactory.Container.GetInstance<ISeoSettingService>();
            var siteSeoSettingCacheManager = StructureMapObjectFactory.Container.GetInstance<ISeoSettingCacheManager>();
            siteSeoSettingCacheManager.Cache(siteSeoSettingService.GetList());

        }
    }
}