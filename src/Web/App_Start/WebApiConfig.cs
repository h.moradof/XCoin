﻿using Web.DependencyResolution;
using System.Web.Http;
using System.Web.Http.Dispatcher;
using System.Web.Http.ExceptionHandling;
using Elmah.Contrib.WebApi;
using Web.Security.Attributes;
using Web.HandleError;
using System.Web.Http.Filters;

namespace Web
{
    public static class WebApiConfig
    {
        /// <summary>
        /// Register configs for WEB API controllers
        /// </summary>
        /// <param name="config"></param>
        public static void Register(HttpConfiguration config)
        {
            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );


            // apply error handler
            config.Filters.Add(new ApiHandleExceptionAttribute());
            config.Services.Add(typeof(IExceptionLogger), new ElmahExceptionLogger());

            // IoC Config
            var container = StructureMapObjectFactory.Container;
            GlobalConfiguration.Configuration.Services.Replace(
                typeof(IHttpControllerActivator), new StructureMapHttpControllerActivator(container));


            // remove xml formatter
            var formatters = GlobalConfiguration.Configuration.Formatters;
            formatters.Remove(formatters.XmlFormatter);

        }

    }
}
