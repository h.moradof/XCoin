﻿using System.Web;
using System.Web.Optimization;

namespace Web
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {

            // --------------------------- styles ---------------------------
            bundles.Add(new StyleBundle("~/theme/css").Include(
                "~/Content/lib/css/bootstrap.css",
                "~/Content/lib/css/font-awesome.min.css",
                "~/Content/lib/css/pagedlist.css",
                "~/Content/lib/css/account.css",
                "~/Content/lib/messagebox/messagebox.css",
                "~/Content/site/css/style.css"));

            //bundles.Add(new StyleBundle("~/theme/main").Include(
            //    ));


            // --------------------------- scripts ---------------------------
            bundles.Add(new ScriptBundle("~/theme/js/jquery").Include(
                "~/Content/lib/js/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/theme/js/jqueryval").Include(
                        "~/Scripts/jquery.validate.js"));

            bundles.Add(new ScriptBundle("~/theme/js/bootstrap").Include(
                "~/Content/lib/js/bootstrap.js",
                "~/Content/lib/js/respond.js"));

            bundles.Add(new ScriptBundle("~/theme/js/main").Include(
                "~/Content/lib/js/AjaxUtility.js",
                "~/Content/lib/messagebox/messagebox.js"));

            bundles.Add(new ScriptBundle("~/theme/js/memberlayout").Include(
                "~/Content/member/js/layout.js"));

            bundles.Add(new ScriptBundle("~/theme/js/deal").Include(
                "~/Content/member/js/deal.js"));

        }
    }
}
