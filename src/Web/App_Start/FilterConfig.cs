﻿using Web.Security.HandleError;
using System.Web.Mvc;
using Web.Security.Attributes;
using Web.LanguageHandler;

namespace Web
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            // filters.Add(new ElmahHandledErrorLoggerAttribute());

            filters.Add(new HandleErrorAttribute());

            //filters.Add(new MvcAuthorizeAttribute()); فاز بعدی

            filters.Add(new LangaugeHandlerAttribute());
        }
    }
}
