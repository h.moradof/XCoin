﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "UserDetail",
                url: "{lang}/user/detail/{username}",
                defaults: new { controller = "User", action = "Detail", username = UrlParameter.Optional, lang = "en" },
                namespaces: new string[] { "Web.Controllers" }
            );

            routes.MapRoute(
                name: "PageDetail",
                url: "{lang}/page/{slug}",
                defaults: new { controller = "Page", action = "Detail", slug = UrlParameter.Optional, lang = "en" },
                namespaces: new string[] { "Web.Controllers" }
            );

            routes.MapRoute(
                name: "DefaultLocalized",
                url: "{lang}/{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional, lang = "en" },
                namespaces: new string[] { "Web.Controllers" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new string[] { "Web.Controllers" }
            );
        }
    }
}
