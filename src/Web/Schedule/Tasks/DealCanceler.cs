﻿using DatabaseContext.Context;
using DNTScheduler;
using Services.Interfaces;
using System;
using Web.DependencyResolution;

namespace Web.Schedule.Tasks
{
    public class DealCanceler : ScheduledTaskTemplate
    {


        /// <summary>
        /// نام وظیفه‌ی جاری است که می‌تواند در گزارشات مفید باشد
        /// </summary>
        public override string Name
        {
            get { return "Deal Canceler"; }
        }

        /// <summary>
        /// اگر چند جاب در یک زمان مشخص داشتید، این خاصیت ترتیب اجرای آن‌ها را مشخص خواهد کرد
        /// </summary>
        public override int Order
        {
            get { return 1; }
        }


        /// <summary>
        /// این متد ثانیه‌ای یکبار فراخوانی می‌شود
        /// </summary>
        /// <param name="utcNow">تاریخ جاری بر اساس ساعت جهانی گرینویچ</param>
        /// <returns>متد 'ران' اجرا بشه یا نشه</returns>
        public override bool RunAt(DateTime utcNow)
        {
            if (this.IsShuttingDown || this.Pause)
                return false;

            //return true;
            return utcNow.Second == 1; //  هر یک دقیقه
        }


        /// <summary>
        ///اصل کدی که باید در زمان های مورد نظر ما اجرا بشه
        /// </summary>
        public override void Run()
        {
            if (this.IsShuttingDown || this.Pause)
                return;

            var _uow = StructureMapObjectFactory.Container.GetInstance<IUnitOfWork>();
            var _dealService = StructureMapObjectFactory.Container.GetInstance<IDealService>();

            _dealService.CancelNotPaidDeals();
            _uow.SaveChanges();
        }

    }
}
