﻿$(function () {

    // ================================ page.load() =================================
    loadUserBalances();

});


// ================================= functions ==================================
function loadUserBalances() {
    Ajax('Post', '/@ViewContext.RouteData.Values["lang"]/Member/Partial/GetUserBalances', null, function (rsp, textStatus, xhr) {

        if (rsp.HasError) {
            showMessage(false, rsp.Message);
        }
        else {

            var balancesHtml = '';

            $(rsp.Data).each(function (i, item) {
                balancesHtml += '<a class="dropdown-item" href="javascript:;">' + item.CoinName + ': <b>' + item.BalanceAmount + '</b></a>';
            });

            $("#userBalances").html(balancesHtml);
            console.log("refresh balances ...");
        }

    }, 'json');
}