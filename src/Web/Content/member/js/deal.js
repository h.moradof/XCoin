﻿$(function () {

    var isUpdatingChatMessages = true;

    // ------------------------------------ page.load ----------------------------------
    // chat
    refreshChatMessages();

    // alerts
    refreshAlerts();


    // -------------------------------------- events -----------------------------------
    // chat
    $('#chatMessage').on('keyup', function (e) {
        if (e.keyCode == 13) {
            sendChatMessage();
        }
    });

    $("#sendChatMessage").click(function () {
        sendChatMessage();
    });

    // I paid
    $("#footerBtns").on("click", "#btnIPaid", function () {
        IPaid();
    });

    // release coin
    $("#footerBtns").on("click", "#btnReleaseCoin", function () {
        releaseCoin();
    });

    // cancel
    $("#footerBtns").on("click", "#btnCancel", function () {
        cancelingDeal();
    });

    // judge
    $("#footerBtns").on("click", "#btnJudge", function () {
        judgeDeal();
    });

    // -------------------------------------- functions ---------------------------------
    function refreshChatMessages() {
        $("#loadingImg").show();

        Ajax('Post', '/'+ currentLang +'/Member/Deal/GetChatMessages', { 'dealId': dealGuid }, function (rsp, textStatus, xhr) {

            if (rsp.HasError) {
                showMessage(false, rsp.Message);
            }
            else {
                var msgsHtml = '';
                var messageItemsCount = 0;

                $(rsp.Data).each(function (i, item) {
                    if (item.IsSender) {
                        msgsHtml += '<p class="chat-message-item is-sender"><b>' + item.Sender + '</b>: ' + item.Message + '<br/><span class="chat-time"><i class="fa fa-clock-o"></i> ' + item.CreatedOn + '</span></p>';
                    }
                    else {
                        msgsHtml += '<p class="chat-message-item is-not-sender"><b>' + item.Sender + '</b>: ' + item.Message + '<br/><span class="chat-time"><i class="fa fa-clock-o"></i> ' + item.CreatedOn + '</span></p>';
                    }

                    messageItemsCount++;
                });

                $("#chatArea").html(msgsHtml);

                // scroll to bottom
                $("#chatArea").animate({
                    scrollTop: (messageItemsCount * 400)
                });
            }

            $("#loadingImg").hide();


            if (isUpdatingChatMessages) {

                // run itself
                setTimeout(function () { refreshChatMessages(); }, 10000);
            }

        }, 'json');
    }


    function sendChatMessage()
    {
        var chatMessage = $("#chatMessage").val();

        if (chatMessage == '') {
            showMessage(false, "please write a message");
        }
        else {

            $("#loadingImg").show();

            Ajax('Post', '/'+ currentLang +'/Member/Deal/sendChatMessage', { 'message': chatMessage, 'dealid': dealGuid }, function (rsp, textstatus, xhr) {

                if (rsp.HasError)
                {
                    showMessage(false, rsp.Message);
                }
                else
                {
                    refreshChatMessages();
                }

                $("#chatMessage").val('');
                $("#loadingImg").hide();

            }, 'json');
        }
    }

    function IPaid() {
        Ajax('Post', '/'+ currentLang +'/Member/Deal/BuyerPayCurrency', { 'rowGuid': dealGuid }, function (rsp, textStatus, xhr) {

            if (rsp.HasError) {
                showMessage(false, rsp.Message);
            }
            else {
                refreshAlerts();
            }

        }, 'json');
    }


    function releaseCoin() {
        Ajax('Post', '/'+ currentLang +'/Member/Deal/SalerReleaseCoin', { 'rowGuid': dealGuid }, function (rsp, textStatus, xhr) {

            if (rsp.HasError) {
                showMessage(false, rsp.Message);
            }
            else {
                refreshAlerts();
            }

        }, 'json');
    }


    function cancelingDeal() {
        Ajax('Post', '/'+ currentLang +'/Member/Deal/CancelingDeal', { 'rowGuid': dealGuid }, function (rsp, textStatus, xhr) {

            if (rsp.HasError) {
                showMessage(false, rsp.Message);
            }
            else {
                showMessage(true, rsp.Message);
            }

            refreshAlerts();
        }, 'json');
    }

    function judgeDeal() {
        Ajax('Post', '/'+ currentLang +'/Member/Deal/JudgeDeal', { 'rowGuid': dealGuid }, function (rsp, textStatus, xhr) {

            if (rsp.HasError) {
                showMessage(false, rsp.Message);
            }
            else {
                showMessage(true, rsp.Message);
            }

            refreshAlerts();
        }, 'json');
    }


    // refresh alerts
    function refreshAlerts() {
        $("#loadingAlertImg").show();

        Ajax('Post', '/'+ currentLang +'/Member/Deal/GetDealState', { 'rowGuid': dealGuid }, function (rsp, textStatus, xhr) {

            if (rsp.HasError)
            {
                showMessage(false, rsp.Message);
            }
            else
            {
                // reset
                $("#mainHelpMsg").html('');
                $("#footerBtns").html('');

                // display status
                $("#dealStatus").html(rsp.Data.StatusDisplayName);

                // =================================== buyer ===================================
                if (rsp.Data.IsBuyer)
                {
                    if (rsp.Data.StatusName == "Doing")
                    {
                        if (rsp.Data.IsBuyerPaidCurrency)
                        {
                            // btns
                            var btns = '';

                            // help msg
                            $("#bodyHelpMsg").html("<p>You paid <b>" + rsp.Data.TotalCurrencyPrice + " " + rsp.Data.PaymentMethod + "</b> to <b>" + rsp.Data.SalerUsername + " </b> (saler).</p><p>Please wait for saler to release the coins</p>");

                            // cancel btn
                            btns += "<button id='btnCancel' class='btn btn-danger'>Cancel</button>";

                            // judge btn
                            btns += "<button id='btnJudge' class='btn btn-info'>Judge</button>";

                            $("#footerBtns").html(btns);
                        }
                        else
                        {
                            // btns
                            var btns = '';

                            var bodyMsg = '';
                            bodyMsg += "<p>You must pay <b>" + rsp.Data.TotalCurrencyPrice + " " + rsp.Data.PaymentMethod + "</b> to <b>" + rsp.Data.SalerUsername + "</b> and press '<b>I Paid</b>' button.</p>";
                            bodyMsg += "<p>Please get " + rsp.Data.SalerUsername + "'s account information via chat.</p>";
                            $("#bodyHelpMsg").html(bodyMsg);

                            if (rsp.Data.IsBuyerHasTimeToPay)
                            {
                                $("#mainHelpMsg").html("<div class='alert alert-danger'><i class='fa fa-bell'></i> You just have <b>" + rsp.Data.BuyerRemainTimeToPayInMinute + " minutes</b> to pay price to <b>" + rsp.Data.SalerUsername + "</b> (saler)</div>");

                                // btn I Paid
                                btns += "<button id='btnIPaid' class='btn btn-primary'>I Paid</button>";
                            }
                            else
                            {
                                $("#mainHelpMsg").html("<div class='alert alert-danger'><i class='fa fa-exclamation-triangle'></i> You haven't any time to pay price to <b>" + rsp.Data.SalerUsername + "</b> (saler). The deal will be canceled (if it's status is not 'Canceled' now)</div>");
                            }

                            // cancel btn
                            btns += "<button id='btnCancel' class='btn btn-danger'>Cancel</button>";

                            // judge btn
                            btns += "<button id='btnJudge' class='btn btn-info'>Judge</button>";

                            $("#footerBtns").html(btns);
                        }
                    }
                    else if (rsp.Data.StatusName == "Done")
                    {
                        $("#bodyHelpMsg").html("<p>You paid <b>" + rsp.Data.TotalCurrencyPrice + " " + rsp.Data.PaymentMethod + "</b> to <b>" + rsp.Data.SalerUsername + "</b> (saler) and you get <b>" + rsp.Data.CoinAmount + " " + rsp.Data.CoinName + "</b></p>");
                    }
                    else if (rsp.Data.StatusName == "Judge")
                    {
                        $("#bodyHelpMsg").html("<p>Saler: <a href='/"+ currentLang +"/user/detail/" + rsp.Data.SalerUsername + "' class='blue-link'>" + rsp.Data.SalerUsername + "</a><br/><br/>Coin Amount: " + rsp.Data.CoinAmount + "<br/><br/> Price: " + rsp.Data.TotalCurrencyPrice + "<br/><br/> Payment Method: " + rsp.Data.PaymentMethod + "<p>");

                        $("#footerBtns").html("<a href='/"+ currentLang +"/member/ticket/detail?rowGuid=" + rsp.Data.JudgeTicketRowGuid + "' class='btn btn-info'>Judgment detail</a>");
                    }
                    else if (rsp.Data.StatusName == "Canceled")
                    {
                        $("#bodyHelpMsg").html("<p>Saler: <a href='/'+ currentLang +'/user/detail/" + rsp.Data.SalerUsername + "' class='blue-link'>" + rsp.Data.SalerUsername + "</a><br/><br/>Coin Amount: " + rsp.Data.CoinAmount + "<br/><br/> Price: " + rsp.Data.TotalCurrencyPrice + "<br/><br/> Payment Method: " + rsp.Data.PaymentMethod + "</p>");
                    }
                }

                // =================================== saler ===================================
                if (rsp.Data.IsSaler)
                {
                    if (rsp.Data.StatusName == "Doing")
                    {
                        var btns = '';

                        var bodyMsg = '';
                        bodyMsg += "<p><b>" + rsp.Data.CoinAmount + " " + rsp.Data.CoinName + "</b> of your balance have been frozen.</p>";
                        bodyMsg += "<p><b>" + rsp.Data.BuyerUsername + "</b> wants to buy <b>" + rsp.Data.CoinAmount + " " + rsp.Data.CoinName + "</b> from you</p>";
                        bodyMsg += "<p>When you received <b>" + rsp.Data.TotalCurrencyPrice + " " + rsp.Data.PaymentMethod + "</b> from <b>" + rsp.Data.BuyerUsername + "</b>, please press '<b>Release Coin</b> button.'</p>";
                        $("#bodyHelpMsg").html(bodyMsg);

                        if (rsp.Data.IsBuyerPaidCurrency)
                        {
                            // release coin btn
                            btns += "<button id='btnReleaseCoin' class='btn btn-primary'>Release coin</button>";

                            $("#mainHelpMsg").html("<div class='alert alert-success'><b>" + rsp.Data.BuyerUsername + "</b> (buyer) paid <b>" + rsp.Data.TotalCurrencyPrice + " " + rsp.Data.PaymentMethod + "</b> to you.</div>");
                        }
                        else
                        {
                            if (rsp.Data.IsBuyerHasTimeToPay)
                            {
                                // release coin btn
                                btns += "<button id='btnReleaseCoin' class='btn btn-primary'>Release coin</button>";

                                $("#mainHelpMsg").html("<div class='alert alert-danger'><i class='fa fa-bell'></i> <b>" + rsp.Data.BuyerUsername + "</b> (buyer) just has <b>" + rsp.Data.BuyerRemainTimeToPayInMinute + " minutes</b> to pay price to you.</div>");
                            }
                            else
                            {
                                $("#mainHelpMsg").html("<div class='alert alert-danger'><i class='fa fa-exclamation-triangle'></i> <b>" + rsp.Data.BuyerUsername + "</b> (buyer) hasn't any time to pay price to you. The deal will be canceled (if it's status is not 'Canceled' now)</div>");
                            }
                        }

                        // judge btn
                        btns += "<button id='btnJudge' class='btn btn-info'>Judge</button>";

                        $("#footerBtns").html(btns);
                    }
                    else if (rsp.Data.StatusName == "Done")
                    {
                        $("#bodyHelpMsg").html("<p>You get <b>" + rsp.Data.TotalCurrencyPrice + " " + rsp.Data.PaymentMethod + "</b> from <b>" + rsp.Data.BuyerUsername + "</b> (buyer) and you give <b>" + rsp.Data.CoinAmount + " " + rsp.Data.CoinName + "</b></p>");
                    }
                    else if (rsp.Data.StatusName == "Judge")
                    {
                        $("#bodyHelpMsg").html("<p>Buyer: <a href='/"+ currentLang +"/user/detail/" + rsp.Data.BuyerUsername + "' class='blue-link'>" + rsp.Data.BuyerUsername + "</a><br/><br/>Coin Amount: " + rsp.Data.CoinAmount + "<br/><br/> Price: " + rsp.Data.TotalCurrencyPrice + "<br/><br/> Payment Method: " + rsp.Data.PaymentMethod + "</p>");

                        $("#footerBtns").html("<a href='/"+ currentLang +"/Member/Ticket/detail?rowGuid=" + rsp.Data.JudgeTicketRowGuid + "' class='btn btn-info'>Judgment detail</a>");
                    }
                    else if (rsp.Data.StatusName == "Canceled")
                    {
                        $("#bodyHelpMsg").html("<p>Buyer: <a href='/"+ currentLang +"/user/detail/" + rsp.Data.BuyerUsername + "' class='blue-link'>" + rsp.Data.BuyerUsername + "</a><br/><br/>Coin Amount: " + rsp.Data.CoinAmount + "<br/><br/> Price: " + rsp.Data.TotalCurrencyPrice + "<br/><br/> Payment Method: " + rsp.Data.PaymentMethod + "</p>");
                    }
                }

                
                if (rsp.Data.StatusName == "Canceled" || rsp.Data.StatusName == "Done")
                {
                    // deactivate updating chat messages
                    isUpdatingChatMessages = false;

                    // hide chat form
                    $("#chatForm").hide();
                }
                else if (rsp.Data.StatusName == "Doing")
                {
                    // show chat form
                    $("#chatForm").show();
                }

            }

            $("#loadingAlertImg").hide();

            if (rsp.Data.StatusName == "Doing" || rsp.Data.StatusName == "Judge") {
                // run itself
                setTimeout(function () { refreshAlerts(); }, 30000);
            }

            loadUserBalances();

        }, 'json');
    }

});