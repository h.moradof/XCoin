﻿/* message */
function showMessage(isSuccess, msg)
{
    if (isSuccess) {
        $("#messageBox").removeClass("error-message-box").addClass("success-message-box");
    }
    else {
        $("#messageBox").removeClass("success-message-box").addClass("error-message-box");
    }

    $("#messageText").html(msg);

    $("#darkMessageBox").fadeIn();
}

$("#closeMessageBox").click(function () {
    $("#darkMessageBox").hide();
});