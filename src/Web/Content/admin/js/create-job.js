﻿// ++++++++++++++++ Admin Area ++++++++++++++++

$(function () {
    

    // ===================================== page_load =====================================
    fillDdlJobCategory_OnLoad();
    fillDdlState_OnLoad();
    initlalizeFileInput();

    // ===================================== load methods ===================================
    // ddl jobcategory
    function fillDdlJobCategory_OnLoad() {
        $.post("/admin/AjaxJob/GetJobCategoryGroupedDdlHtml", null, function (data) {
            $("#ddlJobCategory").html(data);

            fillDdlJobSalerType_OnLoad();
        });
    }


    // ddl jobsalertype
    function fillDdlJobSalerType_OnLoad() {
        $.post("/admin/AjaxJob/GetJobSalerTypes", null, function (data) {
            $("#ddlJobSalerType").html("");

            $(data).each(function () {
                $("#ddlJobSalerType").append("<option value='" + this.Id + "'>" + this.Title + "</option>");
            });

            showOrHideDdlJobSalerType_OnLoad();
        });
    }


    // ddl state
    function fillDdlState_OnLoad()
    {
        $.post("/Ajax/GetAllStates", null, function (data) {
            $(data).each(function () {
                $("#ddlState").append("<option value='" + this.Id + "'>" + this.Title + "</option>");
            });

            fillDdlCity_OnLoad();
        });
    }


    // ddl city
    function fillDdlCity_OnLoad() {
        var ddlStateFirstValue = $("#ddlState").val();
        //console.log("ddlStateFirstValue:" + ddlStateFirstValue);

        $("#ddlCity").html("");
        $.post("/Ajax/GetCitiesByStateId", { id: parseInt(ddlStateFirstValue) }, function (data) {
            $(data).each(function () {
                $("#ddlCity").append("<option value='" + this.Id + "'>" + this.Title + "</option>");
            });

            fillDdlArea_OnLoad();
        });
    }


    // ddl area
    function fillDdlArea_OnLoad() {
        var ddlCityFirstValue = $("#ddlCity").val();
        //console.log("ddlCityFirstValue:" + ddlCityFirstValue);

        $("#ddlArea").html("");
        $.post("/Ajax/GetAreasByCityId", { id: parseInt(ddlCityFirstValue) }, function (data) {
            $(data).each(function () {
                $("#ddlArea").append("<option value='" + this.Id + "'>" + this.Title + "</option>");
            });

            // load map
            var ddlAreaFirstValue = $("#ddlArea").val();
            //console.log("ddlAreaFirstValue:" + ddlAreaFirstValue);
            initializeGoogleMAP(parseInt(ddlAreaFirstValue));

        });
    }


    // showOrHideDdlJobSalerType
    function showOrHideDdlJobSalerType_OnLoad() {
        var ddlJobCategoryFirstValue = $("#ddlJobCategory").val();
        //console.log("ddlJobCategoryFirstValue:" + ddlJobCategoryFirstValue);

        $.post("/Ajax/HaveSalerJobs", { id: parseInt(ddlJobCategoryFirstValue) }, function (data) {
            //console.log("HaveSalerJobs:" + data);

            if (data)
                $("#divDdlJobSalerType").show();
            else
                $("#divDdlJobSalerType").hide();
        });
    }


    // ===================================== ddl.Change() =====================================
    $("#ddlState").change(function () {
        fillDdlCity();
    });


    $("#ddlCity").change(function () {
        fillDdlArea();
    });


    $("#ddlArea").change(function () {
        // load map
        var ddlAreaSelectedValue = $(this).val();
        //console.log("ddlAreaSelectedValue:" + ddlAreaSelectedValue);

        initializeGoogleMAP(parseInt(ddlAreaSelectedValue));
    });

    $("#ddlJobCategory").change(function () {
        showOrHideDdlJobSalerType();
    });


    // ===================================== methods =====================================
    function fillDdlCity() {
        var ddlStateSelectedValue = $("#ddlState").val();
        //console.log("ddlStateSelectedValue:" + ddlStateSelectedValue);

        $("#ddlCity").html("");
        $.post("/Ajax/GetCitiesByStateId", { id: parseInt(ddlStateSelectedValue) }, function (data) {
            $(data).each(function () {
                $("#ddlCity").append("<option value='" + this.Id + "'>" + this.Title + "</option>");
            });

            fillDdlArea();
        });
    }


    function fillDdlArea() {
        var ddlCitySelectedValue = $("#ddlCity").val();
        //console.log("ddlCitySelectedValue:" + ddlCitySelectedValue);

        $("#ddlArea").html("");
        $.post("/Ajax/GetAreasByCityId", { id: parseInt(ddlCitySelectedValue) }, function (data) {
            $(data).each(function () {
                $("#ddlArea").append("<option value='" + this.Id + "'>" + this.Title + "</option>");
            });

            // load map
            var ddlAreaFirstValue = $("#ddlArea").val();
            //console.log("ddlAreaFirstValue:" + ddlAreaFirstValue);
            initializeGoogleMAP(parseInt(ddlAreaFirstValue));
        });
    }


    function showOrHideDdlJobSalerType()
    {
        var ddlJobCategorySelectedValue = $("#ddlJobCategory").val();
        //console.log("ddlJobCategorySelectedValue:" + ddlJobCategorySelectedValue);

        $.post("/Ajax/HaveSalerJobs", { id: parseInt(ddlJobCategorySelectedValue) }, function (data) {
            //console.log("HaveSalerJobs:" + data);

            if (data)
                $("#divDdlJobSalerType").show();
            else
                $("#divDdlJobSalerType").hide();
        });
    }

    
    // ===================================== initialize =====================================
    // googleMap
    function initializeGoogleMAP(areaId) {

        var positionX = "1";
        var positionY = "1";

        $.post("/Admin/AjaxJob/GetAreaGooglePlace", { areaId: parseInt(areaId) }, function (data) {
            positionX = data.split('-')[0];
            positionY = data.split('-')[1];

            $("#GooglePlaceX").val(positionX);
            $("#GooglePlaceY").val(positionY);

            var position = new google.maps.LatLng(positionX, positionY);

            var myOptions = {
                zoom: 14,
                center: position,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            var map = new google.maps.Map(
                document.getElementById("map_canvas"),
                myOptions);

            var marker = new google.maps.Marker({
                position: position,
                map: map,
                title: "پین شغل",
                draggable: true
            });

            google.maps.event.addListener(marker, "dragend", function () {
                var point = marker.getPosition();
                map.panTo(point);

                document.getElementById("GooglePlaceX").value = point.lat();
                document.getElementById("GooglePlaceY").value = point.lng();
            });

        });

    }


    // fileinput
    function initlalizeFileInput() {

        $("#fileupload").fileinput({
            uploadUrl: "/Member/Job/Create",
            allowedFileExtensions: ["jpg", "gif", "png", "jpeg"],
            maxFileSize: 1000,
            maxFilesNum: 10,
            showUpload: false,
            showCaption: true,
            showPreview: false,
            elErrorContainer: "#errorBlock"
        });

        $("#fileupload").on('filebrowse', function (event, data, previewId, index, jqXHR) {
            $("#fileupload").fileinput('clear');
        });
    }

});