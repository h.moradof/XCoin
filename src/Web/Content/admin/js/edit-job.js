﻿// ++++++++++++++++ Admin Area ++++++++++++++++

$(function () {

    // ===================================== page_load =====================================
    fillDdlJobCategory_OnLoad();
    fillDdlState_OnLoad();
    initlalizeFileInput();
    initializeGoogleMAP_OnLoad();

    // ===================================== onload methods ===================================
    // ddl jobcategory
    function fillDdlJobCategory_OnLoad() {
        $.post("/admin/AjaxJob/GetJobCategoryGroupedDdlHtml", null, function (data) {
            $("#ddlJobCategory").html(data);

            // set onload selectedvalue
            var onloadJobCategoryId = $("#hfJobCategoryID").val();
            //console.log("onloadJobCategoryId:" + onloadJobCategoryId);
            $("#ddlJobCategory").val(onloadJobCategoryId);

            fillDdlJobSalerType_OnLoad();
        });

    }


    // ddl jobsalertype
    function fillDdlJobSalerType_OnLoad() {
        $.post("/admin/AjaxJob/GetJobSalerTypes", null, function (data) {
            
            $("#ddlJobSalerType").html("");

            $(data).each(function () {
                $("#ddlJobSalerType").append("<option value='" + this.Id + "'>" + this.Title + "</option>");
            });

            // set onload selectedvalue
            var onloadJobSalerTypeId = $("#hfJobSalerTypeID").val();
            //console.log("onloadJobSalerTypeId:" + onloadJobSalerTypeId);
            $("#ddlJobSalerType").val(onloadJobSalerTypeId);
            
            showOrHideDdlJobSalerType();
        });
    }


    // ddl state
    function fillDdlState_OnLoad() {
        $.post("/Ajax/GetAllStates", null, function (data) {
            $(data).each(function () {
                $("#ddlState").append("<option value='" + this.Id + "'>" + this.Title + "</option>");
            });

            // set onload selected value
            var onloadStateId = $("#hfStateID").val();
            //console.log("onloadStateId:" + onloadStateId)
            $("#ddlState").val(onloadStateId);

            fillDdlCity_OnLoad();
        });
    }


    // ddl city
    function fillDdlCity_OnLoad() {
        var onloadStateId = $("#hfStateID").val();
        $("#ddlCity").html("");

        $.post("/Ajax/GetCitiesByStateId", { id: parseInt(onloadStateId) }, function (data) {
            $(data).each(function () {
                $("#ddlCity").append("<option value='" + this.Id + "'>" + this.Title + "</option>");
            });

            // set onload selected value
            var onloadCityId = $("#hfCityID").val();
            $("#ddlCity").val(onloadCityId);

            fillDdlArea_OnLoad();
        });
    }


    // ddl area
    function fillDdlArea_OnLoad() {
        var onloadCityId = $("#hfCityID").val();
        $("#ddlArea").html("");

        $.post("/Ajax/GetAreasByCityId", { id: parseInt(onloadCityId) }, function (data) {
            $(data).each(function () {
                $("#ddlArea").append("<option value='" + this.Id + "'>" + this.Title + "</option>");
            });

            // set onload selected value
            var onloadAreaId = $("#hfAreaID").val();
            $("#ddlArea").val(onloadAreaId);

        });
    }


    // googleMap
    function initializeGoogleMAP_OnLoad() {

        var positionX = $("#Job_GooglePlaceX").val();
        var positionY = $("#Job_GooglePlaceY").val();

        var position = new google.maps.LatLng(positionX, positionY);

        var myOptions = {
            zoom: 14,
            center: position,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        var map = new google.maps.Map(
            document.getElementById("map_canvas"),
            myOptions);

        var marker = new google.maps.Marker({
            position: position,
            map: map,
            title: "پین شغل",
            draggable: true
        });

        google.maps.event.addListener(marker, "dragend", function () {
            var point = marker.getPosition();
            map.panTo(point);

            document.getElementById("Job_GooglePlaceX").value = point.lat();
            document.getElementById("Job_GooglePlaceY").value = point.lng();
        });

    }



    // ===================================== ddl.Change() =====================================
    $("#ddlState").change(function () {
        fillDdlCity();
    });


    $("#ddlCity").change(function () {
        fillDdlArea();
    });


    $("#ddlArea").change(function () {
        // load map
        var ddlAreaSelectedValue = $(this).val();
        initializeGoogleMAP(parseInt(ddlAreaSelectedValue));
    });


    $("#ddlJobCategory").change(function () {
        showOrHideDdlJobSalerType();
    });


    // ===================================== methods =====================================
    // fill ddlcity on ddlstate.change()
    function fillDdlCity() {
        var ddlStateSelectedValue = $("#ddlState").val();
        $("#ddlCity").html("");

        $.post("/Ajax/GetCitiesByStateId", { id: parseInt(ddlStateSelectedValue) }, function (data) {
            $(data).each(function () {
                $("#ddlCity").append("<option value='" + this.Id + "'>" + this.Title + "</option>");
            });

            fillDdlArea();
        });
    }


    // fill ddlarea on ddlcity.change()
    function fillDdlArea() {
        var ddlCitySelectedValue = $("#ddlCity").val();
        $("#ddlArea").html("");

        $.post("/Ajax/GetAreasByCityId", { id: parseInt(ddlCitySelectedValue) }, function (data) {
            $(data).each(function () {
                $("#ddlArea").append("<option value='" + this.Id + "'>" + this.Title + "</option>");
            });

            // load map
            var ddlAreaFirstValue = $("#ddlArea").val();
            //console.log("ddlAreaFirstValue:" + ddlAreaFirstValue);
            initializeGoogleMAP(parseInt(ddlAreaFirstValue));
        });
    }


    function showOrHideDdlJobSalerType() {
        var ddlJobCategorySelectedValue = $("#ddlJobCategory").val();
        //console.log("ddlJobCategorySelectedValue:" + ddlJobCategorySelectedValue);

        $.post("/Ajax/HaveSalerJobs", { id: parseInt(ddlJobCategorySelectedValue) }, function (data) {
            if (data)
                $("#divDdlJobSalerType").show();
            else
                $("#divDdlJobSalerType").hide();
        });
    }
    
    // ===================================== initialize =====================================

    // googleMap
    function initializeGoogleMAP(areaId) {
        
        //console.log("initializeGoogleMAP >> areaId:" + areaId);

        var placeX = "35.1";
        var placeY = "50.1";

        $.post("/Admin/AjaxJob/GetAreaGooglePlace", { areaId: parseInt(areaId) }, function (data) {
            placeX = data.split('-')[0];
            placeY = data.split('-')[1];

            // set area point
            $("#Job_GooglePlaceX").val(placeX);
            $("#Job_GooglePlaceY").val(placeY);

            var position = new google.maps.LatLng(placeX, placeY);

            var myOptions = {
                zoom: 14,
                center: position,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            var map = new google.maps.Map(
                document.getElementById("map_canvas"),
                myOptions);

            var marker = new google.maps.Marker({
                position: position,
                map: map,
                title: "پین شغل",
                draggable: true
            });

            google.maps.event.addListener(marker, "dragend", function () {
                var point = marker.getPosition();
                map.panTo(point);

                document.getElementById("Job_GooglePlaceX").value = point.lat();
                document.getElementById("Job_GooglePlaceY").value = point.lng();
            });

        });

    }


    // fileinput
    function initlalizeFileInput() {

        $("#fileupload").fileinput({
            uploadUrl: "/Member/Job/Create",
            allowedFileExtensions: ["jpg", "gif", "png", "jpeg"],
            maxFileSize: 1000,
            maxFilesNum: 10,
            showUpload: false,
            showCaption: true,
            showPreview: false,
            elErrorContainer: "#errorBlock"
        });

        $("#fileupload").on('filebrowse', function (event, data, previewId, index, jqXHR) {
            $("#fileupload").fileinput('clear');
        });
    }

});