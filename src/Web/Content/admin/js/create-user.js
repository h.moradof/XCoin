﻿$(document).ready(function () {

    // =========================== page_load() ========================

    var selectedRegistrationPeriodId = $("#RegistrationPeriodID option:selected").val();
    var firstRegistrationPeriodId = $("#RegistrationPeriodID option:first").val();
    var sendingRegistrationPeriodId = "";

    if (selectedRegistrationPeriodId != null) {
        sendingRegistrationPeriodId = selectedRegistrationPeriodId;
    }
    else {
        sendingRegistrationPeriodId = firstRegistrationPeriodId;
    }

    $.post("/AjaxRegister/HaveFacility", { registrationPeriodId: parseInt(sendingRegistrationPeriodId), facilityKeyName: "blog" }, function (data) {
        if (data) {
            $("#divBlogKeyName").css("display", "block");
        }
        else {
            $("#divBlogKeyName").css("display", "none");
        }
    });


    // =========================== ddlRegistrationType.change() ========================
    $("#RegistrationPeriodID").change(function () {

        var selectedRegistrationPeriodId = $("#RegistrationPeriodID option:selected").val();

        $.post("/AjaxRegister/HaveFacility", { registrationPeriodId: parseInt(selectedRegistrationPeriodId), facilityKeyName: "blog" }, function (data) {
            if (data) {
                $("#divBlogKeyName").css("display", "block");
            }
            else {
                $("#BlogKeyName").val('');
                $("#BlogKeyName").css("background-color", "inherit");
                $("#divBlogKeyName").css("display", "none");
            }
        });

    });


    // =================================== txt BlogKeyName.Blur() =================================
    $("#BlogKeyName").on("blur", function () {
        var _blogKeyName = $(this).val();

        if (_blogKeyName.length > 2) {
            $.post("/AjaxRegister/IsValidBlogKeyName", { blogKeyName: _blogKeyName }, function (data) {
                if (data) {
                    $("#BlogKeyName").css("background-color", "#88E371");
                }
                else {
                    $("#BlogKeyName").css("background-color", "#FF4949");
                }
            });
        }
        else {
            $("#BlogKeyName").css("background-color", "#FFD391");
        }

    });


    // =================================== txt ParentID.keyup() =================================
    $("#ParentID").keyup(function () {
        var _parentId = $(this).val();

        if (_parentId.length > 0) {
            if (!isNaN(_parentId)) {
                $.post("/Ajax/GetPresenterFullName", { id: parseInt(_parentId) }, function (data) {
                    if (data == "notfound") {
                        $("#lblPresenter").text("معرف یافت نشد");
                    }
                    else {
                        $("#lblPresenter").text("معرف: " + data);
                    }
                });
            }
            else {
                $("#lblPresenter").text("کد معرف باید یک مقدار عددی باشد");
            }
        }
        else {
            $("#lblPresenter").text("لطفا کد معرف را وارد نمایید");
        }
    });


    // =================================== btn CheckBlogKeyName.click() =================================
    $("#btnCheckBlogKeyName").click(function () {
        blogKeyNameValidation();
    });


    // ========================================= methods =========================================
    function blogKeyNameValidation() {
        var _blogKeyName = $("#BlogKeyName").val();

        if (_blogKeyName.length > 2) {
            $.post("/AjaxRegister/IsValidBlogKeyName", { blogKeyName: _blogKeyName }, function (data) {
                if (data) {
                    $("#BlogKeyName").css("background-color", "#88E371");
                }
                else {
                    $("#BlogKeyName").css("background-color", "#FF4949");
                }
            });
        }
        else {
            $("#BlogKeyName").css("background-color", "#FFD391");
        }
    }


});