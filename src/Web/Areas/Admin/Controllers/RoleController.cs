﻿using DatabaseContext.Context;
using DomainModels.Entities.Access;
using Infrastructure.Common;
using Infrastructure.Exceptions;
using Infrastructure.Messages;
using Services.Interfaces;
using System.Linq;
using System.Web.Mvc;
using ViewModels.Access;
using ViewModels.Base;

namespace Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "administrators")]
    public class RoleController : Controller
    {

        #region props
        private readonly IUnitOfWork _uow;
        private readonly IRoleService _roleService;
        private readonly IPermissionService _permissionService;
        private readonly IRolePermissionService _rolePermissionService;
        #endregion

        #region ctor
        public RoleController(
            IUnitOfWork uow,
            IRoleService roleService,
            IPermissionService permissionService,
            IRolePermissionService rolePermissionService)
        {
            _uow = uow;
            _roleService = roleService;
            _permissionService = permissionService;
            _rolePermissionService = rolePermissionService;
        }
        #endregion

        #region private methods

        private BaseCheckboxItem[] GetCheckBoxItems()
        {
            var permissions = _permissionService.GetList(PermissionType.Site, false);
            return permissions.Select(c => new BaseCheckboxItem { Id = c.Id, Name = c.Name }).ToArray();
        }

        #endregion



        // ---------------------------------------------- List
        public ActionResult Index(int? page = 1)
        {
            var roles = _roleService.GetAllEditableRoles(page.Value);
            return View(roles);
        }


        // ---------------------------------------------- Create
        [HttpGet]
        public ActionResult Create()
        {
            ViewBag.Permissions = GetCheckBoxItems();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(RoleCreateViewModel model)
        {
            try
            {
                ViewBag.Permissions = GetCheckBoxItems();

                if (!ModelState.IsValid)
                    return View(model);

                _roleService.Add(model);
                _uow.SaveChanges();

                TempData["Message"] = new Message { IsSuccess = true, MessageBody = MessageCoreManager.MessageType.SuccessUpdate.Description() };
                return RedirectToAction("Index");
            }
            catch (CustomException ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View(model);
            }

        }



        // ---------------------------------------------- Edit
        [HttpGet]
        public ActionResult Edit(long id)
        {
            var role = _roleService.Find(id);

            if (role == null)
                return HttpNotFound();

            ViewBag.Permissions = GetCheckBoxItems();

            var permissionIds = _rolePermissionService.GetPermissionIdsOfRole(id);

            return View(new RoleEditViewModel { Id = role.Id, Name = role.DisplayName, PermissionIds = permissionIds });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(RoleEditViewModel model)
        {
            try
            {
                ViewBag.Permissions = GetCheckBoxItems();

                if (!ModelState.IsValid)
                    return View(model);

                var updatingItem = _roleService.Find(model.Id);
                updatingItem.DisplayName = model.Name;

                _roleService.Edit(updatingItem, model.PermissionIds);
                _uow.SaveChanges();

                TempData["Message"] = new Message { IsSuccess = true, MessageBody = MessageCoreManager.MessageType.SuccessUpdate.Description() };
                return RedirectToAction("Index");
            }
            catch (CustomException ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View(model);
            }
        }


    }
}