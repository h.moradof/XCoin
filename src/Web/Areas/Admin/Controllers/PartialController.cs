﻿using Infrastructure.Cache.Interfaces;
using Services.Interfaces;
using Web.Security.Authentication;
using System.Web.Mvc;

namespace Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "administrators")]
    public class PartialController : Controller
    {

        #region props

        private readonly IUserService _userService;
        private readonly ICurrentUser _currentUser;
        private readonly ISeoSettingCacheManager _siteSeoSettingCacheManager;
        private readonly IMenuService _menuService;
        private readonly ISiteVisitService _siteVisitService;
        private readonly ICurrentUser _currnetUser;

        #endregion

        #region ctor

        public PartialController(
            IUserService userService,
            ICurrentUser currentUser,
            ISeoSettingCacheManager siteSeoSettingCacheManager,
            IMenuService menuService,
            ISiteVisitService siteVisitService,
            ICurrentUser currnetUser)
        {
            _userService = userService;
            _currentUser = currentUser;
            _siteSeoSettingCacheManager = siteSeoSettingCacheManager;
            _menuService = menuService;
            _siteVisitService = siteVisitService;
            _currnetUser = currnetUser;
        }

        #endregion


        [ChildActionOnly]
        public PartialViewResult _RightMenu()
        {
            var menuHtml = _menuService.GetPanelMenuHtml(_currentUser.Id);
            return PartialView(model: menuHtml);
        }
        

        [ChildActionOnly]
        public PartialViewResult _DashboardTitle()
        {
            var siteSetting = _siteSeoSettingCacheManager.GetDefaultSetting();

            return PartialView(siteSetting);
        }

    }
}