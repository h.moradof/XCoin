﻿using DatabaseContext.Context;
using DomainModels.Entities.Site;
using Infrastructure.Common;
using Infrastructure.File;
using Infrastructure.Messages;
using Services.Interfaces;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Mvc;
using ViewModels.Access;
using ViewModels.Base;
using ViewModels.Site;

namespace Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "administrators")]
    public class SitePageController : Controller
    {

        #region props

        private readonly IUnitOfWork _uow;
        private readonly ISitePageService _sitePageService;

        #endregion

        #region ctor

        public SitePageController(
            IUnitOfWork uow, 
            ISitePageService sitePageService)
        {
            _uow = uow;
            _sitePageService = sitePageService;
        }

        #endregion


        // ---------------------------------------------- List
        [HttpGet]
        public ActionResult Index(int? page = 1)
        {
            var pages = _sitePageService.GetAll(page.Value);

            return View(pages);
        }



        // ---------------------------------------------- Detail
        [HttpGet]
        public ActionResult Detail(long id)
        {
            var sitePage =  _sitePageService.Find(id);

            if (sitePage == null)
                return HttpNotFound();

            return View(sitePage);
        }




        // ---------------------------------------------- Create
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(SitePage model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var photoName = FileManager.UploadFirstValidPhoto(Request.Files, EntityName.Photo, false);
            model.PhotoName = photoName;

            _sitePageService.Add(model);
            _uow.SaveChanges();

            TempData["Message"] = new Message { IsSuccess = true, MessageBody = MessageCoreManager.MessageType.SuccessInsert.Description() };
            return RedirectToAction("Index");
        }




        // ---------------------------------------------- Edit
        [HttpGet]
        public ActionResult Edit(long id)
        {
            var sitePage = _sitePageService.Find(id);
            
            return View(sitePage);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(SitePage model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var updatingItem = _sitePageService.Find(model.Id);

            var photoName = FileManager.UploadFirstValidPhoto(Request.Files, EntityName.Photo, false);
            if (!string.IsNullOrEmpty(photoName))
            {
                FileManager.DeleteFile(model.PhotoName, EntityName.Photo); // delete file from H.D.D
                model.PhotoName = photoName;
            }

            model.CopyTo<SitePage>(ref updatingItem);

            _sitePageService.Edit(updatingItem);
            _uow.SaveChanges();

            TempData["Message"] = new Message { IsSuccess = true, MessageBody = MessageCoreManager.MessageType.SuccessUpdate.Description() };
            return RedirectToAction("Index");
        }


	}
}