﻿using Services.Interfaces;
using System.Threading.Tasks;
using System.Web.Mvc;
using Infrastructure.Common;
using Infrastructure.Messages;
using DatabaseContext.Context;
using System.Threading;
using DomainModels.Entities.Setting;

namespace Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "administrators")]
    public class SeoSettingController : Controller
    {

        #region props

        private readonly IUnitOfWork _uow;
        private readonly ISeoSettingService _siteSeoSettingService;

        #endregion

        #region ctor

        public SeoSettingController(IUnitOfWork uow, ISeoSettingService siteSeoSettingService)
        {
            _siteSeoSettingService = siteSeoSettingService;
            _uow = uow;
        }

        #endregion



        // ---------------------------------------------- Edit
        [HttpGet]
        public async Task<ActionResult> Edit(long id)
        {
            var siteSetting = await _siteSeoSettingService.FindAsync(id);

            if (siteSetting == null)
                return HttpNotFound();

            return View(siteSetting);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(SeoSetting model, CancellationToken cancellationToken)
        {
            if (!ModelState.IsValid)
                return View(model);

            var updatingItem = await _siteSeoSettingService.FindAsync(model.Id);

            model.CopyTo<SeoSetting>(ref updatingItem);

            await _siteSeoSettingService.EditAsync(updatingItem);
            await _uow.SaveChangesAsync(cancellationToken);

            TempData["Message"] = new Message { IsSuccess = true, MessageBody = MessageCoreManager.MessageType.SuccessUpdate.Description() };
            return RedirectToAction("Show", "Message", new { area = "Admin" });

        }


	}
}