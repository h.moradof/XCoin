﻿using DatabaseContext.Context;
using DomainModels.Entities.Access;
using Infrastructure.Common;
using Infrastructure.Exceptions;
using Infrastructure.Messages;
using Services.Interfaces;
using System.Web.Mvc;
using ViewModels.HumanResource;

namespace Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "administrators")]
    public class UserController : Controller
    {

        #region props
        private readonly IUnitOfWork _uow;
        private readonly IUserService _userService;
        private readonly IRoleService _roleService;
        #endregion

        #region ctor
        public UserController(
            IUnitOfWork uow,
            IUserService userService,
            IRoleService roleService)
        {
            _uow = uow;
            _userService = userService;
            _roleService = roleService;
        }
        #endregion



        // ---------------------------------------------- List
        [HttpGet]
        public ActionResult Index(int? page = 1)
        {
            var vmUsers = _userService.GetAllByRoleNames(new RoleName[] { RoleName.Employees }, page.Value);

            return View(vmUsers);
        }




        // ---------------------------------------------- Detail
        [HttpGet]
        public ActionResult Detail(long id)
        {
            var model = _userService.Find(id);
            return View(model);
        }



        // ---------------------------------------------- Create
        [HttpGet]
        public ActionResult Create()
        {
            ViewBag.EmployeeRoles = _roleService.GetListByRoleNames(RoleName.Employees);
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(UserCreateViewModel model)
        {
            try
            {

                ViewBag.EmployeeRoles = _roleService.GetListByRoleNames(RoleName.Employees);

                if (!ModelState.IsValid)
                    return View(model);

                _userService.Add(model);
                _uow.SaveChanges();

                TempData["Message"] = new Message { IsSuccess = true, MessageBody = MessageCoreManager.MessageType.SuccessUpdate.Description() };
                return RedirectToAction("Index");
            }
            catch (CustomException ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View(model);
            }

        }



        // ---------------------------------------------- Edit
        [HttpGet]
        public ActionResult Edit(long id)
        {
            try
            {
                var vmUserEdit = _userService.GetEditViewModel(id);

                if (vmUserEdit == null)
                    return HttpNotFound();

                ViewBag.EmployeeRoles = _roleService.GetListByRoleNames(RoleName.Employees);

                return View(vmUserEdit);
            }
            catch (CustomException ex)
            {
                TempData["Message"] = new Message { IsSuccess = false, MessageBody = ex.Message };
                return RedirectToAction("Show", "Message");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(UserEditViewModel model)
        {
            try
            {
                ViewBag.EmployeeRoles = _roleService.GetListByRoleNames(RoleName.Employees);

                if (!ModelState.IsValid)
                    return View(model);

                ValidateEditViewModel(model);

                _userService.Edit(model);
                _uow.SaveChanges();

                TempData["Message"] = new Message { IsSuccess = true, MessageBody = MessageCoreManager.MessageType.SuccessUpdate.Description() };
                return RedirectToAction("Index");
            }
            catch (CustomException ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View(model);
            }
        }

        private void ValidateEditViewModel(UserEditViewModel model)
        {
            if (model.WantChangePassword && string.IsNullOrEmpty(model.Password))
            {
                throw new CustomException("please enter password");
            }
        }


    }
}