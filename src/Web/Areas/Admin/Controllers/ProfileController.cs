﻿using DatabaseContext.Context;
using Infrastructure.Common;
using Infrastructure.Messages;
using Services.Interfaces;
using ViewModels.Account;
using Web.Security;
using Web.Security.Authentication;
using System.Web.Mvc;
using System.Web.Security;
using Infrastructure.Security.Password;

namespace Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "administrators")]
    public class ProfileController : Controller
    {

        #region props

        private readonly IUnitOfWork _uow;
        private readonly IUserService _userService;
        private readonly ICurrentUser _currentUser;

        #endregion

        #region ctor

        public ProfileController(
            IUnitOfWork uow,
            IUserService userService,
            ICurrentUser currentUser)
        {
            _uow = uow;
            _userService = userService;
            _currentUser = currentUser;
        }

        #endregion


        // ---------------------------------------------- Edit
        [HttpGet]
        public ActionResult Edit()
        {
            var vmProfile = _userService.GetProfile(_currentUser.Id);

            if (vmProfile == null)
                return HttpNotFound();

            return View(vmProfile);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ProfileViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            ModelState.Clear();

            model.UserId = _currentUser.Id;

            // validate username and password
            if (!IsValidProfileViewModel(model))
                return View(model);

            _userService.EditProfile(model);
            _uow.SaveChanges();

            TempData["Message"] = new Message { IsSuccess = true, MessageBody = MessageCoreManager.MessageType.SuccessUpdate.Description() };
            return RedirectToAction("Show", "Message", new { area = "Admin" });
        }


        private bool IsValidProfileViewModel(ProfileViewModel vmProfile)
        {

            // check password
            if (vmProfile.WantUpdatePassword)
            {
                if (string.IsNullOrEmpty(vmProfile.Password))
                {
                    ModelState.AddModelError("", "Password is required");
                    return false;
                }

                if (vmProfile.Password != vmProfile.RePassword)
                {
                    ModelState.AddModelError("", "Password and confirm password are not the same");
                    return false;
                }

                var passwordValidateResult = PasswordValidator.Validate(vmProfile.Password);

                if (passwordValidateResult == PasswordValidateResult.IsInvalidLengthIsSmall)
                {
                    ModelState.AddModelError("", string.Format("Password langth must be greater than {0} characters", PasswordValidator.MinimumLength));
                    return false;
                }
                else if (passwordValidateResult == PasswordValidateResult.IsInvalidNotContainNumber)
                {
                    ModelState.AddModelError("", "Password must include one digit at least");
                    return false;
                }
            }

            return true;
        }

    }
}