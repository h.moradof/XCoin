﻿using Services.Interfaces;
using System.Web.Mvc;
using Web.Security.Authentication;

namespace Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "administrators")]
    public class DashboardController : Controller
    {

        #region props
        private readonly ISiteVisitService _siteVisitService;
        private readonly ICurrentUser _currentUser;
        #endregion

        #region ctor
        public DashboardController(
            ISiteVisitService siteVisitService,
            ICurrentUser currentUser)
        {
            _siteVisitService = siteVisitService;
            _currentUser = currentUser;
        }
        #endregion


        [HttpGet]
        public ActionResult Show()
        {
            return View();
        }
   

        [ChildActionOnly]
        public PartialViewResult _SiteVisitReport()
        {
            var vmSiteVisitReport = _siteVisitService.GetSiteVisitReport();

            return PartialView(vmSiteVisitReport);
        }

    }
}