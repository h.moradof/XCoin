﻿using DatabaseContext.Context;
using DomainModels.Entities.HumanResource;
using Services.Interfaces;
using System;
using System.Globalization;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using ViewModels.Account;
using Infrastructure.Security;
using Infrastructure.Cache.Interfaces;
using Infrastructure.Common;
using Infrastructure.Mail.Interfaces;
using Infrastructure.Exceptions;
using Google.Authenticator;
using DomainModels.Entities.Access;
using Infrastructure.Security.Password;

namespace Web.Areas.Account.Controllers
{
    [AllowAnonymous]
    public class HomeController : Controller
    {

        #region props

        private readonly IUnitOfWork _uow;
        private readonly IUserService _userService;
        private readonly IMailSender _mailSender;
        private readonly ISeoSettingCacheManager _siteSeoSettingCacheManager;
        private readonly IApplicationSettingCacheManager _applicationSettingCacheManager;

        #endregion

        #region ctor

        public HomeController(
            IUnitOfWork uow,
            IUserService userService,
            IMailSender mailSender,
            ISeoSettingCacheManager siteSeoSettingCacheManager,
            IApplicationSettingCacheManager applicationSettingCacheManager)
        {
            _uow = uow;
            _userService = userService;
            _mailSender = mailSender;
            _siteSeoSettingCacheManager = siteSeoSettingCacheManager;
            _applicationSettingCacheManager = applicationSettingCacheManager;
        }

        #endregion


        // common
        private void ValidatePassword(string password)
        {
            var passwordValidateResult = PasswordValidator.Validate(password);

            if (passwordValidateResult == PasswordValidateResult.IsInvalidLengthIsSmall) { throw new CustomException(string.Format("Password length must be greater than {0} characters", PasswordValidator.MinimumLength)); }

            if (passwordValidateResult == PasswordValidateResult.IsInvalidNotContainNumber) { throw new CustomException("Password must have a number at least"); }

            if (passwordValidateResult != PasswordValidateResult.IsValid) { throw new CustomException("an exception occured"); }
        }


        // -------------------------------- Sign Up
        #region signup
        [HttpGet]
        public ActionResult SignUp()
        {
            return GetSignUpView(new SignUpViewModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SignUp(SignUpViewModel vmSignUp)
        {
            try
            {
                if (!ModelState.IsValid) { return GetSignUpView(vmSignUp); }
                ValidatePassword(vmSignUp.Password);

                var googleAuthenticatorKey = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 10);
                _userService.SignUp(vmSignUp, googleAuthenticatorKey);
                _uow.SaveChanges();

                SetupCode setupInfo = SetUpGoogleAuthenricator(vmSignUp.EmailAddress, googleAuthenticatorKey);

                ViewBag.QrCodeImageUrl = setupInfo.QrCodeSetupImageUrl;
                ViewBag.ManualEntrySetupCode = setupInfo.ManualEntryKey;

                return GetSignUpView(new SignUpViewModel());
            }
            catch (CustomException ex)
            {
                ModelState.AddModelError("", ex.Message);
                return GetSignUpView(vmSignUp);
            }
        }


        private SetupCode SetUpGoogleAuthenricator(string emailAddress, string googleAuthenticatorKey)
        {
            var applicationSetting = _applicationSettingCacheManager.Get();
            var tfa = new TwoFactorAuthenticator();
            var setupInfo = tfa.GenerateSetupCode(applicationSetting.WebsiteDomain, emailAddress, googleAuthenticatorKey, 300, 300);
            return setupInfo;
        }


        // get signup view (for captcha)
        private ActionResult GetSignUpView(SignUpViewModel vmSignUp)
        {
            vmSignUp.cc = CaptchaManager.GenerateHash(new RandomMaker().CreateRandomNumbers(4));
            return View(vmSignUp);
        }
        #endregion


        // -------------------------------- Sign In
        #region signin

        [HttpGet]
        public ActionResult SignIn()
        {
            return GetSignInView(new SignInViewModel());
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SignIn(SignInViewModel vmSignIn)
        {
            try
            {
                if (!ModelState.IsValid) { return GetSignInView(vmSignIn); }
                if (!CaptchaManager.IsValid(vmSignIn.cc, vmSignIn.Captcha))  { throw new CustomException("Security code is not valid"); }

                var vmResult = _userService.SignIn(vmSignIn.Username, vmSignIn.Password);

                ValidateUserOnSignIn(vmResult.User);
                
                //ValidateTemporaryPassword(vmResult.User.GoogleAuthenticatorKey, vmSignIn.TemporaryPassword);
                
                //_userService.ValidateDuplicateTemporaryPassword(vmResult.User.Id, vmSignIn.TemporaryPassword);
                //_uow.SaveChanges();

                SetupFormsAuthTicket(vmResult.User, vmSignIn.RememberMe);

                if (vmResult.Role.Name == RoleName.Administrators)
                {
                    // administrators
                    return RedirectToAction("Show", "Dashboard", new { area = "Admin" });
                }
                else
                {
                    if (string.IsNullOrEmpty(vmSignIn.ReturnUrl))
                    {
                        // customers
                        return RedirectToAction("Index", "Dashboard", new { area = "Member" });
                    }
                    else
                    {
                        return Redirect(vmSignIn.ReturnUrl);
                    }
                }
            }
            catch (CustomException ex)
            {
                ModelState.AddModelError("", ex.Message);
                return GetSignInView(vmSignIn);
            }

        }

        private void ValidateTemporaryPassword(string googleAuthenticatorKey, string temporaryPassword)
        {
            var tfa = new TwoFactorAuthenticator();
            var isValid = tfa.ValidateTwoFactorPIN(googleAuthenticatorKey, temporaryPassword);
            if (!isValid) { throw new CustomException("Temporary password was not valid."); }
        }

        private void ValidateUserOnSignIn(User user)
        {
            if (user == null) { throw new CustomException("invalid username or password"); }
            if (user.AccountStatus == AccountStatus.Baned) { throw new CustomException("Your account was accepted. Reason: " + user.BanReason); }
            if (user.AccountStatus == AccountStatus.Deactive) { throw new CustomException("Your account is not active."); }
        }

        // get signin view (for captcha)
        private ActionResult GetSignInView(SignInViewModel vmSignIn)
        {
            vmSignIn.cc = CaptchaManager.GenerateHash(new RandomMaker().CreateRandomNumbers(4));
            return View(vmSignIn);
        }

        private void SetupFormsAuthTicket(User user, bool persistanceFlag)
        {
            var userData = user.Id.ToString(CultureInfo.InvariantCulture);

            var authTicket = new FormsAuthenticationTicket(1, //version
                                user.Username, // name
                                DateTime.Now,             //creation
                                DateTime.Now.AddMinutes(30), //Expiration
                                persistanceFlag, //Persistent
                                userData  // userId
                                );

            var encryptedTicket = FormsAuthentication.Encrypt(authTicket);
            Response.Cookies.Add(new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket));
        }

        #endregion


        // -------------------------------- Forget Password
        #region forgetpassword

        [HttpGet]
        public ActionResult ForgetPassword()
        {
            return GetForgetPasswordView(new ForgetPasswordViewModel());
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ForgetPassword(ForgetPasswordViewModel vmForgetPassword)
        {
            try
            {
                if (!ModelState.IsValid) { return GetForgetPasswordView(vmForgetPassword); }
                ValidateForgetPaddwordViewModel(vmForgetPassword);

                bool emailSent = SendForgetPasswordEmail(vmForgetPassword);
                if (!emailSent) { throw new CustomException("Some error occored while sending email"); }

                return View("~/Areas/Account/Views/Home/SuccessForgetPassword.cshtml");
            }
            catch (CustomException ex)
            {
                ModelState.AddModelError("", ex.Message);
                return GetForgetPasswordView(vmForgetPassword);
            }
        }

        private void ValidateForgetPaddwordViewModel(ForgetPasswordViewModel vmForgetPassword)
        {
            if (string.IsNullOrEmpty(vmForgetPassword.EmailAddress) || string.IsNullOrWhiteSpace(vmForgetPassword.EmailAddress)) {
                throw new CustomException("Email address is required");
            }

            if (!CaptchaManager.IsValid(vmForgetPassword.cc, vmForgetPassword.Captcha)) {
                throw new CustomException("Security image text is invalid");
            }

            // validate email address
            vmForgetPassword.EmailAddress = vmForgetPassword.EmailAddress.Trim();
            if (!_userService.IsValidEmail(vmForgetPassword.EmailAddress)) {
                throw new CustomException("Email address format is invalid");
            }
        }

        private bool SendForgetPasswordEmail(ForgetPasswordViewModel vmForgetPassword)
        {
            bool flag = false;
            var userData = _userService.GetForgetPasswordDataByEmail(vmForgetPassword.EmailAddress);
            var siteSetting = _siteSeoSettingCacheManager.GetDefaultSetting();
            string subject = string.Format("{0} - فراموشی کلمه عبور", siteSetting.Title);

            var websiteDomain = _applicationSettingCacheManager.Get().WebsiteDomain;

            // send email
            string body = string.Format("", siteSetting.Title, websiteDomain, userData.Code, userData.FullName, userData.Username);

            flag = _mailSender.Send(siteSetting.Title, vmForgetPassword.EmailAddress, subject, body);
            return flag;
        }


        // get forget password view (for captcha)
        private ActionResult GetForgetPasswordView(ForgetPasswordViewModel vmForgetPassword)
        {
            vmForgetPassword.cc = CaptchaManager.GenerateHash(new RandomMaker().CreateRandomNumbers(4));
            return View("~/Areas/Account/Views/Home/ForgetPassword.cshtml", vmForgetPassword);
        }

        #endregion


        // -------------------------------- Set Password
        #region setpassword

        [HttpGet]
        public ActionResult SP(string rpc)
        {
            try
            {
                //ValidateResetPasswordCode(rpc);

                return GetSetPasswordView(new SetPasswordViewModel());
            }
            catch (CustomException)
            {
                return HttpNotFound();
            }
        }

        private void ValidateResetPasswordCode(string resetPasswordCode)
        {
            if (string.IsNullOrEmpty(resetPasswordCode)) { throw new CustomException(); }

            var guidCode = new Guid();
            if (!Guid.TryParse(resetPasswordCode, out guidCode)) { throw new CustomException(); }

            // validate passwordResetCode in db
            if (!_userService.IsValidResetPasswordCode(guidCode)) { throw new CustomException(); }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SP(SetPasswordViewModel vmSetPassword)
        {
            try
            {
                if (!ModelState.IsValid) { return GetSetPasswordView(vmSetPassword); }
                    
                ValidateSetPasswordViewModel(vmSetPassword);

                long userId = _userService.GetUserIdByResetPasswordCode(vmSetPassword.ResetPasswordCode);

                vmSetPassword.UserId = userId;
                _userService.SetNewPassword(vmSetPassword);
                _uow.SaveChanges();

                return View("~/Areas/Account/Views/Home/SuccessSetPassword.cshtml");
            }
            catch (CustomException ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View(vmSetPassword);
            }
        }

        // setpassword validation
        private void ValidateSetPasswordViewModel(SetPasswordViewModel vmSetPassword)
        {
            if (!CaptchaManager.IsValid(vmSetPassword.cc, vmSetPassword.Captcha)) { throw new CustomException("Invalid security code"); }
            if (vmSetPassword.Password != vmSetPassword.RePassword) { throw new CustomException("Password and confirm password are not the same"); }
            if (!_userService.IsValidResetPasswordCode(vmSetPassword.ResetPasswordCode)) { throw new CustomException("Invalid input parameters"); }

            ValidatePassword(vmSetPassword.Password);
        }


        // get setpassword view (for captcha)
        private ActionResult GetSetPasswordView(SetPasswordViewModel vmSetPassword)
        {
            vmSetPassword.cc = CaptchaManager.GenerateHash(new RandomMaker().CreateRandomNumbers(4));

            return View("~/Areas/Account/Views/Home/SP.cshtml", vmSetPassword);
        }

        #endregion


        // -------------------------------- Sign Out
        #region signout

        public ActionResult SignOut()
        {
            FormsAuthentication.SignOut();
            return View();
        }

        #endregion

    }
}