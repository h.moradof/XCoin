﻿using System.Web.Mvc;

namespace Web.Areas.Account
{
    public class AccountAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Account";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {

            context.MapRoute(
                "Account_forgetpassword",
                "{lang}/Account/forgetpassword",
                new { controller = "Home", action = "ForgetPassword", lang = "en" },
                namespaces: new string[] { "Web.Areas.Account.Controllers" }
            );

            context.MapRoute(
                "Account_singup",
                "{lang}/Account/signup",
                new { controller = "Home", action = "SignUp", lang = "en" },
                namespaces: new string[] { "Web.Areas.Account.Controllers" }
            );

            context.MapRoute(
                "Account_singin",
                "{lang}/Account/signin",
                new { controller = "Home", action = "SignIn", lang = "en" },
                namespaces: new string[] { "Web.Areas.Account.Controllers" }
            );

            context.MapRoute(
                "Account_singout",
                "{lang}/Account/signout",
                new { controller = "Home", action = "SignOut", lang = "en" },
                namespaces: new string[] { "Web.Areas.Account.Controllers" }
            );

            context.MapRoute(
                "Account_default",
                "{lang}/Account/{controller}/{action}/{id}",
                new { controller = "Home", action = "SignIn", id = UrlParameter.Optional, lang = "en" },
                namespaces: new string[] { "Web.Areas.Account.Controllers" }
            );

        }
    }
}