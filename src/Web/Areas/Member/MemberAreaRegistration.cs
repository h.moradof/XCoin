﻿using System.Web.Mvc;

namespace Web.Areas.Member
{
    public class MemberAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Member";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
               "Member_default_lang",
               "{lang}/Member/{controller}/{action}/{id}",
               new { action = "Index", id = UrlParameter.Optional, lang = "en" },
               namespaces: new string[] { "Web.Areas.Member.Controllers" }
           );

            context.MapRoute(
                "Member_default",
                "Member/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
                namespaces: new string[] { "Web.Areas.Member.Controllers" }
            );
        }
    }
}