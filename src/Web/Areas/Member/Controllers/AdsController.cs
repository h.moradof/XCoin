﻿using DatabaseContext.Context;
using Infrastructure.Exceptions;
using Infrastructure.Messages;
using Services.Interfaces;
using System.Web.Mvc;
using ViewModels.Trade;
using Web.Security.Authentication;

namespace Web.Areas.Member.Controllers
{
    [Authorize(Roles = "registeredusers,verifiedusers")]
    public class AdsController : Controller
    {

        #region props
        private readonly IUnitOfWork _uow;
        private readonly IAdsService _adsService;
        private readonly ICurrentUser _currentUser;
        private readonly ICountryService _countryService;
        private readonly ICoinService _coinService;
        private readonly IPaymentMethodService _paymentMethodService;
        private readonly ICurrencyService _currencyService;
        #endregion

        #region ctor
        public AdsController(
            IUnitOfWork uow, 
            IAdsService adsService,
            ICurrentUser currentUser,
            ICountryService countryService,
            ICoinService coinService,
            IPaymentMethodService paymentMethodService,
            ICurrencyService currencyService)
        {
            _uow = uow;
            _adsService = adsService;
            _currentUser = currentUser;
            _countryService = countryService;
            _coinService = coinService;
            _paymentMethodService = paymentMethodService;
            _currencyService = currencyService;
        }
        #endregion


        // ------------------------------------------- List
        [HttpGet]
        public ActionResult Index(string lang, bool isSell, int? page = 1)
        {
            var userAds = _adsService.GetAllUserAdsByLang(lang, _currentUser.Id, isSell, page.Value, 15);
            return View(userAds);
        }



        // ------------------------------------------- Create
        [HttpGet]
        public ActionResult Create()
        {
            ViewBag.Countries = _countryService.GetList();
            ViewBag.Coins = _coinService.GetList();
            ViewBag.Currencies = _currencyService.GetList();
            ViewBag.PaymentMethods = _paymentMethodService.GetList();

            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(string lang, AdsCreateViewModel model)
        {
            ViewBag.Countries = _countryService.GetList();
            ViewBag.Coins = _coinService.GetList();
            ViewBag.Currencies = _currencyService.GetList();
            ViewBag.PaymentMethods = _paymentMethodService.GetList();

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            try
            {
                // prevent to create duplicate adses with same currency and same country 
                _adsService.Add(model, _currentUser.Id);
                _uow.SaveChanges();

                TempData["Message"] = new Message { IsSuccess = true, MessageBody = WebResources.Message.SuccessInsert };
                return RedirectToAction("Index", new { issell = model.IsSell, lang = lang });
            }
            catch (CustomException ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View(model);
            }
        }



        // ------------------------------------------- Edit
        [HttpGet]
        public ActionResult Edit(long id)
        {
            ViewBag.Countries = _countryService.GetList();
            ViewBag.Coins = _coinService.GetList();
            ViewBag.Currencies = _currencyService.GetList();
            ViewBag.PaymentMethods = _paymentMethodService.GetList();

            var model = _adsService.GetEditViewModel(id , _currentUser.Id);

            return View(model);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(string lang, AdsEditViewModel model)
        {
            ViewBag.Countries = _countryService.GetList();
            ViewBag.Coins = _coinService.GetList();
            ViewBag.Currencies = _currencyService.GetList();
            ViewBag.PaymentMethods = _paymentMethodService.GetList();

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            try
            {
                // prevent to create duplicate adses with same currency and same country
                var updatedModel =_adsService.Edit(model, _currentUser.Id);
                _uow.SaveChanges();

                TempData["Message"] = new Message { IsSuccess = true, MessageBody = WebResources.Message.SuccessUpdate };
                return RedirectToAction("Index", new { issell = updatedModel.IsSell, lang = lang });
            }
            catch (CustomException ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View(model);
            }
        }


    }
}