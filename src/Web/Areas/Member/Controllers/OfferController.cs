﻿using DatabaseContext.Context;
using Infrastructure.Exceptions;
using Services.Interfaces;
using System;
using System.Web.Mvc;
using ViewModels.Base;
using Web.Security.Authentication;

namespace Web.Areas.Member.Controllers
{
    [Authorize(Roles = "verifiedusers")]
    public class OfferController : Controller
    {

        #region props
        private readonly IUnitOfWork _uow;
        private readonly IOfferService _offerService;
        private readonly ICurrentUser _currentUser;
        private readonly IDealService _dealService;
        private readonly IUserService _userService;
        #endregion

        #region ctor
        public OfferController(
            IUnitOfWork uow,
            IOfferService offerService,
            ICurrentUser currentUser,
            IDealService dealService,
            IUserService userService)
        {
            _uow = uow;
            _offerService = offerService;
            _currentUser = currentUser;
            _dealService = dealService;
            _userService = userService;
        }
        #endregion


        // ------------------------------------- Create
        public JsonResult Create(string lang, long adsId, decimal amount)
        {
            try
            {
                // create offer
                var offer = _offerService.Add(adsId, amount, _currentUser.Id);

                // create deal on offer above
                var deal = _dealService.Add(offer);

                // check and lock balance
                _userService.LockBalance(deal);

                _uow.SaveChanges();

                return Json(new ApiResultViewModel { Data = string.Format("/{0}/member/deal?rowGuid={1}", lang, deal.RowGuid) });
            }
            catch (CustomException ex)
            {
                return Json(new ApiResultViewModel { HasError = true , Message = ex.Message });
            }
            catch (Exception ex)
            {
                return Json(new ApiResultViewModel { HasError = true, Message = "An error occoured" });
            }
        }
    }
}