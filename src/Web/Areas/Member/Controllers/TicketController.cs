﻿using DatabaseContext.Context;
using DomainModels.Entities.Support;
using Infrastructure.Common;
using Infrastructure.Messages;
using Services.Interfaces;
using System;
using System.Web.Mvc;
using ViewModels.Support;
using Web.Security.Authentication;

namespace Web.Areas.Member.Controllers
{
    [Authorize(Roles = "registeredusers,verifiedusers")]
    public class TicketController : Controller
    {

        #region ctor

        private readonly IUnitOfWork _uow;
        private readonly ITicketService _ticketService;
        private readonly ITicketReplyService _ticketReplyService;
        private readonly ICurrentUser _currentUser;

        public TicketController(
            IUnitOfWork uow, 
            ITicketService ticketService, 
            ITicketReplyService ticketReplyService,
            ICurrentUser currentUser)
        {
            _uow = uow;
            _ticketService = ticketService;
            _ticketReplyService = ticketReplyService;
            _currentUser = currentUser;
        }

        #endregion




        // ---------------------------------------------- List
        [HttpGet]
        public ActionResult Index(bool showJudge, int? page = 1)
        {
            ViewBag.Title = showJudge ? "Judge Tickets" : "Support Tickets";

            var tickets = _ticketService.GetAllUserTickets(_currentUser.Id, showJudge, page.Value, 15);

            return View(tickets);
        }


        // -------------------------------------------- Detail
        [HttpGet]
        public ActionResult Detail(Guid rowGuid)
        {
            var userId = _currentUser.Id;
            var vm = _ticketService.GetUserTicketDetail(userId, rowGuid);

            ViewBag.UserId = userId;

            return View(vm);
        }


        // ---------------------------------------------- Create
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(TicketCreateViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            _ticketService.Add(new Ticket
            {
                Subject = model.Subject,
                Message = model.Message,
                RowGuid = Guid.NewGuid(),
                SenderId = _currentUser.Id,
                IsOpen = true,
                CreatedOn = DateTime.Now
            });
            _uow.SaveChanges();

            TempData["Message"] = new Message { IsSuccess = true, MessageBody = "New ticket saved successfully" };
            return RedirectToAction("Index", "Dashboard", new { area = "member" });
        }



        // ---------------------------------------------- _Reply
        [ChildActionOnly]
        [HttpGet]
        public PartialViewResult _Reply(Guid rowGuid)
        {
            return PartialView(new ReplyViewModel { RowGuid = rowGuid });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult _Reply(ReplyViewModel model)
        {
            if (!ModelState.IsValid)
                return PartialView(model);

            _ticketReplyService.Add(model.RowGuid, model.Message, _currentUser.Id);
            _uow.SaveChanges();

            return RedirectToAction("Detail", "Ticket", new { area = "Member", rowGuid = model.RowGuid });
        }

    }
}