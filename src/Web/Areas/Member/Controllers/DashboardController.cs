﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web.Areas.Member.Controllers
{

    [Authorize(Roles = "registeredusers,verifiedusers")]
    public class DashboardController : Controller
    {
        public ActionResult Index(string lang)
        {
            return View();
        }
    }
}