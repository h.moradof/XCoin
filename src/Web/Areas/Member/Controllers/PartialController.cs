﻿using Services.Interfaces;
using System.Web.Mvc;
using ViewModels.Base;
using Web.Security.Authentication;

namespace Web.Areas.Member.Controllers
{
    [Authorize(Roles = "registeredusers,verifiedusers")]
    public class PartialController : Controller
    {
        #region ctor

        private readonly IUserBalanceService _userBalanceService;
        private readonly ICurrentUser _currentUser;

        public PartialController(
            IUserBalanceService userBalanceService,
            ICurrentUser currentUser)
        {
            _userBalanceService = userBalanceService;
            _currentUser = currentUser;
        }

        #endregion


        // ------------------------------------------ Get User Balances
        [HttpPost]
        public JsonResult GetUserBalances()
        {
            var balances = _userBalanceService.GetUserBalances(_currentUser.Id);

            return Json(new ApiResultViewModel { Data = balances });
        }




    }
}