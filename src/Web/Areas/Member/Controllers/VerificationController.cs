﻿using DatabaseContext.Context;
using Infrastructure.Exceptions;
using Services.Interfaces;
using System;
using System.Web.Mvc;
using ViewModels.Base;
using ViewModels.HumanResource;
using Web.Security.Authentication;

namespace Web.Areas.Member.Controllers
{
    [Authorize(Roles = "registeredusers,verifiedusers")]
    public class VerificationController : Controller
    {
        #region props
        private readonly IUnitOfWork _uow;
        private readonly IUserService _userService;
        private readonly ICurrentUser _currentUser;
        #endregion

        #region ctor
        public VerificationController(IUnitOfWork uow, IUserService userService, ICurrentUser currentUser)
        {
            _uow = uow;
            _userService = userService;
            _currentUser = currentUser;
        }
        #endregion



        // ---------------------------------------------- Face And Identification Card
        [HttpGet]
        public ActionResult IdentificationCard(string lang)
        {
            var vm = _userService.GetIdentificationCardVerifyViewModel(_currentUser.Id);

            ViewBag.Lang = lang;

            return View(vm);
        }


        [HttpPost]
        public JsonResult IdentificationCard(string lang, string base64PhotoContent)
        {
            try
            {
                // ثبت تصویر با وبکم
                _userService.SetIdentificationCardPhoto(_currentUser.Id, base64PhotoContent, lang);
                _uow.SaveChanges();

                return Json(new ApiResultViewModel());
            }
            catch (CustomException ex)
            {
                return Json(new ApiResultViewModel { HasError = true, Message = ex.Message });
            }
            catch (Exception ex)
            {
                return Json(new ApiResultViewModel { HasError = true, Message = "some error occur" });
            }
        }



        // ---------------------------------------------- Face And Bank Card
        [HttpGet]
        public ActionResult BankCard(string lang)
        {
            var vm = _userService.GetBankCardVerifyViewModel(_currentUser.Id);

            ViewBag.Lang = lang;

            return View(vm);
        }


        [HttpPost]
        public JsonResult BankCard(string lang, string base64PhotoContent)
        {
            try
            {
                // ثبت تصویر با وبکم
                _userService.SetBankAccountCardPhoto(_currentUser.Id, base64PhotoContent, lang);
                _uow.SaveChanges();

                return Json(new ApiResultViewModel());
            }
            catch (CustomException ex)
            {
                return Json(new ApiResultViewModel { HasError = true, Message = ex.Message });
            }
            catch (Exception ex)
            {
                return Json(new ApiResultViewModel { HasError = true, Message = "some error occur" });
            }
        }


    }
}