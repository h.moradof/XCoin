﻿using System.Web.Mvc;

namespace Web.Areas.Member.Controllers
{
    [Authorize(Roles = "registeredusers")]
    public class MessageController : Controller
    {
        
        public ActionResult Show()
        {
            return View();
        }
	}
}