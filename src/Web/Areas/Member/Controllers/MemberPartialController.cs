﻿using Services.Interfaces;
using System.Web.Mvc;
using ViewModels.Base;
using Web.Security.Authentication;

namespace Web.Areas.Member.Controllers
{
    [Authorize(Roles = "registeredusers,verifiedusers")]
    public class MemberPartialController : Controller
    {

        #region ctor
        private readonly IDealService _dealService;
        private readonly ICurrentUser _currentUser;

        public MemberPartialController(
            IDealService dealService,
            ICurrentUser currentUser)
        {
            _dealService = dealService;
            _currentUser = currentUser;
        }
        #endregion



        
        public PartialViewResult _Notifications(string lang)
        {
            var vm = _dealService.GetNofitications(_currentUser.Id);

            return PartialView(vm);
        }
    }
}