﻿using DatabaseContext.Context;
using DomainModels.Entities.Trade;
using Infrastructure.Exceptions;
using Services.Interfaces;
using System;
using System.Linq;
using System.Web.Mvc;
using ViewModels.Base;
using Web.Security.Authentication;

namespace Web.Areas.Member.Controllers
{
    [Authorize(Roles = "verifiedusers")]
    public class DealController : Controller
    {

        #region ctor
        private readonly IUnitOfWork _uow;
        private readonly IDealService _dealService;
        private readonly IChatService _chatService;
        private readonly ICurrentUser _currentUser;
        private readonly IDealStatusService _dealStatusService;
       
        public DealController(
            IUnitOfWork uow,
            IDealService dealService,
            IChatService chatService,
            ICurrentUser currentUser,
            IDealStatusService dealStatusService)
        {
            _uow = uow;
            _dealService = dealService;
            _chatService = chatService;
            _currentUser = currentUser;
            _dealStatusService = dealStatusService;
        }
        #endregion


        // -------------------------------------- Have chat and Make a deal
        [HttpGet]
        public ActionResult Index(Guid rowGuid)
        {
            return View();
        }


        // ----------------------------------------- chating
        [HttpPost]
        public JsonResult SendChatMessage(string message, Guid dealId)
        {
            try
            {
                _chatService.Add(message, _currentUser.Id, dealId);
                _uow.SaveChanges();

                return Json(new ApiResultViewModel());
            }
            catch (CustomException ex)
            {
                return Json(new ApiResultViewModel { HasError = true, Message = ex.Message });
            }
            catch (Exception ex)
            {
                return Json(new ApiResultViewModel { HasError = true, Message = "An error occoured" });
            }
        }

        [HttpPost]
        public JsonResult GetChatMessages(Guid dealId)
        {
            try
            {
                var userId = _currentUser.Id;

                var msgs = _chatService.GetMessages(userId, dealId);

                return Json(new ApiResultViewModel { Data = msgs.Select(c => new { Message = c.Chat.Message, Sender = c.SenderFullName, IsSender = (c.Chat.SenderId == userId), CreatedOn = c.Chat.CreatedOn.ToString("MM/DD/YYYY hh:mm:ss") }) });
            }
            catch (Exception ex)
            {
                return Json(new ApiResultViewModel { HasError = true, Message = "An error occoured" });
            }
        }



        // -------------------------------------- Member deals
        [HttpGet]
        public ActionResult MemberDeals(string lang, long statusId, int? page = 1)
        {
            var userId = _currentUser.Id;

            var status = _dealStatusService.Find(statusId);
            if (status == null)
            {
                return HttpNotFound();
            }

            ViewBag.StatusTitle = status.DisplayName;

            var deals = _dealService.GetMemberDeals(userId, statusId, page.Value , 15);

            ViewBag.UserId = userId;

            return View(deals);
        }



        // -------------------------------------- Buyer Paid Currency
        [HttpPost]
        public JsonResult BuyerPayCurrency(Guid rowGuid)
        {
            try
            {
                _dealService.BuyerPayCurrency(rowGuid, _currentUser.Id);
                _uow.SaveChanges();

                return Json(new ApiResultViewModel());
            }
            catch (CustomException ex)
            {
                return Json(new ApiResultViewModel { HasError = true, Message = ex.Message });
            }
        }


        // --------------------------------------- Saler Release Coin
        [HttpPost]
        public JsonResult SalerReleaseCoin(Guid rowGuid)
        {
            try
            {
                _dealService.SalerReleaseCoin(rowGuid, _currentUser.Id);
                _uow.SaveChanges();

                return Json(new ApiResultViewModel());
            }
            catch (CustomException ex)
            {
                return Json(new ApiResultViewModel { HasError = true, Message = ex.Message });
            }
        }


        // --------------------------------------- Get Deal State
        [HttpPost]
        public JsonResult GetDealState(Guid rowGuid)
        {
            try
            {
                var vm = _dealService.GetDealState(rowGuid, _currentUser.Id);
                
                return Json(new ApiResultViewModel { Data = vm });
            }
            catch (CustomException ex)
            {
                return Json(new ApiResultViewModel { HasError = true, Message = ex.Message });
            }
            catch (Exception ex)
            {
                return Json(new ApiResultViewModel { HasError = true, Message = ex.Message });
            }
        }


        // --------------------------------------- Canceling Deal
        [HttpPost]
        public JsonResult CancelingDeal(Guid rowGuid)
        {
            try
            {
                _dealService.CancelingDeal(rowGuid, _currentUser.Id);
                _uow.SaveChanges();

                return Json(new ApiResultViewModel { Message = "The deal canceled successfully" });
            }
            catch (CustomException ex)
            {
                return Json(new ApiResultViewModel { HasError = true, Message = ex.Message });
            }
        }


        // --------------------------------------- JudgeDeal Deal
        [HttpPost]
        public JsonResult JudgeDeal(Guid rowGuid)
        {
            try
            {
                _dealService.JudgeDeal(rowGuid, _currentUser.Id);
                _uow.SaveChanges();

                return Json(new ApiResultViewModel { Message = "Your judge request sent successfully" });
            }
            catch (CustomException ex)
            {
                return Json(new ApiResultViewModel { HasError = true, Message = ex.Message });
            }
        }
        

    }
}