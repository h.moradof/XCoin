﻿using Infrastructure.Cache.Interfaces;
using System.Web.Mvc;

namespace Web.Controllers
{
    [AllowAnonymous]
    public class PartialController : Controller
    {

        #region ctor
        private readonly ISeoSettingCacheManager _siteSeoSettingCacheManager;

        public PartialController(ISeoSettingCacheManager siteSeoSettingCacheManager)
        {
            _siteSeoSettingCacheManager = siteSeoSettingCacheManager;
        }
        #endregion




        [ChildActionOnly]
        public PartialViewResult _Meta()
        {
            var siteSeoSetting = _siteSeoSettingCacheManager.GetDefaultSetting();

            return PartialView(siteSeoSetting);
        }

        [ChildActionOnly]
        public PartialViewResult  _Title()
        {
            var siteSeoSetting = _siteSeoSettingCacheManager.GetDefaultSetting();

            return PartialView(model: siteSeoSetting.Title);
        }

        [ChildActionOnly]
        public PartialViewResult _Copyright()
        {
            var siteSeoSetting = _siteSeoSettingCacheManager.GetDefaultSetting();

            return PartialView(model: siteSeoSetting);
        }

    }
}