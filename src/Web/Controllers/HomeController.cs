﻿using Services.Interfaces;
using System.Web.Mvc;
using ViewModels.Base;
using ViewModels.Trade;

namespace Web.Controllers
{
    [AllowAnonymous]
    public class HomeController : Controller
    {

        #region props
        private readonly ICountryService _countryService;
        private readonly ICoinService _coinService;
        private readonly ICurrencyService _currencyService;
        private readonly IPaymentMethodService _paymentMethodService;
        private readonly IAdsService _adsService;
        #endregion

        #region ctor
        public HomeController(
            ICountryService countryService, 
            ICoinService coinService,
            ICurrencyService currencyService,
            IPaymentMethodService paymentMethodService,
            IAdsService adsService)
        {
            _countryService = countryService;
            _coinService = coinService;
            _currencyService = currencyService;
            _paymentMethodService = paymentMethodService;
            _adsService = adsService;
        }
        #endregion


        [HttpGet]
        public ActionResult Index(string lang)
        {
            var vm = new HomePageViewModel();

            #region search form
            var allDllItem = new BaseDdlBaseOnSlugViewModel { Name = "All", Slug = "" };

            vm.Coins = _coinService.GetBaseOnSlugDdlList();
            vm.Coins.Insert(0, allDllItem);

            vm.Countries = _countryService.GetBaseOnSlugDdlList();
            vm.Countries.Insert(0, allDllItem);

            vm.Currencies = _currencyService.GetBaseOnSlugDdlList();
            vm.Currencies.Insert(0, allDllItem);

            vm.PaymentMethods = _paymentMethodService.GetBaseOnSlugDdlList(lang);
            vm.PaymentMethods.Insert(0, allDllItem);
            #endregion

            vm.LastBuyAds = _adsService.GetLast(false, lang , 10);
            vm.LastSellAds = _adsService.GetLast(true, lang, 10);

            ViewBag.Lang = lang;

            return View(vm);
        }

    }
}