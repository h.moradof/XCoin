﻿using Infrastructure.Common;
using Infrastructure.Security;
using System.Web.Mvc;
using ViewModels.Base;

namespace Web.Controllers
{
    [AllowAnonymous]
    public class AjaxController : Controller
    {

        [HttpPost]
        public JsonResult GetCaptcha()
        {
            var captcha = CaptchaManager.GenerateHash(new RandomMaker().CreateRandomNumbers(4));

            var result = new ApiResultViewModel
            {
                Data = captcha,
                HasError = false,
                Message = null
            };

            return Json(result);
        }
    }
}