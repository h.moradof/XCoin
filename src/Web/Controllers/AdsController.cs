﻿using Services.Interfaces;
using System.Text;
using System.Web.Mvc;
using ViewModels.Base;
using ViewModels.Trade;
using System;

namespace Web.Controllers
{
    [AllowAnonymous]
    public class AdsController : Controller
    {
        #region props
        private readonly ICountryService _countryService;
        private readonly ICoinService _coinService;
        private readonly ICurrencyService _currencyService;
        private readonly IPaymentMethodService _paymentMethodService;
        private readonly IAdsService _adsService;
        #endregion

        #region ctor
        public AdsController(
            ICountryService countryService,
            ICoinService coinService,
            ICurrencyService currencyService,
            IPaymentMethodService paymentMethodService,
            IAdsService adsService)
        {
            _countryService = countryService;
            _coinService = coinService;
            _currencyService = currencyService;
            _paymentMethodService = paymentMethodService;
            _adsService = adsService;
        }
        #endregion


        // --------------------------------------------------- search
        [HttpGet]
        public ActionResult Search(string lang, bool issell, string country, string currency, decimal? amount, string coin, string paymentMethod, string sortBy, string sortMethod, int? page = 1)
        {
            var vm = new AdsSearchViewModel();

            #region search form
            var allDllItem = new BaseDdlBaseOnSlugViewModel { Name = "All", Slug = "" };

            vm.Coins = _coinService.GetBaseOnSlugDdlList();
            vm.Coins.Insert(0, allDllItem);

            vm.Countries = _countryService.GetBaseOnSlugDdlList();
            vm.Countries.Insert(0, allDllItem);

            vm.Currencies = _currencyService.GetBaseOnSlugDdlList();
            vm.Currencies.Insert(0, allDllItem);

            vm.PaymentMethods = _paymentMethodService.GetBaseOnSlugDdlList(lang);
            vm.PaymentMethods.Insert(0, allDllItem);
            #endregion

            vm.Adses = _adsService.Search(issell, coin, currency, country, paymentMethod, amount, lang, sortBy, sortMethod, page.Value, 10);

            ViewBag.Lang = lang;
            ViewBag.Title = MakePageTitle(issell, coin, country);
            ViewBag.Header = MagePageHeader(issell, coin, country);

            return View(vm);
        }



        // --------------------------------------------------- detail
        public ActionResult Detail(string lang, long id)
        {
            var vm = _adsService.GetDetailViewModel(id, lang);

            if (vm == null)
            {
                return HttpNotFound();
            }

            ViewBag.Lang = lang;
            ViewBag.Title = MakePageTitle(vm);

            return View(vm);
        }




        #region private methods
        private string MakePageTitle(AdsDetailViewModel vm)
        {
            return string.Format("{0} {1} in {2} from {3} using {4}", (vm.IsSell ? "Buy" : "Sell"), vm.CoinName, vm.CountryName, vm.Username, vm.PaymentMethodName);
        }

        private string MagePageHeader(bool isSell, string coinSlug, string countrySlug)
        {
            var titleBuilder = new StringBuilder(isSell ? WebResources.StaticText.IWantToBuy : WebResources.StaticText.IWantToSell);

            if (!string.IsNullOrEmpty(coinSlug))
            {
                var coin = _coinService.FindBySlug(coinSlug);
                if (coin != null)
                {
                    titleBuilder.Append(" ");
                    titleBuilder.Append(coin.Name);
                }
                else
                {
                    titleBuilder.Append(" ");
                    titleBuilder.Append(WebResources.StaticText.Coin);
                }
            }
            else
            {
                titleBuilder.Append(" ");
                titleBuilder.Append(WebResources.StaticText.Coin);
            }

            if (!string.IsNullOrEmpty(countrySlug))
            {
                var country = _countryService.FindBySlug(countrySlug);
                if (country != null)
                {
                    titleBuilder.Append(" ");
                    titleBuilder.Append(WebResources.StaticText.In);
                    titleBuilder.Append(" ");
                    titleBuilder.Append(country.Name);
                }
            }

            return titleBuilder.ToString();
        }

        private string MakePageTitle(bool isSell, string coinSlug, string countrySlug)
        {
            var titleBuilder = new StringBuilder(isSell ? "Buy" : "Sell");

            if (!string.IsNullOrEmpty(coinSlug))
            {
                var coin = _coinService.FindBySlug(coinSlug);
                if (coin != null)
                {
                    titleBuilder.Append(" ");
                    titleBuilder.Append(coin.Name);
                }
                else
                {
                    titleBuilder.Append(" coin");
                }
            }
            else
            {
                titleBuilder.Append(" coin");
            }

            if(!string.IsNullOrEmpty(countrySlug))
            {
                var country = _countryService.FindBySlug(countrySlug);
                if(country != null)
                {
                    titleBuilder.Append(" in ");
                    titleBuilder.Append(country.Name);
                }
            }

            return titleBuilder.ToString();
        }
        #endregion

    }
}