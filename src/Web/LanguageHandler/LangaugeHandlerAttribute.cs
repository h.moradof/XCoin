﻿using System.Web.Mvc;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using Infrastructure.MultiLanguage;

namespace Web.LanguageHandler
{
    public class LangaugeHandlerAttribute : ActionFilterAttribute
    {
        #region props
        private readonly IList<string> _supportedLocales;
        private readonly string _defaultLang;
        #endregion

        #region ctor
        public LangaugeHandlerAttribute()
        {
            _supportedLocales = LanguageManager.GetSupportedLocales();
            _defaultLang = LanguageManager.GetDefaultLanaguage();
        }
        #endregion

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            // Get locale from route values
            string lang = (string)filterContext.RouteData.Values["lang"] ?? _defaultLang;

            if (!_supportedLocales.Contains(lang))
                lang = _defaultLang;

            SetLang(lang);
        }


        private void SetLang(string lang)
        {
            Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo(lang);
            Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo(lang);
        }

    }
}