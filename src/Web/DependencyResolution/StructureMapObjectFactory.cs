﻿using System;
using StructureMap;
using StructureMap.Web;
using System.Threading;
using Services.Interfaces;
using DatabaseContext.Context;
using Infrastructure.Cache.Interfaces;
using Infrastructure.Cache;
using Infrastructure.Mail.Interfaces;
using Web.Security.Authentication;
using Infrastructure.WebServices;
using Infrastructure.Sms.Interfaces;
using Infrastructure.Sms;
using Infrastructure.Sms.Providers;
using Infrastructure.Mail.Core;
using Infrastructure.Mail;
using Infrastructure.Security.Password;

namespace Web.DependencyResolution
{
    /// <summary>
    /// object factory
    /// </summary>
    public static class StructureMapObjectFactory
    {
        private static readonly Lazy<Container> _containerBuilder =
            new Lazy<Container>(defaultContainer, LazyThreadSafetyMode.ExecutionAndPublication);

        public static IContainer Container
        {
            get { return _containerBuilder.Value; }
        }

        private static Container defaultContainer()
        {
            return new Container(x =>
            {
                x.For<IUnitOfWork>().HybridHttpOrThreadLocalScoped().Use<DefaultDatabaseContext>();

                x.Scan(scan =>
                {
                    scan.AssemblyContainingType<IUserService>();
                    scan.WithDefaultConventions();
                });

                // current user
                x.For<ICurrentUser>().Use<CurrentUser>();

                // cache managers
                x.For<IApplicationSettingCacheManager>().Singleton().Use<ApplicationSettingCacheManager>();
                x.For<ISeoSettingCacheManager>().Singleton().Use<SeoSettingCacheManager>();
                x.For<IAccessCacheManager>().Singleton().Use<AccessCacheManager>();

                // mail & sms
                x.For<IMailSender>().Singleton().Use<MailSender>();
                x.For<ISmsSender>().Singleton().Use<SmsSender>();
                x.For<IUserAccountMailSender>().Singleton().Use<UserAccountMailSender>();

                // sms provider
                // x.For<ISmsProvider>().Singleton().Use<KaveNegarSmsProvider>().Ctor<string>("templateName").Is("Verify");

                // webservice
                // x.For<IWebServiceManager>().Singleton().Use<WebServiceManager>();


            });
        }

    }
}