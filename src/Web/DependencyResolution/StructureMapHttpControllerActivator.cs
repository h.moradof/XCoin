﻿using StructureMap;
using System;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Dispatcher;

namespace Web.DependencyResolution
{
    public class StructureMapHttpControllerActivator : IHttpControllerActivator
    {
        #region props

        private readonly IContainer _container;

        #endregion

        #region ctor

        public StructureMapHttpControllerActivator(IContainer container)
        {
            _container = container;
        }

        #endregion

        public IHttpController Create(HttpRequestMessage request, HttpControllerDescriptor controllerDescriptor, Type controllerType)
        {
            var nestedContainer = _container.GetNestedContainer();
            request.RegisterForDispose(nestedContainer);
            return (IHttpController)nestedContainer.GetInstance(controllerType);
        }
    }
}