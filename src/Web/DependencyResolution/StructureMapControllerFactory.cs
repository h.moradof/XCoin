﻿using System;
using System.Web.Mvc;
using System.Web.Routing;

namespace Web.DependencyResolution
{
    /// <summary>
    /// Inject dependencies into controllers
    /// </summary>
    public class StructureMapControllerFactory : DefaultControllerFactory
    {
        protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
        {
            if (controllerType == null)
            {
                // return null;
                throw new Exception(string.Format("Page Not Found : {0}", requestContext.HttpContext.Request.RawUrl));
            }

            return StructureMapObjectFactory.Container.GetInstance(controllerType) as Controller;
        }
    }
}