﻿using System.Collections.Generic;
using System.Reflection;
using System.Web.Mvc;

namespace Web.Helpers.GeneralHelpers
{
    public static class BooleanHelper
    {
        private const string NULL_TEXT = "---";
        private const string TRUE_CSS_CLASS = "active-span";
        private const string FALSE_CSS_CLASS = "deactive-span";

        public static MvcHtmlString ShowBoolean(this HtmlHelper htmlHelper, bool booleanValue, string trueText, string falseText)
        {
            TagBuilder spanTag = new TagBuilder("span");
            spanTag.InnerHtml = booleanValue ? trueText : falseText;
            spanTag.MergeAttribute("class", booleanValue ? TRUE_CSS_CLASS : FALSE_CSS_CLASS);

            return MvcHtmlString.Create(spanTag.ToString());
        }

        public static MvcHtmlString ShowBoolean(this HtmlHelper htmlHelper, bool? booleanValue, string trueText, string falseText)
        {
            TagBuilder spanTag = new TagBuilder("span");

            if (!booleanValue.HasValue)
            {
                spanTag.InnerHtml = NULL_TEXT;
                return MvcHtmlString.Create(spanTag.ToString());
            }
            
            spanTag.InnerHtml = booleanValue.Value ? trueText : falseText;
            spanTag.MergeAttribute("class", booleanValue.Value ? TRUE_CSS_CLASS : FALSE_CSS_CLASS);

            return MvcHtmlString.Create(spanTag.ToString());
        }


        public static MvcHtmlString ShowBoolean(this HtmlHelper htmlHelper, bool booleanValue, string trueText, string falseText, object htmlTrueAttributes, object htmlFalseAttributes)
        {
            TagBuilder spanTag = new TagBuilder("span");
            spanTag.InnerHtml = booleanValue ? trueText : falseText;

            if (booleanValue)
            {
                // add html attributes
                if (htmlTrueAttributes != null)
                {
                    var props = new List<PropertyInfo>(htmlTrueAttributes.GetType().GetProperties());
                    foreach (PropertyInfo prop in props)
                        spanTag.MergeAttribute(prop.Name, prop.GetValue(htmlTrueAttributes, null).ToString());
                }
            }
            else
            {
                // add html attributes
                if (htmlFalseAttributes != null)
                {
                    var props = new List<PropertyInfo>(htmlFalseAttributes.GetType().GetProperties());
                    foreach (PropertyInfo prop in props)
                        spanTag.MergeAttribute(prop.Name, prop.GetValue(htmlFalseAttributes, null).ToString());
                }
            }

            return MvcHtmlString.Create(spanTag.ToString());
        }


        public static MvcHtmlString ShowBoolean(this HtmlHelper htmlHelper, bool? booleanValue, string trueText, string falseText, object htmlTrueAttributes, object htmlFalseAttributes)
        {
            TagBuilder spanTag = new TagBuilder("span");

            if (!booleanValue.HasValue)
            {
                spanTag.InnerHtml = NULL_TEXT;
                return MvcHtmlString.Create(spanTag.ToString());
            }

            if (booleanValue.Value)
            {
                // add html attributes
                if (htmlTrueAttributes != null)
                {
                    var props = new List<PropertyInfo>(htmlTrueAttributes.GetType().GetProperties());
                    foreach (PropertyInfo prop in props)
                        spanTag.MergeAttribute(prop.Name, prop.GetValue(htmlTrueAttributes, null).ToString());
                }
            }
            else
            {
                // add html attributes
                if (htmlFalseAttributes != null)
                {
                    var props = new List<PropertyInfo>(htmlFalseAttributes.GetType().GetProperties());
                    foreach (PropertyInfo prop in props)
                        spanTag.MergeAttribute(prop.Name, prop.GetValue(htmlFalseAttributes, null).ToString());
                }
            }

            spanTag.InnerHtml = booleanValue.Value ? trueText : falseText;
            return MvcHtmlString.Create(spanTag.ToString());
        }

    }

}