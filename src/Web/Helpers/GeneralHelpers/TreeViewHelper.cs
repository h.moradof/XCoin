﻿using ViewModels.Base;
using Web.Helpers.Common;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Web.Helpers.GeneralHelpers
{
    public static class TreeViewHelper
    {

        public static MvcHtmlString ShowTreeView(
            this HtmlHelper htmlHelper, 
            IList<BaseTreeViewViewModel> model,
            long rootItemId,
            string targetUrl, 
            bool appendIdToTargetUrl,
            bool putIdsInCustomAttribute,
            string customAttributeNameForIds
            )
        {
            var treeViewMaker = new TreeViewMaker(model, targetUrl, appendIdToTargetUrl, putIdsInCustomAttribute, customAttributeNameForIds);

            return MvcHtmlString.Create(treeViewMaker.Create(rootItemId));
        }

    }
}