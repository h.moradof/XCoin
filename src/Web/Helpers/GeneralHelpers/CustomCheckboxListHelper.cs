﻿using ViewModels.Base;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace Web.Helpers.GeneralHelpers
{
    public static class CustomCheckboxListHelper
    {
        public static MvcHtmlString ShowCheckboxList(this HtmlHelper htmlHelper, string groupName, IEnumerable<BaseCheckboxItem> allItems, long[] selectedIds = null)
        {
            var checkBoxList = new StringBuilder("<div class='chkbox-wrap'>");

            foreach (var item in allItems)
            {
                var checkBox = new TagBuilder("input");
                checkBox.MergeAttribute("type", "checkbox");
                checkBox.MergeAttribute("name", groupName);
                checkBox.MergeAttribute("value", item.Id.ToString());

                if (selectedIds != null && selectedIds.Any(c => c == item.Id))
                    checkBox.MergeAttribute("checked", "checked");

                checkBoxList.Append(checkBox.ToString());

                var span = new TagBuilder("span");
                span.InnerHtml = item.Name;
                checkBoxList.Append("&nbsp; ");
                checkBoxList.Append(span.ToString());

                checkBoxList.Append("<br/>");
            }
            checkBoxList.Append("</div>");

            return MvcHtmlString.Create(checkBoxList.ToString());
        }
    }
}