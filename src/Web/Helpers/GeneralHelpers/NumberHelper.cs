﻿using Infrastructure.Common;
using System.Web.Mvc;

namespace Web.Helpers.GeneralHelpers
{

    public static class NumberHelper
    {

        public static MvcHtmlString ShowNumberWithCommaSeperator(this HtmlHelper htmlHelper, long? number)
        {
            var output = string.Empty;
            if (number.HasValue)
                output = number.Value.ToString("#,##0", new System.Globalization.CultureInfo("en-US"));

            return MvcHtmlString.Create(output);
        }

        public static MvcHtmlString ShowNumberWithCommaSeperator(this HtmlHelper htmlHelper, int? number)
        {
            var output = string.Empty;
            if (number.HasValue)
                output = number.Value.ToString("#,##0", new System.Globalization.CultureInfo("en-US"));

            return MvcHtmlString.Create(output);
        }


        public static MvcHtmlString ShowPersianNumberWithCommaSeperator(this HtmlHelper htmlHelper, long? number)
        {
            var output = string.Empty;
            if (number.HasValue)
                output = number.Value.ToString("#,##0", new System.Globalization.CultureInfo("en-US")).ToPersianNumber();

            return MvcHtmlString.Create(output);
        }

        public static MvcHtmlString ShowPersianNumberWithCommaSeperator(this HtmlHelper htmlHelper, int? number)
        {
            var output = string.Empty;
            if (number.HasValue)
                output = number.Value.ToString("#,##0", new System.Globalization.CultureInfo("en-US")).ToPersianNumber();

            return MvcHtmlString.Create(output);
        }

    }
}