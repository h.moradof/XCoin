﻿using Infrastructure.File;
using System.Web.Mvc;

namespace Web.Helpers.GeneralHelpers
{
    public static class FileHelper
    {
        public static MvcHtmlString ShowValidMaximumFileLength(this HtmlHelper htmlHelper)
        {
            return MvcHtmlString.Create(string.Format(" حداکثر حجم قابل آپلود برای فایل ها <b class='valid-length'>{0} MB</b> می باشد", (FileValidator.ValidUploadCenterLength / (1024*1024))));
        }


        public static MvcHtmlString ShowValidFileExtensions(this HtmlHelper htmlHelper)
        {
            return MvcHtmlString.Create(string.Format(" فرمت های مجاز برای فایل <b class='valid-extensions'>{0}</b> می باشد", FileValidator.ValidFileExtensions.Replace(",", ", ")));
        }


        public static MvcHtmlString ShowValidPictureExtensions(this HtmlHelper htmlHelper)
        {
            return MvcHtmlString.Create(string.Format(" فرمت های مجاز برای تصویر <b class='valid-extensions'>{0}</b> می باشد", FileValidator.ValidPictureExtensions.Replace(",", ", ")));
        }

    }
}