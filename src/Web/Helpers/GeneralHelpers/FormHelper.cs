﻿using System.Web.Mvc;

namespace Web.Helpers.GeneralHelpers
{
    public static class FormHelper
    {

        private const string FORM_PATTERN = "<form action='/{0}/{1}/{2}?{9}' method='post' class='{3}'>{4}"
                    + "<input type='hidden' name='{5}' value='{6}' />"
                    + "<input type='submit' value='{7}' class='{8}' />"
                    + "</form>";

        private const string FORM_PATTERN_Without_Area = "<form action='/{0}/{1}/?{8}' method='post' class='{2}'>{3}"
                   + "<input type='hidden' name='{4}' value='{5}' />"
                   + "<input type='submit' value='{6}' class='{7}' />"
                   + "</form>";

        public static MvcHtmlString ShowForm(this HtmlHelper htmlHelper,
            string hiddenFieldName, string hiddenFieldValue,
            string btnText, string btnCssClass,
            string actionName, string controllerName, string areaName,
            string formCssClass,
            string routeValues,
            bool isUseAntiForgeryToken = true)
        {

            var antiForgeryToken = isUseAntiForgeryToken ? htmlHelper.AntiForgeryToken() : MvcHtmlString.Create(string.Empty);

            string form = string.Empty;

            if (string.IsNullOrEmpty(areaName))
            {
                form = string.Format(FORM_PATTERN_Without_Area,
                controllerName, actionName,
                formCssClass,
                antiForgeryToken,
                hiddenFieldName, hiddenFieldValue,
                btnText, btnCssClass, routeValues);
            }
            else
            {
                form = string.Format(FORM_PATTERN,
                                areaName, controllerName, actionName,
                                formCssClass,
                                antiForgeryToken,
                                hiddenFieldName, hiddenFieldValue,
                                btnText, btnCssClass, routeValues);
            }

            return MvcHtmlString.Create(form);
        }


    }
}