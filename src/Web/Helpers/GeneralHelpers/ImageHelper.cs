﻿using System.Collections.Generic;
using System.Reflection;
using System.Web.Mvc;

namespace Web.Helpers.GeneralHelpers
{

    public static class ImageHelper
    {


        public static MvcHtmlString ShowImage(
            this HtmlHelper htmlHelper,
            string imageFileName,
            object htmlAttributes = null)
        {
            return ShowImageByDirectUrl(htmlHelper, imageFileName, htmlAttributes);
        }



        // --------------------------------------------- [Show Image By Direct Url] ---------------------------------------------

        private static MvcHtmlString ShowImageByDirectUrl(
            this HtmlHelper htmlHelper,
            string imageFileName,
            object htmlAttributes = null)
        {
            var imgTag = new TagBuilder("img");

            imgTag.MergeAttribute("src", string.Format("/Photo/{0}", imageFileName));

            if (htmlAttributes != null)
            {
                IList<PropertyInfo> props = new List<PropertyInfo>(htmlAttributes.GetType().GetProperties());
                foreach (PropertyInfo prop in props)
                    imgTag.MergeAttribute(prop.Name, prop.GetValue(htmlAttributes, null).ToString());
            }

            return MvcHtmlString.Create(imgTag.ToString(TagRenderMode.SelfClosing));

        }



        // --------------------------------------------- [Show Image By Mvc Action] ---------------------------------------------

        private static MvcHtmlString ShowImageByMvcAction(
            this HtmlHelper htmlHelper,
            string imageFileName,
            object htmlAttributes)
        {
            var imgTag = new TagBuilder("img");

            if (htmlAttributes != null)
            {
                IList<PropertyInfo> props = new List<PropertyInfo>(htmlAttributes.GetType().GetProperties());
                foreach (PropertyInfo prop in props)
                    imgTag.MergeAttribute(prop.Name, prop.GetValue(htmlAttributes, null).ToString());
            }

            imgTag.MergeAttribute("src", string.Format("/File/Image/?fileName={0}", imageFileName));

            return MvcHtmlString.Create(imgTag.ToString(TagRenderMode.SelfClosing));
        }


        private static MvcHtmlString ShowImageByMvcAction(
            this HtmlHelper htmlHelper,
            string imageFileName,
            object htmlAttributes,
            string areaName)
        {
            var imgTag = new TagBuilder("img");

            if (htmlAttributes != null)
            {
                IList<PropertyInfo> props = new List<PropertyInfo>(htmlAttributes.GetType().GetProperties());
                foreach (PropertyInfo prop in props)
                    imgTag.MergeAttribute(prop.Name, prop.GetValue(htmlAttributes, null).ToString());
            }

            if (string.IsNullOrEmpty(areaName))
                imgTag.MergeAttribute("src", string.Format("/File/Image/?fileName={0}", imageFileName));
            else
                imgTag.MergeAttribute("src", string.Format("/{0}/File/Image/?fileName={1}", areaName, imageFileName));

            return MvcHtmlString.Create(imgTag.ToString(TagRenderMode.SelfClosing));
        }



        private static MvcHtmlString ShowImageByMvcAction(
            this HtmlHelper htmlHelper,
            string imageFileName,
            object htmlAttributes,
            string areaName,
            string controllerName,
            string actionName)
        {
            var imgTag = new TagBuilder("img");

            if (htmlAttributes != null)
            {
                IList<PropertyInfo> props = new List<PropertyInfo>(htmlAttributes.GetType().GetProperties());
                foreach (PropertyInfo prop in props)
                    imgTag.MergeAttribute(prop.Name, prop.GetValue(htmlAttributes, null).ToString());
            }

            if (string.IsNullOrEmpty(areaName))
                imgTag.MergeAttribute("src", string.Format("/{0}/{1}/?fileName={2}", controllerName, actionName, imageFileName));
            else
                imgTag.MergeAttribute("src", string.Format("/{0}/{1}/{2}/?fileName={3}", areaName, controllerName, actionName, imageFileName));

            return MvcHtmlString.Create(imgTag.ToString(TagRenderMode.SelfClosing));
        }


    }
}