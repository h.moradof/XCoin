﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Web.Helpers.GeneralHelpers
{

    public static class SeperatedListHelper
    {
        public static MvcHtmlString ShowSeperatedList(this HtmlHelper htmlHelper, List<string> list, string seperateChar = "، ")
        {
            return MvcHtmlString.Create(String.Join(seperateChar, list));
        }
    }
}