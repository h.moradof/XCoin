﻿using Web.Helpers.Common;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Web.Helpers.ProjectHelpers
{
    public static class UserHelper
    {


        public static MvcHtmlString ShowBanForm(this HtmlHelper htmlHelper, long userId, string textAreaValue)
        {
            var hiddenFields = new Dictionary<string, string>();
            hiddenFields.Add("id", userId.ToString());

            var textArea = TextAreaMaker.CreateWithWraper("دلیل بن کردن کاربر", "banReason", textAreaValue, "form-control min-height", "required='required'");

            var form = FormMaker.GetNewForm("/Admin/User/Ban", "post", "بن کردن کاربر", "btn btn-danger", hiddenFields, new string[] { textArea }, htmlHelper.AntiForgeryToken().ToString());

            return MvcHtmlString.Create(form.ToString());
        }

    }
}