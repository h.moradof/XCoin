﻿using Infrastructure.Pagination.Base;
using System;
using System.Web;
using System.Web.Mvc;
using ViewModels.Trade;
using Web.Helpers.GeneralHelpers;

namespace Web.Helpers.ProjectHelpers
{
    public static class AdsHelper
    {
        private static string DefaultSearchMethod = "desc";


        public static MvcHtmlString ShowAdsSearchTableHeader(this HtmlHelper htmlHelper, string text, string sortByColumnName, string lang)
        {
            var aTag = new TagBuilder("a");
            var vmQueryString = GetQueryStringViewModel();
            vmQueryString.SortMethod = GetReversedSortMethod(vmQueryString.SortMethod);
            vmQueryString.Page = 1; // go to first page
            
            var href = string.Format("/{8}/ads/search?issell={0}&country={1}&currency={2}&coin={3}&paymentmethod={4}&sortby={5}&sortmethod={6}&page={7}",
                HttpContext.Current.Request["issell"],
                HttpContext.Current.Request["country"],
                HttpContext.Current.Request["currency"],
                HttpContext.Current.Request["coin"],
                HttpContext.Current.Request["paymentmethod"],
                sortByColumnName,
                vmQueryString.SortMethod,
                vmQueryString.Page,
                lang);

            aTag.MergeAttribute("href", href);

            string sortMethodIcon = GetSortMethodIcon(sortByColumnName, vmQueryString);
            aTag.InnerHtml = string.Format("<i class='{1}'></i>&nbsp;{0}", text, sortMethodIcon);

            return MvcHtmlString.Create(aTag.ToString());
        }

        

        public static MvcHtmlString ShowAdsSearchPager(this HtmlHelper htmlHelper, string lang, BasePagedList model)
        {
            var urlHelper = new UrlHelper(htmlHelper.ViewContext.RequestContext);
            var vmQueryString = GetQueryStringViewModel();

            if (HasAnyQueryString(vmQueryString))
            {
                return htmlHelper.ShowPager(model, urlHelper.Action("Search", "Ads", new { lang = lang, issell = vmQueryString.IsSell, country = vmQueryString.Country, currency = vmQueryString.Currency, coin = vmQueryString.Coin, amount = vmQueryString.Amount, paymentmethod = vmQueryString.PaymentMethod, sotyby = vmQueryString.SortBy, sortmethod = vmQueryString.SortMethod }), true);
            }
            else
            {
                return htmlHelper.ShowPager(model, urlHelper.Action("Search", "Ads", new { lang = lang }), true);
            }
        }






        #region private methods
        private static string GetSortMethodIcon(string sortByColumnName, AdsSearchQueryStringParamViewModel vmQueryString)
        {
            var sortMethodIcon = "fa fa-sort";
            if (!string.IsNullOrEmpty(vmQueryString.SortBy) && vmQueryString.SortBy == sortByColumnName)
            {
                sortMethodIcon = string.Format("fa fa-sort-{0}", vmQueryString.SortMethod);
            }

            return sortMethodIcon;
        }

        private static string GetReversedSortMethod(string sortMethod)
        {
            // change method
            return (sortMethod == "asc") ? "desc" : "asc";
        }

        private static bool HasAnyQueryString(AdsSearchQueryStringParamViewModel vmQueryString)
        {
            return (vmQueryString.SortBy != null || 
                    vmQueryString.SortMethod != null || 
                    vmQueryString.Country != null || 
                    vmQueryString.Currency != null || 
                    vmQueryString.Coin != null ||
                    vmQueryString.Amount != null || 
                    vmQueryString.PaymentMethod != null);
        }

        private static AdsSearchQueryStringParamViewModel GetQueryStringViewModel()
        {
            var vm = new AdsSearchQueryStringParamViewModel();
            var request = HttpContext.Current.Request;

            decimal amount = 0;
            decimal.TryParse(request["amount"], out amount);
            vm.Amount = (amount > 0) ? (Nullable<decimal>)amount : null;

            var isSell = false;
            bool.TryParse(request["issell"], out isSell);
            vm.IsSell = isSell;

            int page = 1;
            int.TryParse(request["page"], out page);
            vm.Page = page;

            vm.Coin = request["coin"];
            vm.Country = request["country"];
            vm.Currency = request["currency"];
            vm.PaymentMethod = request["paymentmethod"];
            vm.SortBy = request["sortby"];
            vm.SortMethod = request["sortmethod"];

            if (string.IsNullOrEmpty(vm.SortMethod))
            {
                vm.SortMethod = DefaultSearchMethod;
            }

            // we don't need search method when we don't have sortBy
            if (string.IsNullOrEmpty(vm.SortBy))
            {
                vm.SortMethod = string.Empty;
            }

            return vm;
        }
        #endregion

    }

}