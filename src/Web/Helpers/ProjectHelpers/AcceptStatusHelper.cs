﻿using DomainModels.Entities.HumanResource;
using System.Web;
using System.Web.Mvc;

namespace Web.Helpers.ProjectHelpers
{
    public static class AcceptStatusHelper
    {

        public static MvcHtmlString ShowAcceptStatus(this HtmlHelper htmlHelper, AcceptStatus status, string lang)
        {
            if (lang == "zh")
            {
                return MvcHtmlString.Create(status.ZhDisplayName);
            }

            return MvcHtmlString.Create(status.DisplayName);
        }
    }
}