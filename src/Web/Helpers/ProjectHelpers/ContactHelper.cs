﻿using System.Web.Mvc;

namespace Web.Helpers.ProjectHelpers
{

    public static class ContactHelper
    {
        public static MvcHtmlString ShowContactStatus(this HtmlHelper helper, bool isReaded)
        {
            var iTag = new TagBuilder("i");
            iTag.MergeAttribute("class", isReaded ? "fa fa-folder-open" : "fa fa-folder");
            return MvcHtmlString.Create(iTag.ToString());
        }
    }
}