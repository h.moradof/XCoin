﻿//using DomainModels.Entities.Access;
//using Infrastructure.Cache.Interfaces;
//using Infrastructure.Security.Interfaces;
//using Web.DependencyResolution;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Net.Http;
//using System.Web.Http;
//using System.Web.Http.Controllers;
//using Infrastructure.Security;
//using System.Web;
//using Web.Security.Authentication;
//using Infrastructure.Exceptions;
//using Services.Interfaces;

//namespace Web.Security.Attributes
//{
//    public class ApiAuthorizeAttribute : AuthorizeAttribute
//    {

//        public override void OnAuthorization(HttpActionContext actionContext)
//        {
//            try
//            {
//                ValidateResourceAccess(actionContext);

//                //base.OnAuthorization(actionContext);
//            }
//            catch (UnauthorizedAccessException ex)
//            {
//                actionContext.Response = new HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized);
//                actionContext.Response.ReasonPhrase = ex.Message;
//            }
//        }


//        /// <summary>
//        /// vaidate access to resource
//        /// </summary>
//        /// <param name="actionContext"></param>
//        /// <exception cref="UnauthorizedAccessException">throw when access to resource is denied</exception>
//        private void ValidateResourceAccess(HttpActionContext actionContext)
//        {
//            // دریافت توکن از هدر درخواست
//            string xAuth = GetXAuthFromHeader(actionContext);

//            // آدرس وب سرویس درخواستی
//            var resouecePath = actionContext.Request.RequestUri.AbsolutePath;

//            // فعل درخواست
//            var httpVerb = (HttpVerb)Enum.Parse(typeof(HttpVerb), actionContext.Request.Method.ToString(), true);


//            var tokenManager = StructureMapObjectFactory.Container.GetInstance<ITokenManager>();
//            var accessCacheManager = StructureMapObjectFactory.Container.GetInstance<IAccessCacheManager>();

//            /* 
//                /api/controllerName/ActionName
//            */
//            var isResourcePublic = accessCacheManager.IsResourcePublic(resouecePath, httpVerb);

//            if (isResourcePublic)
//            {
//                // بررسی دسترسی به منابعی که نیاز به لاگین ندارند
//                ValidateAccessToPublicResources(tokenManager, xAuth);
//            }
//            else
//            {
//                // بررسی دسترسی به منابعی که نیاز به لاگین دارند
//                ValidateAccessToPrivateResources(tokenManager, xAuth, resouecePath, httpVerb);
//            }

//        }

//        private void ValidateAccessToPublicResources(ITokenManager tokenManager, string xAuth)
//        {
//            if (xAuth.Length == TokenManager.ApplicationCodeValidLength)
//            {
//                // xAuth is ApplicationCode, so just check application code
//                var isValidApplicationCode = tokenManager.IsValidApplicationCode(xAuth);

//                if (!isValidApplicationCode)
//                    throw new UnauthorizedAccessException("Invalid authenticate header(3)");
//            }
//            else
//            {
//                // check token
//                tokenManager.ValidateAndVerifyXAuth(xAuth);
//            }

//            tokenManager.SetAuthenticationHeader(xAuth);
//        }

//        private void ValidateAccessToPrivateResources(ITokenManager tokenManager, string xAuth, string resouecePath, HttpVerb httpVerb)
//        {
//            // بررسی صحت فرمت و امضای توکن
//            tokenManager.ValidateAndVerifyXAuth(xAuth);

//            // ست کردن توکن روی هدر درخواست
//            tokenManager.SetAuthenticationHeader(xAuth);

//            // جلوگیری از دسترسی کاربران بن شده به وب سرویس ها
//            ValidateUserAccountStatus();

//            // بررسی دسترسی کاربر به 
//            CheckUserAccessToResource(resouecePath, httpVerb);
//        }





//        private static string GetXAuthFromHeader(HttpActionContext actionContext)
//        {
//            if (!actionContext.Request.Headers.Any(c => c.Key == TokenManager.AuthenticationHeaderName))
//                throw new UnauthorizedAccessException("Authenticate header required");

//            IEnumerable<string> headers;
//            if (!actionContext.Request.Headers.TryGetValues(TokenManager.AuthenticationHeaderName, out headers))
//                throw new UnauthorizedAccessException("Authenticate header required");

//            var xAuth = headers.FirstOrDefault();

//            // بررسی وجود و اندازه توکن دریافتی
//            ValidateXAuthFormat(xAuth);

//            return xAuth;
//        }

//        private static void CheckUserAccessToResource(string resouecePath, HttpVerb httpVerb)
//        {
//            var userService = StructureMapObjectFactory.Container.GetInstance<IUserService>();
//            var currentUser = StructureMapObjectFactory.Container.GetInstance<ICurrentUser>();

//            var hasAccess = userService.HasAccessToResource(currentUser.UserData.Id, resouecePath, httpVerb);
//            if (!hasAccess)
//            {
//                throw new UnauthorizedAccessException("Access denied!");
//            }
//        }

//        private static void ValidateXAuthFormat(string xAuth)
//        {
//            if (string.IsNullOrEmpty(xAuth) || string.IsNullOrWhiteSpace(xAuth))
//                throw new UnauthorizedAccessException("Invalid authenticate header(1)");

//            if (xAuth.Length < TokenManager.ApplicationCodeValidLength)
//                throw new UnauthorizedAccessException("Invalid authenticate header(2)");
//        }

//        private static void ValidateUserAccountStatus()
//        {
//            var activeUserIdsCacheManager = StructureMapObjectFactory.Container.GetInstance<IActiveUserCacheManager>();
//            var currentUser = StructureMapObjectFactory.Container.GetInstance<ICurrentUser>();

//            var activeUserIds = activeUserIdsCacheManager.Get();

//            if (!activeUserIds.Contains(currentUser.UserData.Id))
//                throw new CustomException("101 - حساب کاربری شما بن شده است");
//        }

//    }
//}