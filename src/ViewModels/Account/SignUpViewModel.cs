﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ViewModels.Base;

namespace ViewModels.Account
{
    public class SignUpViewModel : BaseCaptchaViewModel
    {

        [DisplayName("First name")]
        [Required(ErrorMessage = "{0} is required")]
        [StringLength(128, ErrorMessage = "Maximum length of {0} is {1}")]
        public virtual string FirstName { get; set; }


        [DisplayName("Last name")]
        [Required(ErrorMessage = "{0} is required")]
        [StringLength(128, ErrorMessage = "Maximum length of {0} is {1}")]
        public virtual string LastName { get; set; }

        [DisplayName("Email address")]
        [Required(ErrorMessage = "{0} is required")]
        [StringLength(128, ErrorMessage = "Maximum length of {0} is {1}")]
        [EmailAddress(ErrorMessage = "Email address format is not valid")]
        public virtual string EmailAddress { get; set; }

        [DisplayName("Username")]
        [Required(ErrorMessage = "{0} is required")]
        [StringLength(128, ErrorMessage = "Maximum length of {0} is {1}")]
        [RegularExpression(@"^[0-9a-z]+$", ErrorMessage = "Just lower letters(a-z) and numbers(0-9) are valid")] // just number and 
        public virtual string Username { get; set; }


        [DisplayName("Password")]
        [Required(ErrorMessage = "{0} is required")]
        [StringLength(64, ErrorMessage = "Maximum length of {0} is {1}")]
        public virtual string Password { get; set; }

        [DisplayName("Confirm password")]
        [StringLength(64, ErrorMessage = "Maximum length of {0} is {1}")]
        [Compare("Password", ErrorMessage = "Password and confirm password are not the same")]
        public virtual string RePassword { get; set; }


    }
}
