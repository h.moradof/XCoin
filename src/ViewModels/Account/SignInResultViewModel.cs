﻿using DomainModels.Entities.Access;
using DomainModels.Entities.HumanResource;

namespace ViewModels.Account
{
    public class SignInResultViewModel
    {
        public User User { get; set; }
        public Role Role { get; set; }
    }
}
