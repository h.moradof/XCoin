﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ViewModels.Base;

namespace ViewModels.Account
{
    public class SignInViewModel : BaseCaptchaViewModel
    {

        public string ReturnUrl { get; set; }


        [DisplayName("Username")]
        [Required(ErrorMessage = "{0} is required")]
        [StringLength(128, ErrorMessage = "Maximum length of {0} is {1}")]
        [RegularExpression(@"^[0-9a-z]+$", ErrorMessage = "Just lower letters(a-z) and numbers(0-9) are valid")]
        public string Username { get; set; }


        [DisplayName("Password")]
        [Required(ErrorMessage = "{0} is required")]
        [StringLength(128, ErrorMessage = "Maximum length of {0} is {1}")]
        [DataType(DataType.Password)]
        public string Password { get; set; }


        [DisplayName("Google authenticator temporary password")]
        [Required(ErrorMessage = "{0} is required")]
        [StringLength(16, ErrorMessage = "Maximum length of {0} is {1}")]
        [DataType(DataType.Password)]
        public string TemporaryPassword { get; set; }


        [DisplayName("Remember me")]
        public bool RememberMe { get; set; }

    }
}
