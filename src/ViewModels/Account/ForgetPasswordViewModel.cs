﻿using System.ComponentModel.DataAnnotations;
using ViewModels.Base;
using System.ComponentModel;

namespace ViewModels.Account
{
    public class ForgetPasswordViewModel : BaseCaptchaViewModel
    {
        [DisplayName("Email address")]
        [Required(ErrorMessage = "{0} is required")]
        [StringLength(128, ErrorMessage = "Maximum length of {0} is {1}")]
        [EmailAddress(ErrorMessage = "Email address format is not valid")]
        public string EmailAddress { get; set; }

    }
}
