﻿using System;

namespace ViewModels.Account
{
    public class ForgetPasswordSendEmail
    {
        public string FullName { get; set; }
        public string Username { get; set; }
        public string Code { get; set; }
    }
}
