﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ViewModels.Account
{
    public class ProfileViewModel
    {
        [Key]
        public long UserId { get; set; }


        [DisplayName("Fullname")]
        public string Fullname { get; set; }


        [DisplayName("Email address")]
        public virtual string EmailAddress { get; set; }  // just show


        [DisplayName("Username")]
        public string Username { get; set; } // just show


        [DisplayName("Password")]
        [StringLength(128, ErrorMessage = "Maximum length of {0} is {1}")]
        public string Password { get; set; }


        [DisplayName("Confirm password")]
        [StringLength(128, ErrorMessage = "Maximum length of {0} is {1}")]
        public string RePassword { get; set; }


        [DisplayName("I want to update my password")]
        public bool WantUpdatePassword { get; set; }

    }
}
