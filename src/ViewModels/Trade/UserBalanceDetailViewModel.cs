﻿namespace ViewModels.Trade
{
    public class UserBalanceDetailViewModel
    {
        public string CoinName { get; set; }
        public decimal BalanceAmount { get; set; }
    }
}
