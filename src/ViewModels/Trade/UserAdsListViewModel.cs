﻿using System.ComponentModel.DataAnnotations;

namespace ViewModels.Trade
{
    public class UserListAdsViewModel
    {
        [Display(Name = "Ads_AdsId", ResourceType = typeof(WebResources.Prop))]
        public long AdsId { get; set; }

        [Display(Name = "Ads_IsSell", ResourceType = typeof(WebResources.Prop))]
        public bool IsSell { get; set; }

        [Display(Name = "Ads_IsActive", ResourceType = typeof(WebResources.Prop))]
        public bool IsActive { get; set; }

        [Display(Name = "Ads_CoinName", ResourceType = typeof(WebResources.Prop))]
        public string CoinName { get; set; }

        [Display(Name = "Ads_CurrencyName", ResourceType = typeof(WebResources.Prop))]
        public string CurrencyName { get; set; }

        [Display(Name = "Ads_CurrencyUnitPriceOfCoin", ResourceType = typeof(WebResources.Prop))]
        public decimal CurrencyUnitPriceOfCoin { get; set; }

        [Display(Name = "Ads_PaymentMethodName", ResourceType = typeof(WebResources.Prop))]
        public string PaymentMethodName { get; set; }

        [Display(Name = "Ads_CountryName", ResourceType = typeof(WebResources.Prop))]
        public string CountryName { get; set; }

        [Display(Name = "Ads_MinimumCurrencyPrice", ResourceType = typeof(WebResources.Prop))]
        public decimal MinimumCurrencyPrice { get; set; }

        [Display(Name = "Ads_MaximumCurrencyPrice", ResourceType = typeof(WebResources.Prop))]
        public decimal MaximumCurrencyPrice { get; set; }
    }
}
