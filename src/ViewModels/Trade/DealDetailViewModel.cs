﻿using DomainModels.Entities.HumanResource;
using DomainModels.Entities.Trade;

namespace ViewModels.Trade
{
    public class DealDetailViewModel
    {
        public User Saler { get; set; }
        public User Buyer { get; set; }
        public Deal Deal { get; set; }
        public DealStatus DealStatus { get; set; }
        public PaymentMethod PaymentMethod { get; set; }
        public Coin Coin { get; set; }
        public Currency Currency { get; set; }
        public Country Country { get; set; }
    }
}
