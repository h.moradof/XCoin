﻿using System.ComponentModel.DataAnnotations;

namespace ViewModels.Trade
{
    public class AdsDetailViewModel : AdsSearchResultViewModel
    {
        public string Description { get; set; }
    }
}
