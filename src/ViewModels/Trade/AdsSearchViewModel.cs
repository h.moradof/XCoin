﻿using Infrastructure.Pagination;

namespace ViewModels.Trade
{
    public class AdsSearchViewModel : AdsSearchFormViewModel
    {
        public PagedList<AdsSearchResultViewModel> Adses { get; set; }
    }
}
