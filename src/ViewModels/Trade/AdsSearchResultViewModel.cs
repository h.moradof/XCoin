﻿using System.ComponentModel.DataAnnotations;

namespace ViewModels.Trade
{
    public class AdsSearchResultViewModel
    {
        public long AdsId { get; set; }

        public bool IsSell { get; set; }

        public string Username { get; set; }

        public long UserId { get; set; }

        [Display(Name = "Ads_UserRating", ResourceType = typeof(WebResources.Prop))]
        public decimal UserRating { get; set; }

        [Display(Name = "Ads_IsUserVerified", ResourceType = typeof(WebResources.Prop))]
        public bool IsUserVerified { get; set; } // from roleId

        public string CoinName { get; set; }

        [Display(Name = "Ads_CurrencyName", ResourceType = typeof(WebResources.Prop))]
        public string CurrencyName { get; set; }

        [Display(Name = "Ads_CurrencyUnitPriceOfCoin", ResourceType = typeof(WebResources.Prop))]
        public decimal CurrencyUnitPriceOfCoin { get; set; }

        [Display(Name = "Ads_PaymentMethodName", ResourceType = typeof(WebResources.Prop))]
        public string PaymentMethodName { get; set; }

        [Display(Name = "Ads_CountryName", ResourceType = typeof(WebResources.Prop))]
        public string CountryName { get; set; }

        [Display(Name = "Ads_MinimumCurrencyPrice", ResourceType = typeof(WebResources.Prop))]
        public decimal MinimumCurrencyPrice { get; set; }

        [Display(Name = "Ads_MaximumCurrencyPrice", ResourceType = typeof(WebResources.Prop))]
        public decimal MaximumCurrencyPrice { get; set; }
    }
}
