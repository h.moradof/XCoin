﻿using System.Collections.Generic;
using ViewModels.Base;

namespace ViewModels.Trade
{
    public class AdsSearchFormViewModel
    {
        public List<BaseDdlBaseOnSlugViewModel> Coins { get; set; }
        public List<BaseDdlBaseOnSlugViewModel> Countries { get; set; }
        public List<BaseDdlBaseOnSlugViewModel> Currencies { get; set; }
        public List<BaseDdlBaseOnSlugViewModel> PaymentMethods { get; set; }

    }
}
