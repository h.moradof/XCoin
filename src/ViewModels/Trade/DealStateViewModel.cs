﻿using System;

namespace ViewModels.Trade
{
    public class DealStateViewModel
    {
        public string StatusName { get; set; }
        public string StatusDisplayName { get; set; }

        public bool IsBuyer { get; set; }
        public bool IsSaler { get; set; }

        public bool IsBuyerPaidCurrency { get; set; }
        public bool IsSalerReleasedCoin { get; set; }

        public string CurrencyName { get; set; }
        public string CoinName { get; set; }
        public string PaymentMethod { get; set; }

        public decimal CoinAmount { get; set; }
        public decimal TotalCurrencyPrice { get; set; }

        public DateTime BuyerPayEndTime  { get; set; }
        public string _BuyerPayEndTime { get { return BuyerPayEndTime.ToString("yyyy/MM/dd HH:mm"); } }

        public DateTime CreatedOn { get; set; }
        public string _CreatedOn { get { return CreatedOn.ToString("yyyy/MM/dd HH:mm"); } }

        public DateTime? PaidDate { get; set; }
        public string _PaidDate { get { return (PaidDate.HasValue) ? PaidDate.Value.ToString("yyyy/MM/dd HH:mm") : "---"; } }

        public DateTime? ReleasedDate { get; set; }
        public string _ReleasedDate { get { return (ReleasedDate.HasValue) ? ReleasedDate.Value.ToString("yyyy/MM/dd HH:mm") : "---"; } }

        public int BuyerRemainTimeToPayInMinute { get; set; }

        public bool IsBuyerHasTimeToPay { get; set; }

        public string SalerUsername { get; set; }
        public string BuyerUsername { get; set; }

        public Guid? JudgeTicketRowGuid { get; set; }
    }

}
