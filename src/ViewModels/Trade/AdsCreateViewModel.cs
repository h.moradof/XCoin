﻿using System.ComponentModel.DataAnnotations;

namespace ViewModels.Trade
{
    public class AdsCreateViewModel
    {

        [Display(Name = "Ads_CoinName", ResourceType = typeof(WebResources.Prop))]
        public long CoinId { get; set; }

        [Display(Name = "Ads_CurrencyName", ResourceType = typeof(WebResources.Prop))]
        public long CurrencyId { get; set; }

        [Display(Name = "Ads_CountryName", ResourceType = typeof(WebResources.Prop))]
        public long CountryId { get; set; }

        [Display(Name = "Ads_PaymentMethodName", ResourceType = typeof(WebResources.Prop))]
        public long PaymentMethodId { get; set; }

        [Display(Name = "Ads_MinimumCurrencyPrice", ResourceType = typeof(WebResources.Prop))]
        [Required(ErrorMessage = "{0} is required")]
        public decimal MinimumCurrencyPrice { get; set; }

        [Display(Name = "Ads_MaximumCurrencyPrice", ResourceType = typeof(WebResources.Prop))]
        [Required(ErrorMessage = "{0} is required")]
        public decimal MaximumCurrencyPrice { get; set; }

        [Display(Name = "Ads_CurrencyUnitPriceOfCoin", ResourceType = typeof(WebResources.Prop))]
        [Required(ErrorMessage = "{0} is required")]
        public decimal CurrencyUnitPriceOfCoin { get; set; }

        [Display(Name = "Ads_Description", ResourceType = typeof(WebResources.Prop))]
        [StringLength(512, ErrorMessage = "Maximum length of {0} is {1}")]
        public string Description { get; set; }

        [Display(Name = "Ads_IsActive", ResourceType = typeof(WebResources.Prop))]
        [Required(ErrorMessage = "{0} is required")]
        public bool IsActive { get; set; }

        [Display(Name = "Ads_IsSell", ResourceType = typeof(WebResources.Prop))]
        [Required(ErrorMessage = "{0} is required")]
        public bool IsSell { get; set; }

    }
}
