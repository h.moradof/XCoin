﻿namespace ViewModels.Trade
{
    public class AdsSearchQueryStringParamViewModel
    {
        public string Country { get; set; }
        public string Currency { get; set; }
        public decimal? Amount { get; set; }
        public string Coin { get; set; }
        public int Page { get; set; }
        public string SortBy { get; set; }
        public string SortMethod { get; set; }
        public bool IsSell { get; set; }
        public string PaymentMethod { get; set; }

    }
}
