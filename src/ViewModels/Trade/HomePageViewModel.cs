﻿using Infrastructure.Pagination;

namespace ViewModels.Trade
{
    public class HomePageViewModel : AdsSearchFormViewModel
    {
        public PagedList<AdsSearchResultViewModel> LastBuyAds { get; set; }
        public PagedList<AdsSearchResultViewModel> LastSellAds { get; set; }
    }
}
