﻿using DomainModels.Entities.Trade;

namespace ViewModels.Trade
{
    public class ChatListViewModel
    {
        public Chat Chat { get; set; }
        public string SenderFullName  { get; set; }
    }
}
