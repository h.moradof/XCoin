﻿using DomainModels.Entities.Support;
using System.Collections.Generic;

namespace ViewModels.Support
{
    public class TicketDetailViewModel
    {
        public string SenderFullName { get; set; }
        public Ticket Ticket { get; set; }
        public List<TicketReplyListViewModel>  TicketReplies { get; set; }
    }
}
