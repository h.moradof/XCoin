﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ViewModels.Support
{
    public class ReplyViewModel
    {
        [DisplayName("Message")]
        [Required( ErrorMessage = "{0} is required")]
        public string Message { get; set; }

        public Guid RowGuid { get; set; }

    }
}
