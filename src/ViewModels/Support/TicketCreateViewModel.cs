﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ViewModels.Support
{
    public class TicketCreateViewModel
    {
       
        [DisplayName("Subject")]
        [Required(ErrorMessage = "{0} is required")]
        [StringLength(256, ErrorMessage = "Maximum length of {0} is {1}")]
        public virtual string Subject { get; set; }


       
        [DisplayName("Message")]
        [Required(ErrorMessage = "{0} is required")]
        [StringLength(2048, ErrorMessage = "Maximum length of {0} is {1}")]
        public virtual string Message { get; set; }
    }
}
