﻿using DomainModels.Entities.Support;

namespace ViewModels.Support
{
    public class TicketReplyListViewModel
    {
        public string SenderFullName { get; set; }
        public TicketReply TicketReply { get; set; }
    }
}
