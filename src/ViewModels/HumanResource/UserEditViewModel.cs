﻿using DomainModels.Entities.Base;
using DomainModels.Entities.HumanResource;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ViewModels.HumanResource
{
    public class UserEditViewModel
    {
        public long Id { get; set; }

        [DisplayName("گروه کاربری")]
        [Required(ErrorMessage = "{0} الزامی است")]
        public long RoleId { get; set; }

        [DisplayName("نام و نام خانوادگی")]
        [Required(ErrorMessage = "{0} الزامی است")]
        [StringLength(128, ErrorMessage = "حداکثر طول مجاز برای {0} {1} کاراکتر می باشد")]       
        public string FirstName { get; set; }

        [DisplayName("نام و نام خانوادگی")]
        [Required(ErrorMessage = "{0} الزامی است")]
        [StringLength(128, ErrorMessage = "حداکثر طول مجاز برای {0} {1} کاراکتر می باشد")]
        public string LastName { get; set; }

        [DisplayName("نام کاربری (موبایل)")]
        [Required(ErrorMessage = "{0} الزامی است")]
        [StringLength(64, ErrorMessage = "حداکثر طول مجاز برای {0} {1} کاراکتر می باشد")]
        [RegularExpression(@"^[0-9]{11}$", ErrorMessage = "فرمت نام کاربری نادرست است")] // شماره موبایل
        public string Username { get; set; }

        [DisplayName("آدرس ایمیل")]
        [StringLength(128, ErrorMessage = "حداکثر طول مجاز برای {0} {1} کاراکتر می باشد")]
        [EmailAddress(ErrorMessage = "فرمت آدرس ایمیل نادرست است")]
        public string EmailAddress { get; set; }

        [DisplayName("کد ملی")]
        [StringLength(32, ErrorMessage = "حداکثر طول مجاز برای {0} {1} کاراکتر می باشد")]
        public string NationalCode { get; set; }

        [DisplayName("وضعیت حساب کاربری")]
        public AccountStatus AccountStatus { get; set; }

        [DisplayName("دلیل بن")]
        public string BanReason { get; set; }

        [DisplayName("کلمه عبور")]
        [StringLength(2048, ErrorMessage = "حداکثر طول مجاز برای {0} {1} کاراکتر می باشد")]
        public string Password { get; set; }

        [DisplayName("می خواهم کلمه عبور را ویرایش نمایم")]
        public bool WantChangePassword { get; set; }

        public UserEditViewModel()
        {
            WantChangePassword = false;
        }

    }
}
