﻿using DomainModels.Entities.HumanResource;
using System.Collections.Generic;

namespace ViewModels.HumanResource
{
    public class UserDetailViewModel
    {
        public User User { get; set; }
        public List<User> Childs { get; set; }
        public User Parent { get; set; }
    }
}
