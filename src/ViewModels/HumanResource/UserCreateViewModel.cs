﻿using DomainModels.Entities.Base;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ViewModels.HumanResource
{
    public class UserCreateViewModel
    {
        [DisplayName("گروه کاربری")]
        [Required(ErrorMessage = "{0} الزامی است")]
        public long RoleId { get; set; }

        [DisplayName("نام")]
        [StringLength(128, ErrorMessage = "حداکثر طول مجاز برای {0} {1} کاراکتر می باشد")]
        [Required(ErrorMessage = "{0} الزامی است")]
        public string FirstName { get; set; }

        [DisplayName("نام خانوادگی")]
        [StringLength(128, ErrorMessage = "حداکثر طول مجاز برای {0} {1} کاراکتر می باشد")]
        [Required(ErrorMessage = "{0} الزامی است")]
        public string LastName { get; set; }

        [DisplayName("نام کاربری (موبایل)")]
        [Required(ErrorMessage = "{0} الزامی است")]
        [StringLength(64, ErrorMessage = "حداکثر طول مجاز برای {0} {1} کاراکتر می باشد")]
        [RegularExpression(@"^[0-9]{11}$", ErrorMessage = "فرمت نام کاربری نادرست است")] // شماره موبایل
        public string Username { get; set; }

        [DisplayName("آدرس ایمیل")]
        [StringLength(128, ErrorMessage = "حداکثر طول مجاز برای {0} {1} کاراکتر می باشد")]
        [EmailAddress(ErrorMessage = "فرمت آدرس ایمیل نادرست است")]
        public string EmailAddress { get; set; }

        [DisplayName("کد ملی")]
        [StringLength(32, ErrorMessage = "حداکثر طول مجاز برای {0} {1} کاراکتر می باشد")]
        public string NationalCode { get; set; }

        [DisplayName("کلمه عبور")]
        [Required(ErrorMessage = "{0} الزامی است")]
        [StringLength(2048, ErrorMessage = "حداکثر طول مجاز برای {0} {1} کاراکتر می باشد")]
        public string Password { get; set; }

        [DisplayName("تکرار کلمه عبور")]
        [Required(ErrorMessage = "{0} الزامی است")]
        [StringLength(2048, ErrorMessage = "حداکثر طول مجاز برای {0} {1} کاراکتر می باشد")]
        [Compare("Password", ErrorMessage = "کلمه عبور و تکرار کلمه عبور یکسان نیستند")]
        public string RePassword { get; set; }

    }
}
