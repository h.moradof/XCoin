﻿using DomainModels.Entities.HumanResource;

namespace ViewModels.HumanResource
{
    public class VerifyViewModel
    {
        public AcceptStatus AcceptStatus { get; set; }
        public string PhotoContent { get; set; }
    }
}
