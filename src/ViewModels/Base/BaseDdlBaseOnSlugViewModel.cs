﻿namespace ViewModels.Base
{
    public class BaseDdlBaseOnSlugViewModel
    {
        public string Slug { get; set; }
        public string Name { get; set; }
    }
}
