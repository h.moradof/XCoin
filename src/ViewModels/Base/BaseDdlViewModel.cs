﻿namespace ViewModels.Base
{
    public class BaseDdlViewModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}
