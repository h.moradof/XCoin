﻿namespace ViewModels.Base
{
    public class BaseCheckboxItem
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}
