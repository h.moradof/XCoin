﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ViewModels.Base
{
    public abstract class BaseCaptchaViewModel
    {
        // additional prop
        [DisplayName("Security image text")]
        [Required(ErrorMessage = "{0} is required")]
        public string Captcha { get; set; }


        public string cc { get; set; }
    }
}
