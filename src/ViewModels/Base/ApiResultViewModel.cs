﻿namespace ViewModels.Base
{
    public class ApiResultViewModel
    {
        public object Data { get; set; }
        public bool HasError { get; set; }
        public string Message { get; set; }


        public ApiResultViewModel()
        {
            Data = null;
            HasError = false;
            Message = string.Empty;
        }
    }
}
