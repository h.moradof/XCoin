﻿using DomainModels.Entities.Access;
using System.Collections.Generic;

namespace ViewModels.Access
{
    // for cache
    public class AccessViewModel
    {
        public IEnumerable<Role> Roles {get;set;}
        public IEnumerable<RolePermission> RolePermissions { get;set;}
        public IEnumerable<Permission> Permissions { get;set;}
    }
}
