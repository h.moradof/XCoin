﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ViewModels.Access
{
    public class RoleEditViewModel
    {
        public long Id { get; set; }

        [DisplayName("عنوان")]
        [Required(ErrorMessage = "{0} الزامی است")]
        [StringLength(128, ErrorMessage = "حداکثر طول مجاز برای {0} {1} کاراکتر می باشد")]
        public string Name { get; set; }

        [DisplayName("دسترسی ها")]
        public long[] PermissionIds { get; set; }

    }
}
