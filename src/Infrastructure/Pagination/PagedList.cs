﻿using Infrastructure.Pagination.Base;
using System.Collections.Generic;

namespace Infrastructure.Pagination
{
    public sealed class PagedList<T> : BasePagedList
    {
        public IList<T> Data { get; set; }

        public PagedList()
        {
            Data = new List<T>();
        }
    }
}
