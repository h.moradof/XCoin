﻿using Infrastructure.Common;
using System.Text.RegularExpressions;

namespace Infrastructure.Security.Password
{
    public static class PasswordValidator
    {

        public const int MinimumLength = 8;

        public static PasswordValidateResult Validate(string password)
        {
            if (string.IsNullOrEmpty(password)) { return PasswordValidateResult.PasswordIsRequired; }

            if (password.Length < MinimumLength) { return PasswordValidateResult.IsInvalidLengthIsSmall; }

            if (!password.ContainsNumber()) { return PasswordValidateResult.IsInvalidNotContainNumber; }

            return PasswordValidateResult.IsValid;
        }


        //private static bool IsVeryStrong(string password)
        //{
        //    // upper and lower character and a number and a special character
        //    var regx = new Regex(@"((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%!?.&]).{8,64})");

        //    return regx.IsMatch(password);

        //}

    }
}
