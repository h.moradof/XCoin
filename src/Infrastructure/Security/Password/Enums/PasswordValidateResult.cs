﻿namespace Infrastructure.Security.Password
{
    public enum PasswordValidateResult
    {
        IsValid,
        IsInvalidLengthIsSmall,
        IsInvalidNotContainNumber,
        PasswordIsRequired
    }
}
