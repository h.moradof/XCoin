﻿using System;

namespace Infrastructure.Security
{
    public static class CaptchaManager
    {
        public static string GenerateHash(string data, int timeOut = 5)
        {
            return DataEncryptor.EncryptTpl(data + "_" + DateTime.Now.AddMinutes(timeOut).ToString("yyyy-MM-dd-HH-mm"));
        }

        public static bool IsValid(string hashData, string data)
        {
            string[] cParts = DataEncryptor.DecryptTpl(hashData).Split('_');

            if (data != cParts[0])
            {
                return false;
            }

            string[] expDateParts = cParts[1].Split('-');
            DateTime now = DateTime.Now;
            DateTime expDate = new DateTime(int.Parse(expDateParts[0]), int.Parse(expDateParts[1]), int.Parse(expDateParts[2]),
                int.Parse(expDateParts[3]), int.Parse(expDateParts[4]), 0);
            
            if (expDate < now)
	        {
		        return false;
	        }
            else
	        {
                return true;
	        }

        }
    }
}