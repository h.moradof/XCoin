﻿namespace Infrastructure.File
{
    public struct FileUploadResult
    {
        public FileManager.UploadResult UploadResult;
        public string UploadedFileName, ErrorMessage;

        public FileUploadResult(FileManager.UploadResult uploadResult, string uploadedFileName, string errorMessage)
        {
            UploadResult = uploadResult;
            UploadedFileName = uploadedFileName;
            ErrorMessage = errorMessage;
        }
    }
}