﻿namespace Infrastructure.File
{
    public enum EntityName
    {
        Photo,
        Certificate,
        UploadCenter
    }
}
