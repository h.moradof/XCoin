﻿//using Infrastructure.Core;
using Infrastructure.Cache.Interfaces;
using System.Configuration;
using System.IO;
using System.Web.Hosting;

namespace Infrastructure.File
{
    public static class UploadFilePathManager
    {
        public const string RootPath = "~/up/";
        private const string UploadFolderName = "up";


        #region upload control methods

        /// <summary>
        /// دریافت مسیر وبی محل آپلود فابل ها
        /// </summary>
        public static string GetUploadRootPath()
        {
            return string.Format("~/{0}/", UploadFolderName);
        }


        /// <summary>
        /// دریافت مسیر وبی محل آپلود فابل ها
        /// </summary>
        public static string GetUploadPath(EntityName entityName)
        {
            return string.Format("~/{0}/{1}/", UploadFolderName, entityName.ToString().ToLowerInvariant());
        }


        /// <summary>
        /// دریافت مسیر وبی محل آپلود فابل ها
        /// </summary>
        public static string GetUploadPathWithoutRootCharacter(EntityName entityName)
        {
            return string.Format("{0}/{1}/", UploadFolderName, entityName.ToString().ToLowerInvariant());
        }


        /// <summary>
        /// دریافت مسیر فیزیکی یا ویندوزی محل آپلود فایل ها
        /// </summary>
        public static string GetUploadPhysicalPath(EntityName entityName)
        {
            return HostingEnvironment.MapPath(GetUploadPath(entityName));
        }

        public static string GetUplodedFileFullUrl(EntityName entityName, string fileName, ISeoSettingCacheManager siteSeoSettingCacheManager)
        {
            return string.Format("http://{0}/{1}{2}", WebsiteDomain, GetUploadPathWithoutRootCharacter(entityName), fileName);
        }

        public static string GetUplodedFileFullActionUrl(string fileName, ISeoSettingCacheManager siteSeoSettingCacheManager)
        {
            return string.Format("http://{0}/File/Image/?fileName={1}", WebsiteDomain, fileName);
        }

        private static string WebsiteDomain = ConfigurationManager.AppSettings["WebsiteDomin"].ToString();

        #endregion

    }
}