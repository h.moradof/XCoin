﻿namespace Infrastructure.Mail.Interfaces
{
    public interface IUserAccountMailSender
    {
        void SendEditUserEmail(string subject, string userEmailAddress, string lang);
        void SendForgetPasswordEmail(string subject, string userEmailAddress, string memberFullname, string memberUsername, string resetPasswordCode, string lang);
    }
}
