﻿using Infrastructure.Cache.Interfaces;
using Infrastructure.Mail.Interfaces;
using System.Text;
using DomainModels.Entities.Setting;
using Infrastructure.Cache.Model;

namespace Infrastructure.Mail
{
    public class UserAccountMailSender: IUserAccountMailSender
    {

        #region props
        private readonly IMailSender _mailSender;
        private readonly ISeoSettingCacheManager _seoSettingCacheManager;
        private readonly IApplicationSettingCacheManager _applicationSettingCacheManager;

        private readonly SeoSetting _seoSetting;
        private readonly ApplicationSetting _applicationSetting;
        #endregion

        #region ctor
        public UserAccountMailSender(IMailSender mailSender, ISeoSettingCacheManager seoSettingCacheManager, IApplicationSettingCacheManager applicationSettingCacheManager)
        {
            _mailSender = mailSender;
            _seoSettingCacheManager = seoSettingCacheManager;
            _applicationSettingCacheManager = applicationSettingCacheManager;


            _seoSetting = _seoSettingCacheManager.GetDefaultSetting();
            _applicationSetting = _applicationSettingCacheManager.Get();
        }
        #endregion


        public void SendEditUserEmail(string subject, string userEmailAddress, string lang)
        {
            var body = new StringBuilder(EmailPatternManager.GetEditUserEmailPattern(lang));
            body.Replace("[WebsiteDomain]", _applicationSetting.WebsiteDomain);
            body.Replace("[SiteTitle]", _seoSetting.Title);

            _mailSender.Send(_seoSetting.Title, userEmailAddress, subject, body.ToString());
        }

        public void SendForgetPasswordEmail(string subject, string userEmailAddress, string memberFullname, string memberUsername, string resetPasswordCode, string lang)
        {
            var body = new StringBuilder(EmailPatternManager.GetForgetPasswordEmailPattern(lang));
            body.Replace("[WebsiteDomain]", _applicationSetting.WebsiteDomain);
            body.Replace("[SiteTitle]", _seoSetting.Title);
            body.Replace("[ResetPasswordCode]", resetPasswordCode);
            body.Replace("[MemberFullname]", memberFullname);
            body.Replace("[MemberUsername]", memberUsername);

            _mailSender.Send(_seoSetting.Title, userEmailAddress, subject, body.ToString());
        }

    }
}
