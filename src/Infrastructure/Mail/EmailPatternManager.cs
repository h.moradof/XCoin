﻿using System.IO;
using System.Web.Hosting;
// ReSharper disable All

namespace Infrastructure.Mail
{
    public static class EmailPatternManager
    {

        // --------- New Member Creation Email Pattern ---------
        private const string EDIT_USER_PATTERN_FILE_PATH = "~/Views/EmailPattern/_EditUser.{0}.cshtml";

        public static string GetEditUserEmailPattern(string lang)
        {
            string viewFileName = string.Format(EDIT_USER_PATTERN_FILE_PATH, lang);

            using (var reader = new StreamReader(HostingEnvironment.MapPath(viewFileName)))
            {
                return reader.ReadToEnd();
            }
        }


        // --------- Forget Password Email Pattern ---------
        private const string FORGET_PASSWORD_PATTERN_FILE_PATH = "~/Views/EmailPattern/_ForgetPassword.{0}.cshtml";

        public static string GetForgetPasswordEmailPattern(string lang)
        {
            string viewFileName = string.Format(FORGET_PASSWORD_PATTERN_FILE_PATH, lang);

            using (var reader = new StreamReader(HostingEnvironment.MapPath(viewFileName)))
            {
                return reader.ReadToEnd();
            }
        }



    }
}