﻿using Newtonsoft.Json.Linq;

namespace Infrastructure.WebServices
{
    public interface IWebServiceManager
    {
        JObject GET(string url);

        JObject POST(string url, string postData);

        JArray GetArray(string url);

        JArray PostArray(string url);
    }
}