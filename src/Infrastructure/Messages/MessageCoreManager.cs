﻿using System.ComponentModel;

namespace Infrastructure.Messages
{
    public static class MessageCoreManager
    {
        public enum MessageType
        {
            [Description("Inserted successfully")]
            SuccessInsert,
            [Description("Updated successfully")]
            SuccessUpdate,
            [Description("deleted successfully")]
            SuccessDelete,
            [Description("Insert was not successful")]
            ErrorInsert,
            [Description("Update was not successful")]
            ErrorUpdate,
            [Description("Delete was not successful")]
            ErrorDelete,
        }

    }
}