﻿
namespace Infrastructure.Messages
{
    public sealed class Message
    {
        public string MessageBody { get; set; }
        public bool IsSuccess { get; set; }
    }
}