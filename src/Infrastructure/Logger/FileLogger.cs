﻿using System;
using System.IO;
using System.Web.Hosting;

namespace Infrastructure.Logger
{
    public static class FileLogger
    {
        public static void Log(string message, string filePath = "~/up/log.txt")
        {
            try
            {
                using (var w = new StreamWriter(HostingEnvironment.MapPath(filePath), true))
                {
                    w.WriteLine("-------------------- " + DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss") + " --------------------");
                    w.WriteLine(message);
                }
            }
            catch (Exception)
            { }
        }
    }
}