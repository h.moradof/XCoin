﻿using System.Collections.Generic;

namespace Infrastructure.MultiLanguage
{
    public static class LanguageManager
    {

        public static string GetDefaultLanaguage()
        {
            return "en";
        }

        public static List<string> GetSupportedLocales()
        {
            return new List<string> { "en" };
        }

    }
}
