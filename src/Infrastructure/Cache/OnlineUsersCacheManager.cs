﻿namespace Infrastructure.Cache
{
    public class OnlineUsersCacheManager
    {
        #region singleton members

        private static OnlineUsersCacheManager _instance;
        private static object _lock = new object();
        private OnlineUsersCacheManager()
        {  }

        public static OnlineUsersCacheManager GetInstance()
        {
            if (_instance == null)
            {
                lock (_lock)
                {
                    if (_instance == null)
                        _instance = new OnlineUsersCacheManager();
                }
            }
            return _instance;
        }

        #endregion

        private int _onlineUsersCount;


        public void Plus()
        {
            _onlineUsersCount++;
        }

        public void Minus()
        {
            if (_onlineUsersCount > 0)
                _onlineUsersCount--;
        }

        public int Count
        {
            get
            {
                return _onlineUsersCount;
            }
        }

    }
}
