﻿using DomainModels.Entities.Setting;
using Infrastructure.Cache.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace Infrastructure.Cache
{
    public class SeoSettingCacheManager : ISeoSettingCacheManager
    {

        #region fileds

        private IEnumerable<SeoSetting> _data;

        #endregion


        public void Cache(IEnumerable<SeoSetting> data)
        {
            _data = data;
        }

        public SeoSetting GetDefaultSetting()
        {
            return _data.FirstOrDefault();
        }

        public List<SeoSetting> GetAll()
        {
            return _data.ToList<SeoSetting>();
        }

    }
}
