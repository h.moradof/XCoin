﻿using Infrastructure.Cache.Interfaces;
using Infrastructure.Cache.Model;

namespace Infrastructure.Cache
{
    public class ApplicationSettingCacheManager : IApplicationSettingCacheManager
    {

        #region fields

        private ApplicationSetting _data;

        #endregion


        public void Cache(ApplicationSetting data)
        {
            _data = data;
        }

        public ApplicationSetting Get()
        {
            return _data;
        }
    }
}
