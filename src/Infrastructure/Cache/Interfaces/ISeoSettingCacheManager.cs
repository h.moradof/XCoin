﻿using System.Collections.Generic;
using DomainModels.Entities.Setting;

namespace Infrastructure.Cache.Interfaces
{
    public interface ISeoSettingCacheManager
    {
        void Cache(IEnumerable<SeoSetting> data);
        List<SeoSetting> GetAll();
        SeoSetting GetDefaultSetting();
    }
}