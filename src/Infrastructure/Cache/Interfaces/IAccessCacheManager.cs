﻿using System.Collections.Generic;
using DomainModels.Entities.Access;

namespace Infrastructure.Cache.Interfaces
{
    public interface IAccessCacheManager
    {
        void Cache(IEnumerable<Permission> permissions, IEnumerable<Role> roles, IEnumerable<RolePermission> rolePermissions);
        bool IsResourcePublic(string path, HttpVerb httpVerb);
    }
}