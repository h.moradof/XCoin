﻿namespace Infrastructure.Cache.Model
{
    public sealed class ApplicationSetting
    {
        public string WebsiteDomain { get; set; }
        public string DefaultPicName { get; set; }
        public bool AllowNotAjaxRequests { get; set; }

        public MailSetting MailSetting { get; set; }
        public SmsSetting SmsSetting { get; set; }
    }
}
