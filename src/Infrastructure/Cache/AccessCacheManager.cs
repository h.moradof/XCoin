﻿using DomainModels.Entities.Access;
using Infrastructure.Cache.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace Infrastructure.Cache
{

    /// <summary>
    /// Cache all accesses
    /// </summary>
    public sealed class AccessCacheManager : IAccessCacheManager
    {
        #region fileds

        private IEnumerable<Permission> _permissions;
        private IEnumerable<Role> _roles;
        private IEnumerable<RolePermission> _rolePermissions;

        #endregion


        public void Cache(IEnumerable<Permission> permissions, IEnumerable<Role> roles, IEnumerable<RolePermission> rolePermissions)
        {
            _permissions = permissions;
            _roles = roles;
            _rolePermissions = rolePermissions;
        }


        public bool IsResourcePublic(string path, HttpVerb httpVerb)
        {
            var query = (from p in _permissions
                         join rp in _rolePermissions on p.Id equals rp.PermissionId
                         join r in _roles on rp.RoleId equals r.Id
                         where (p.Path == path) && (p.HttpVerb == httpVerb)
                         select r);

            var role =  query.FirstOrDefault();

            if (role == null)
                return false;

            return (role.Name == RoleName.AnonymousUsers);
        }

    }
}
