﻿using System;
using System.Web;

namespace Infrastructure.Cookie
{
    public static class CookieManager
    {
        public static void CreateCookie(string cookieName, string data, DateTime expiredDate, string domain = null, string path = null)
        {
            DeleteCookie(cookieName);

            var cookie = new HttpCookie(cookieName, data);
            cookie.Expires = expiredDate;

            if(!string.IsNullOrEmpty(domain))
                cookie.Domain = domain;

            if (!string.IsNullOrEmpty(path))
                cookie.Path = path;

            HttpContext.Current.Request.Cookies.Add(cookie);
        }


        public static string GetCookieData(string cookieName)
        {
            HttpCookie currentUserCookie = HttpContext.Current.Request.Cookies[cookieName];
            if (currentUserCookie == null)
                return null;

            return currentUserCookie.Value;
        }


        public static void DeleteCookie(string cookieName)
        {
            HttpCookie currentUserCookie = HttpContext.Current.Request.Cookies[cookieName];
            if (currentUserCookie == null)
                return;

            HttpContext.Current.Response.Cookies.Remove(cookieName);
            currentUserCookie.Expires = DateTime.Now.AddDays(-10);
            currentUserCookie.Value = null;
            HttpContext.Current.Response.SetCookie(currentUserCookie);
        }

    }
}
