﻿namespace Infrastructure.Sms.Interfaces
{
    public interface ISmsSender
    {
        SendSmsResult Send(string mobileNumber, string message);
    }
}
