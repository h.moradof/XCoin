﻿using System.Collections.Generic;

namespace Infrastructure.Sms
{
    public struct SendSmsResult
    {
        public bool IsSuccess;
        public string Message;

        public SendSmsResult(bool isSuccess, string message)
        {
            IsSuccess = isSuccess;
            Message = message;
        }

    }
}
