﻿using System;
using System.Globalization;

namespace Infrastructure.Common
{
    public static class DateUtility
    {

        public static DateTime[] GetCurrentMonthDateRange()
        {
            var now = DateTime.Now;
            var pCalendar = new PersianCalendar();

            var startDate = pCalendar.ToDateTime(pCalendar.GetYear(now), pCalendar.GetMonth(now), 1, 0 , 0, 0, 0);
            var endDate = pCalendar.ToDateTime(pCalendar.GetYear(now), pCalendar.GetMonth(now), GetLastDayOfShamsiMonth(pCalendar.GetMonth(now)), 0, 0, 0, 0);

            return new DateTime[]
            {
                startDate,
                endDate
            };
        }

        public static DateTime[] GetDateRange(DateTime date)
        {
            var pCalendar = new PersianCalendar();

            var startDate = pCalendar.ToDateTime(pCalendar.GetYear(date), pCalendar.GetMonth(date), 1, 0, 0, 0, 0);
            var endDate = pCalendar.ToDateTime(pCalendar.GetYear(date), pCalendar.GetMonth(date), GetLastDayOfShamsiMonth(pCalendar.GetMonth(date)), 0, 0, 0, 0);

            return new DateTime[]
            {
                startDate,
                endDate
            };
        }


        private static int GetLastDayOfShamsiMonth(int monthNumber)
        {
            if (monthNumber >= 7 && monthNumber <= 11)
                return 30;
            else if (monthNumber == 12)
                return 29;
            else
                return 31;
        }
    }
}
