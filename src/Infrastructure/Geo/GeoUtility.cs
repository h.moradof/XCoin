﻿using System;
using System.Data.Entity.Spatial;

namespace Infrastructure.Geo
{
    public static class GeoUtility
    {
        public static DbGeography CreatePoint(double latitude, double longitude, int srid = 4326)
        {
            string wkt = String.Format("POINT({0} {1})", longitude, latitude);
            return DbGeography.PointFromText(wkt, srid);
        }
    }
}
