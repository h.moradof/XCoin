﻿using System.Collections.Generic;

namespace Infrastructure.IQueryableExtensions
{
    public struct QueryableFilter
    {
        public string Predicate { get; set; }
        public List<object> Parameters { get; set; }

        public QueryableFilter(string predicate, List<object> parameters)
        {
            Predicate = predicate;
            Parameters = parameters;
        }
    }
}
