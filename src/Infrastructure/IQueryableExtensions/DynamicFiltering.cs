﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Infrastructure.IQueryableExtensions
{
    public static class QueryableFilterConverter
    {

        #region #fields

        static List<Type> predefinedTypes = new List<Type>() {
                typeof(Object),
                typeof(Nullable<Boolean>),
                typeof(Boolean),
                typeof(Char),
                typeof(String),
                typeof(Nullable<SByte>),
                typeof(SByte),
                typeof(Nullable<Byte>),
                typeof(Byte),
                typeof(Nullable<Int16>),
                typeof(Int16),
                typeof(Nullable<UInt16>),
                typeof(UInt16),
                typeof(Nullable<Int32>),
                typeof(Int32),
                typeof(Nullable<UInt32>),
                typeof(UInt32),
                typeof(Nullable<Int64>),
                typeof(Int64),
                typeof(Nullable<UInt64>),
                typeof(UInt64),
                typeof(Nullable<Single>),
                typeof(Single),
                typeof(Nullable<Double>),
                typeof(Double),
                typeof(Nullable<Decimal>),
                typeof(Decimal),
                typeof(Nullable<DateTime>),
                typeof(DateTime),
                typeof(TimeSpan),
                typeof(Nullable<Guid>),
                typeof(Guid),
                typeof(Math),
                typeof(Convert)
            };

        #endregion

        public static QueryableFilter ConvertEntityToQueryableFilter<TEntity>(TEntity entity)
        {
            var queryableFilter = new QueryableFilter(string.Empty, new List<object>());
            var queryBuilder = new StringBuilder();
            var Expression = string.Empty;

            foreach (var property in entity.GetType().GetProperties())
            {
                object itemValue = property.GetValue(entity, null);

                if (itemValue != null && itemValue.ToString() != "" && !(property.Name == "Id" && Convert.ToInt64(itemValue) == 0))
                {
                    if (predefinedTypes.Any(i => i.Name == property.PropertyType.Name))
                    {
                        Expression = (property.PropertyType.Name.Equals("string",StringComparison.InvariantCultureIgnoreCase)) ? 
                            "{0}.Contains(@{1})" : 
                            "{0}=@{1}";

                        if (queryBuilder.Length != 0)
                            queryBuilder.Append(" and ");

                        queryBuilder.AppendFormat(Expression, property.Name, queryableFilter.Parameters.Count());
                        
                        queryableFilter.Parameters.Add(itemValue);
                    }
                    else
                    {
                        foreach (var childProperty in property.PropertyType.GetProperties())
                        {
                            object childItemValue = childProperty.GetValue(property.GetValue(entity, null), null);

                            if (childItemValue != null && childItemValue.ToString() != "" && !(childProperty.Name == "Id" && Convert.ToInt64(childItemValue) == 0))
                            {
                                Expression = (property.PropertyType.Name.Equals("string", StringComparison.InvariantCultureIgnoreCase)) ?
                                    "{0}.Contains(@{1})" :
                                    "{0}=@{1}";

                                if (queryBuilder.Length != 0)
                                    queryBuilder.Append(" and ");

                                queryBuilder.AppendFormat(Expression, string.Format("{0}.{1}", property.Name, childProperty.Name), queryableFilter.Parameters.Count());

                                queryableFilter.Parameters.Add(childItemValue);
                            }
                        }
                    }
                }
            }

            queryableFilter.Predicate = queryBuilder.ToString();
            return queryableFilter;
        }
    }
}
