﻿/* Project Name   : PushNotification
 * Module Name    : -
 * Description    : This classfile is used for push notification methods.
 * -----------------------------------------------------------------------------------------
 * DATE           | ID/ISSUE | AUTHOR           | REMARKS
 * -----------------------------------------------------------------------------------------
 * 02-Mar-2012    | 1        | Vimal Panara     | Implementing Android Push Notification 
 * -----------------------------------------------------------------------------------------
 */

using Infrastructure.Logger;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;

namespace Notification
{
    public class PushNotification
    {
        static string SERVER_API_KEY = "AAAATU0Zgxc:APA91bFRkAMnyBdoI74gpAuaDuuouf1F3waHiKLQ7pt9N71ddtfJMwa22hr5scmi_t21IwZVIIzgGiCFl_6Or4l9gox4fFeRX5WTKs_GxTLXQKbu1NBXYG8TwhYbO1pcIJ4YBd1qN3Pi";
        static string SENDER_ID = "332005999383";

        // just for test
        //string deviceId = "etSyNFP0-4E:APA91bF2Ku8D-0AdgWOjNoYQJ7_p6rb9tlx5F6exMIzO3C9uSuWD8xreIX_y5_fz9_kPWGLjqtb_C7MRGJV76S22CYazO8hYup307RQWY2HH5ELDylJ22GJiazi8H7q9V71azF321hbL";
        //string deviceId = "f0DXoGcRm6o:APA91bGOqaQZpwD61544EDZb8h9OAG3ladQu43LAkmqW-qmDXsNZzsJTq34cdvpvbDl_bdaKDDn6SzGWaSO-qC68WnWB_fkZjppCtdB2kLuf3jhiteeDh9j2mtQoGN4Gtku7Dh6gtodb";


        public static void Push(string deviceId, string code, string title, string body, string pushData = "")
        {
            if (string.IsNullOrEmpty(deviceId))
                return;

            string errorMessage = string.Empty;

            FileLogger.Log("deviceId:" + deviceId + " - code:" + code + " - title:" + title + " - body:" + body + " - pushData:" + pushData);

            try
            {
                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                tRequest.Method = "post";
                tRequest.ContentType = "application/json";

                var data = new
                {
                    to = deviceId,
                    notification = new
                    {
                        body = body,
                        title = title,
                    },
                    data = new {
                        obj = pushData,
                        code = code,
                    }
                };

                var serializer = new JavaScriptSerializer();
                var json = serializer.Serialize(data);
                Byte[] byteArray = Encoding.UTF8.GetBytes(json);

                tRequest.Headers.Add(string.Format("Authorization: key={0}", SERVER_API_KEY));
                
                tRequest.Headers.Add(string.Format("Sender: id={0}", SENDER_ID));
                tRequest.ContentLength = byteArray.Length;

                using (Stream dataStream = tRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);

                    using (WebResponse tResponse = tRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                string sResponseFromServer = tReader.ReadToEnd();
                                string str = sResponseFromServer;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
            }

        }
    }
}