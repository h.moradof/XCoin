﻿namespace Infrastructure.Notification
{
    public enum NotificationSendingCode : byte
    {
        Null = 0, // not used
        New = 1,
        WaitForCustomerAccept = 2,
        CustomerDeclined = 3,
        OnLoading = 4,
        Sent = 5,
        NoService = 6,
        CompleteDelivery = 7,  // not used
        UnCompleteDelivery = 8, // not used
        UnDelivery = 9,  // not used
        CustomerNotReceived = 10,
        Registration = 11,
        NewSubset = 12,
        PayInvoicePrice = 13,
        PayDebt = 14
    }
}
