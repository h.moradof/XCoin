﻿using DomainModels.Entities.Access;
using Infrastructure.Pagination;
using Services.Base;
using System.Collections.Generic;
using ViewModels.Access;

namespace Services.Interfaces
{
    public interface IRoleService : IGenericService<Role>
    {
        #region  custom role provider

        string[] GetAllRoles();
        bool RoleExists(string roleName);
        string[] GetUsersInRole(string roleName);

        #endregion custom role provider


        Role GetByRoleName(RoleName name);
        List<Role> GetListByRoleNames(params RoleName[] roleNames);
        void Add(RoleCreateViewModel model);
        void Edit(Role model, long[] permissionIds);
        PagedList<Role> GetAllEditableRoles(int? pageNumber = 1, int? pageSize = 15);
    }
}
