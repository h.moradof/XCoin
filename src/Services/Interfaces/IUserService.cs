﻿using DomainModels.Entities.HumanResource;
using Services.Base;
using ViewModels.Account;
using DomainModels.Entities.Access;
using Infrastructure.Pagination;
using ViewModels.HumanResource;
using System.Collections.Generic;
using ViewModels.Base;
using System;
using DomainModels.Entities.Trade;

namespace Services.Interfaces
{
    public interface IUserService : IGenericService<User>
    {


        #region custom role provider
        bool IsUserInRole(string username, string roleName);
        string[] GetRolesForUser(string username);
        #endregion

        #region admin panel
        void Ban(long id, string banReason);
        void Active(long id);
        void Deactive(long id);
        PagedList<User> GetAllByRoleNames(RoleName[] roleNames, int? pageNumber = 1, int? pageSize = 15);
        PagedList<User> SearchInUsernameOrFullName(string searchText, RoleName roleName, int? pageNumber = 1, int? pageSize = 15);
        User Add(UserCreateViewModel model);
        UserEditViewModel GetEditViewModel(long id);
        void Edit(UserEditViewModel model);
        List<BaseDdlViewModel> GetListByRoleName(RoleName roleName);
        #endregion

        #region panels (share)
        bool HaveSameUsername(string username, long userId);
        bool HaveSameUsername(string username);
        ProfileViewModel GetProfile(long id);
        void EditProfile(ProfileViewModel model);
        #endregion

        #region sign up
        void SignUp(SignUpViewModel vmSignUp, string googleAuthenticatorKey);
        #endregion

        #region sign in
        SignInResultViewModel SignIn(string username, string encyptedPassword);
        void ValidateDuplicateTemporaryPassword(long userId, string authenticateCode);
        #endregion

        #region site
        User Add(SignUpViewModel vmSignUp);
        #endregion

        #region forget password
        bool IsValidEmail(string email);
        ForgetPasswordSendEmail GetForgetPasswordDataByEmail(string email);
        #endregion

        #region mvc authorize attribute
        bool HasAccessToResource(long userId, string resourceUrl);
        #endregion

        #region set password
        bool IsValidResetPasswordCode(Guid activateCode);
        long GetUserIdByResetPasswordCode(Guid resetPasswordCode);
        void SetNewPassword(SetPasswordViewModel model);
        #endregion


        #region member panel
        VerifyViewModel GetIdentificationCardVerifyViewModel(long userId);
        VerifyViewModel GetBankCardVerifyViewModel(long userId);
        void SetBankAccountCardPhoto(long userId, string base64PhotoContent, string lang);
        void SetIdentificationCardPhoto(long userId, string base64PhotoContent, string lang);

        void LockBalance(Deal deal);
        #endregion

    }
}