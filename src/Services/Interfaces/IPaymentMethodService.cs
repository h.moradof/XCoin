﻿using DomainModels.Entities.Trade;
using Services.Base;
using System.Collections.Generic;
using ViewModels.Base;

namespace Services.Interfaces
{
    public interface IPaymentMethodService : IGenericService<PaymentMethod>
    {
        PaymentMethod FindBySlug(string slug);
        List<BaseDdlBaseOnSlugViewModel> GetBaseOnSlugDdlList(string lang);
    }
}
