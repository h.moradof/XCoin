﻿using System.Collections.Generic;
using ViewModels.Trade;

namespace Services.Interfaces
{
    public interface IUserBalanceService
    {
        List<UserBalanceDetailViewModel> GetUserBalances(long userId);
    }
}
