﻿using DomainModels.Entities.Trade;

namespace Services.Interfaces
{
    public interface IDealStatusService
    {
        DealStatus Find(long id);
    }
}
