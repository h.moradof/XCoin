﻿using DomainModels.Entities.Site;
using Services.Base;

namespace Services.Interfaces
{
    public interface ISitePageService : IGenericService<SitePage>
    {
        SitePage FindBySlug(string slug);
    }
}
