﻿using DomainModels.Entities.Trade;
using Services.Base;

namespace Services.Interfaces
{
    public interface ICurrencyService : IGenericService<Currency>, IEntityWithSlug<Currency>
    {

    }
}
