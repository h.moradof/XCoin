﻿using DomainModels.Entities.Access;
using Services.Base;

namespace Services.Interfaces
{
    public interface IRolePermissionService : IGenericService<RolePermission>
    {
        void Add(long roleId, long[] permissionIds);
        void Edit(long roleId, long[] permissionIds);
        long[] GetPermissionIdsOfRole(long roleId);
    }
}
