﻿namespace Services.Interfaces
{
    public interface IMenuService
    {
        string GetPanelMenuHtml(long userId);
    }
}
