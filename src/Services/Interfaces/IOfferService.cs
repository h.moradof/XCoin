﻿using DomainModels.Entities.Trade;
using Services.Base;

namespace Services.Interfaces
{
    public interface IOfferService : IGenericService<Offer>
    {
        Offer Add(long adsId, decimal amount, long userId);
    }
}
