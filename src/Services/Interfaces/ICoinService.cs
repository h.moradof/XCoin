﻿using DomainModels.Entities.Trade;
using Services.Base;


namespace Services.Interfaces
{
    public interface ICoinService : IGenericService<Coin>, IEntityWithSlug<Coin>
    {

    }
}
