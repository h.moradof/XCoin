﻿using System;
using DomainModels.Entities.Trade;
using Services.Base;
using ViewModels.Trade;
using Infrastructure.Pagination;

namespace Services.Interfaces
{
    public interface IDealService : IGenericService<Deal>
    {
        Deal Add(Offer offer);
        Deal GetByRowGuid(Guid rowGuid);
        DealDetailViewModel FindByRowGuid(Guid rowGuid);
        PagedList<MemberDealsListViewModel> GetMemberDeals(long userId, long statusId, int pageNumber, int pageSize);
        NotificationsViewModel GetNofitications(long userId);
        void SalerReleaseCoin(Guid dealRowGuid, long salerId);
        void BuyerPayCurrency(Guid dealRowGuid, long buyerId);
        DealStateViewModel GetDealState(Guid dealRowGuid, long userId);
        void CancelingDeal(Guid rowGuid, long userId);
        void JudgeDeal(Guid rowGuid, long userId);
        void CancelNotPaidDeals();
    }
}
