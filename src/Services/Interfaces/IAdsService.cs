﻿using DomainModels.Entities.Trade;
using Infrastructure.Pagination;
using Services.Base;
using ViewModels.Trade;

namespace Services.Interfaces
{
    public interface IAdsService : IGenericService<Ads>
    {
        PagedList<AdsSearchResultViewModel> GetLast(bool isSell, string lang, int count);
        PagedList<AdsSearchResultViewModel> Search(bool isSell, string coinSlug, string currencySlug, string countrySlug, string paymentMethodSlug, decimal? amount, string lang, string sortBy, string sortMethod = "desc", int pageNumber = 1, int pageSize = 15);
        AdsDetailViewModel GetDetailViewModel(long id, string lang);
        PagedList<UserListAdsViewModel> GetAllUserAdsByLang(string lang, long userId, bool isSell, int pageNumber = 1, int pageSize = 15);
        void Add(AdsCreateViewModel model, long userId);
        Ads Edit(AdsEditViewModel model, long userId);
        AdsEditViewModel GetEditViewModel(long id, long userId);
    }
}
