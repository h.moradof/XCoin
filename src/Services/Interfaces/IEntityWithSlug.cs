﻿using System.Collections.Generic;
using ViewModels.Base;

namespace Services.Interfaces
{
    public interface IEntityWithSlug<T>
    {
        T FindBySlug(string slug);
        List<BaseDdlBaseOnSlugViewModel> GetBaseOnSlugDdlList();
    }
}
