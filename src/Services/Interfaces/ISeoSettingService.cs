﻿using DomainModels.Entities.Setting;
using Services.Base;

namespace Services.Interfaces
{
    public interface ISeoSettingService : IGenericService<SeoSetting>
    {

    }
}
