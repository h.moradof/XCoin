﻿using DomainModels.Entities.Trade;
using Services.Base;
using System;
using System.Collections.Generic;
using ViewModels.Trade;

namespace Services.Interfaces
{
    public interface IChatService : IGenericService<Chat>
    {
        Chat Add(string message, long userId, Guid dealRoWGuid);
        List<ChatListViewModel> GetMessages(long userId, Guid dealId);
    }
}
