﻿using DomainModels.Entities.Access;
using Services.Base;
using ViewModels.Access;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Services.Interfaces
{
    public interface IPermissionService : IGenericService<Permission>
    {
        AccessViewModel GetAllAccesses();
        List<Permission> GetList(PermissionType type, bool isDefault);
    }
}
