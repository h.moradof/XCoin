﻿using Services.Base;
using DomainModels.Entities.Support;
using ViewModels.Support;
using System;

namespace Services.Interfaces
{
    public interface ITicketReplyService : IGenericService<TicketReply>
    {
        void Add(TicketReplyCreateViewModel model, long senderId);
        void Add(Guid rowGuid, string message, long senderId);
    }
}
