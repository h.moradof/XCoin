﻿using Services.Base;
using DomainModels.Entities.Support;
using Infrastructure.Pagination;
using ViewModels.Support;
using System;

namespace Services.Interfaces
{
    public interface ITicketService : IGenericService<Ticket>
    {
        PagedList<Ticket> GetTickets(bool isOpen, int? pageNumber = 1, int? pageSize = 15);
        TicketDetailViewModel GetTicketDetail(long id);
        
        PagedList<Ticket> GetAllUserTickets(long userId, bool isShowJustJudgeTickets, int pageNumber, int pageSize);
        TicketDetailViewModel GetUserTicketDetail(long userId, Guid rowGuid);
    }
}
