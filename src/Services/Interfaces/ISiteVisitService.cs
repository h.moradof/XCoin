﻿using DomainModels.Entities.Site;
using Services.Base;
using ViewModels.Site;
using System.Collections.Generic;

namespace Services.Interfaces
{
    public interface ISiteVisitService : IGenericService<SiteVisit>
    {
        void SaveNewVisit(string ip);

        List<SiteVisitMonthlyViewModel> GetMonthlyVisits();

        SiteVisitReportViewModel GetSiteVisitReport();
    }
}
