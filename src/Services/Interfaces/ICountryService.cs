﻿using DomainModels.Entities.Trade;
using Services.Base;

namespace Services.Interfaces
{
    public interface ICountryService : IGenericService<Country>, IEntityWithSlug<Country>
    {

    }
}
