﻿using DomainModels.Entities.Trade;
using Services.Base;
using Services.Interfaces;
using DatabaseContext.Context;
using System;
using System.Linq;
using Infrastructure.Exceptions;

namespace Services.EfServices
{
    public class OfferService : GenericService<Offer>, IOfferService
    {

        #region ctor
        public OfferService(IUnitOfWork uow) 
            : base(uow)
        {
        }
        #endregion


        public Offer Add(long adsId, decimal amount, long userId)
        {
            var ads = _uow.Set<Ads>().FirstOrDefault(c => c.Id == adsId);

            if (ads == null) { throw new ArgumentException("adsId is invalid"); }

            if (ads.UserId == userId) { throw new CustomException("You can't offer on your own ads"); }

            return base.Add(new Offer
            {
                AdsId = adsId,
                Amount = amount,
                UserId = userId,
                CreatedOn = DateTime.Now
            });
        }


    }
}
