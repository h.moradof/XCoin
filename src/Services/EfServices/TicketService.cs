﻿using Services.Base;
using Services.Interfaces;
using DatabaseContext.Context;
using System.Linq;
using Infrastructure.Pagination;
using DomainModels.Entities.Support;
using System;
using ViewModels.Support;
using DomainModels.Entities.Trade;
using DomainModels.Entities.HumanResource;

namespace Services.EfServices
{
    public class TicketService : GenericService<Ticket>, ITicketService
    {

        #region ctor
        public TicketService(
            IUnitOfWork uow) 
            : base(uow)
        {  }
        #endregion


        public PagedList<Ticket> GetTickets(bool isOpen, int? pageNumber = 1, int? pageSize = 15)
        {
            return _dbSet
                .Where(c => c.IsOpen == isOpen)
                .OrderByDescending(c => c.Id)
                .ToPagedList(pageNumber.Value, pageSize.Value);
        }

        public TicketDetailViewModel GetTicketDetail(long id)
        {
            var vm = new TicketDetailViewModel();

            var t_s = (from t in _dbSet
                       join u in _uow.Set<User>() on t.SenderId equals u.Id
                       where t.Id == id
                       select new { Ticket = t, Sender = u })
                       .First();

            vm.Ticket = t_s.Ticket;
            vm.SenderFullName = t_s.Sender.FullName;

            vm.TicketReplies = (from tr in _uow.Set<TicketReply>()
                                join u in _uow.Set<User>() on tr.SenderId equals u.Id
                                where tr.TicketId == id
                                select new TicketReplyListViewModel { TicketReply = tr, SenderFullName = u.FullName })
                                .ToList();
            return vm;
        }

        public PagedList<Ticket> GetAllUserTickets(long userId, bool isShowJustJudgeTickets, int pageNumber, int pageSize)
        {
            if (!isShowJustJudgeTickets)
            {
                // support tickets
                return (from t in _dbSet
                        join d in _uow.Set<Deal>() on t.RowGuid equals d.JudgeTicketRowGuid into g
                        from d in g.DefaultIfEmpty()
                        where t.SenderId == userId && d == null
                        orderby t.Id descending
                        select t)
                        .ToPagedList(pageNumber, pageSize);
            }

            // judge
            return (from t in _dbSet
                    join d in _uow.Set<Deal>() on t.RowGuid equals d.JudgeTicketRowGuid
                    where (d.SalerId == userId || d.BuyerId == userId)
                    orderby t.Id descending
                    select t)
                    .ToPagedList(pageNumber, pageSize);
        }

        public TicketDetailViewModel GetUserTicketDetail(long userId, Guid rowGuid)
        {
            var vm = new TicketDetailViewModel();

            var relatedDeal = _uow.Set<Deal>().FirstOrDefault(c => c.JudgeTicketRowGuid == rowGuid);
            long? anotherDealSideUserId = null;

            if (relatedDeal != null)
            {
                if (relatedDeal.BuyerId == userId)
                {
                    anotherDealSideUserId = relatedDeal.SalerId;
                }
                else
                {
                    anotherDealSideUserId = relatedDeal.BuyerId;
                }
            }


            var t_s = (from t in _dbSet
                       join u in _uow.Set<User>() on t.SenderId equals u.Id
                       where t.RowGuid == rowGuid && (t.SenderId == userId || t.SenderId == anotherDealSideUserId || anotherDealSideUserId == null)
                       select new { Ticket = t, Sender = u })
                       .First();

            vm.Ticket = t_s.Ticket;
            vm.SenderFullName = t_s.Sender.FullName;

            vm.TicketReplies = (from tr in _uow.Set<TicketReply>()
                                join u in _uow.Set<User>() on tr.SenderId equals u.Id
                                where tr.TicketId == t_s.Ticket.Id
                                select new TicketReplyListViewModel { TicketReply = tr, SenderFullName = u.FullName })
                                .ToList();

            return vm;
        }
    }
}
