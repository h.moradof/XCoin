﻿using DomainModels.Entities.Trade;
using Services.Base;
using Services.Interfaces;
using System.Collections.Generic;
using System.Linq;
using DatabaseContext.Context;
using ViewModels.Base;

namespace Services.EfServices
{
    public class CoinService : GenericService<Coin>, ICoinService
    {
        #region ctor
        public CoinService(IUnitOfWork uow) 
            : base(uow)
        { }
        #endregion


        public Coin FindBySlug(string slug)
        {
            if (string.IsNullOrEmpty(slug)) { return null; }

            return _dbSet.FirstOrDefault(c => c.Slug == slug);
        }

        public List<BaseDdlBaseOnSlugViewModel> GetBaseOnSlugDdlList()
        {
            return _dbSet
                .Select(c => new BaseDdlBaseOnSlugViewModel
                {
                    Slug = c.Slug,
                    Name = c.DisplayName
                }).ToList();
        }
    }
}
