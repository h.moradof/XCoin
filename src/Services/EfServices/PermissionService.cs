﻿using DomainModels.Entities.Access;
using Services.Base;
using Services.Interfaces;
using DatabaseContext.Context;
using ViewModels.Access;
using System.Linq;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Services.EfServices
{
    public class PermissionService : GenericService<Permission>, IPermissionService
    {

        #region props

        private readonly IRoleService _roleService;
        private readonly IRolePermissionService _rolePermissionService;

        #endregion

        #region ctor

        public PermissionService(IUnitOfWork uow, IRoleService roleService, IRolePermissionService rolePermissionService)
            : base(uow)
        {
            _roleService = roleService;
            _rolePermissionService = rolePermissionService;
        }

        #endregion

        public List<Permission> GetList(PermissionType type, bool isDefault)
        {
            var permissions = _dbSet
                .Where(c => c.IsDefault == isDefault && c.PermissionType == type)
                .OrderBy(c => c.RowOrder)
                .ToList();

            return permissions;
        }


        public AccessViewModel GetAllAccesses()
        {
            var vmAccess = new AccessViewModel();

            vmAccess.Permissions = _dbSet.ToList();
            vmAccess.RolePermissions = _rolePermissionService.GetList();
            vmAccess.Roles = _roleService.GetList();

            return vmAccess;
        }
        
    }
}
