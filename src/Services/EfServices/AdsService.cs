﻿using DomainModels.Entities.Trade;
using Services.Base;
using Services.Interfaces;
using DatabaseContext.Context;
using Infrastructure.Pagination;
using ViewModels.Trade;
using System.Linq;
using DomainModels.Entities.HumanResource;
using DomainModels.Entities.Access;
using System;
using Infrastructure.Exceptions;

namespace Services.EfServices
{
    public class AdsService : GenericService<Ads>, IAdsService
    {

        #region ctor
        public AdsService(IUnitOfWork uow)
            : base(uow)
        { }
        #endregion

        public PagedList<AdsSearchResultViewModel> Search(bool isSell, string coinSlug, string currencySlug, string countrySlug, string paymentMethodSlug, decimal? amount, string lang, string sortBy, string sortMethod = "desc", int pageNumber = 1, int pageSize = 15)
        {
            var query = (from a in _dbSet
                         join co in _uow.Set<Coin>() on a.CoinId equals co.Id
                         join cu in _uow.Set<Currency>() on a.CurrencyId equals cu.Id
                         join cou in _uow.Set<Country>() on a.CountryId equals cou.Id
                         join p in _uow.Set<PaymentMethod>() on a.PaymentMethodId equals p.Id
                         join u in _uow.Set<User>() on a.UserId equals u.Id
                         join r in _uow.Set<Role>() on u.RoleId equals r.Id
                         where
                                a.IsActive &&
                                a.IsSell == isSell &&
                                (co.Slug == coinSlug || coinSlug == null || coinSlug == "")
                                &&
                                (cu.Slug == currencySlug || currencySlug == null || currencySlug == "")
                                &&
                                (cou.Slug == countrySlug || countrySlug == null || countrySlug == "")
                                &&
                                (p.Slug == paymentMethodSlug || paymentMethodSlug == null || paymentMethodSlug == "")
                                &&
                                ((amount >= a.MinimumCurrencyPrice && amount <= a.MaximumCurrencyPrice) || amount == null)
                         select new AdsSearchResultViewModel
                         {
                             AdsId = a.Id,
                             IsSell = a.IsSell,
                             UserId = u.Id,
                             Username = u.Username,
                             UserRating = u.Rating,
                             CoinName = co.DisplayName,
                             CountryName = cou.Name,
                             CurrencyName = cu.Name,
                             CurrencyUnitPriceOfCoin = a.CurrencyUnitPriceOfCoin,
                             MinimumCurrencyPrice = a.MinimumCurrencyPrice,
                             MaximumCurrencyPrice = a.MaximumCurrencyPrice,
                             PaymentMethodName = (lang == "en") ? p.EnName : p.ZhName,
                             IsUserVerified = (r.Name == RoleName.VerifiedUsers)
                         });

            query = AddSortByClause(sortBy, sortMethod, query);

            return query.ToPagedList(pageNumber, pageSize);
        }

        private static IQueryable<AdsSearchResultViewModel> AddSortByClause(string sortBy, string sortMethod, IQueryable<AdsSearchResultViewModel> query)
        {
            const string DefaultSortMethod = "desc";
            const string DefaultSortBy = "currencyunitpriceofcoin";

            string lowerSortBy = (sortBy != null) ? sortBy.ToLower() : DefaultSortBy;

            string lowerSortMethod = (sortMethod != null) ? sortMethod.ToLower() : DefaultSortMethod;
            if ((lowerSortMethod != "desc" && lowerSortMethod != "asc")) // is user change parameter, correct it
            {
                lowerSortMethod = DefaultSortMethod;
            }

            if (lowerSortBy == "username")
            {
                if (lowerSortMethod == "desc")
                    query = query.OrderByDescending(c => c.Username);
                else
                    query = query.OrderBy(c => c.Username);
            }
            else if (lowerSortBy == "userrating")
            {
                if (lowerSortMethod == "desc")
                    query = query.OrderByDescending(c => c.UserRating);
                else
                    query = query.OrderBy(c => c.UserRating);
            }
            else if (lowerSortBy == "coinname")
            {
                if (lowerSortMethod == "desc")
                    query = query.OrderByDescending(c => c.CoinName);
                else
                    query = query.OrderBy(c => c.CoinName);
            }
            else if (lowerSortBy == "countryname")
            {
                if (lowerSortMethod == "desc")
                    query = query.OrderByDescending(c => c.CountryName);
                else
                    query = query.OrderBy(c => c.CountryName);
            }
            else if (lowerSortBy == "currencyname")
            {
                if (lowerSortMethod == "desc")
                    query = query.OrderByDescending(c => c.CurrencyName);
                else
                    query = query.OrderBy(c => c.CurrencyName);
            }
            else if (lowerSortBy == "currencyunitpriceofcoin")
            {
                if (lowerSortMethod == "desc")
                    query = query.OrderByDescending(c => c.CurrencyUnitPriceOfCoin);
                else
                    query = query.OrderBy(c => c.CurrencyUnitPriceOfCoin);
            }
            else if (lowerSortBy == "paymentmethodname")
            {
                if (lowerSortMethod == "desc")
                    query = query.OrderByDescending(c => c.PaymentMethodName);
                else
                    query = query.OrderBy(c => c.PaymentMethodName);
            }
            else if (lowerSortBy == "isuserverified")
            {
                if (lowerSortMethod == "desc")
                    query = query.OrderByDescending(c => c.IsUserVerified);
                else
                    query = query.OrderBy(c => c.IsUserVerified);
            }

            return query;
        }

        public PagedList<AdsSearchResultViewModel> GetLast(bool isSell, string lang, int count)
        {
            return Search(isSell, null, null, null, null, null, lang, null, null, 1, count);
        }

        public AdsDetailViewModel GetDetailViewModel(long id, string lang)
        {
            var query = (from a in _dbSet
                         join co in _uow.Set<Coin>() on a.CoinId equals co.Id
                         join cu in _uow.Set<Currency>() on a.CurrencyId equals cu.Id
                         join cou in _uow.Set<Country>() on a.CountryId equals cou.Id
                         join p in _uow.Set<PaymentMethod>() on a.PaymentMethodId equals p.Id
                         join u in _uow.Set<User>() on a.UserId equals u.Id
                         join r in _uow.Set<Role>() on u.RoleId equals r.Id
                         where
                                a.IsActive &&
                                a.Id == id
                         select new AdsDetailViewModel
                         {
                             AdsId = a.Id,
                             IsSell = a.IsSell,
                             UserId = u.Id,
                             Username = u.Username,
                             UserRating = u.Rating,
                             CoinName = co.DisplayName,
                             CountryName = cou.Name,
                             CurrencyName = cu.Name,
                             CurrencyUnitPriceOfCoin = a.CurrencyUnitPriceOfCoin,
                             MinimumCurrencyPrice = a.MinimumCurrencyPrice,
                             MaximumCurrencyPrice = a.MaximumCurrencyPrice,
                             PaymentMethodName = (lang == "en") ? p.EnName : p.ZhName,
                             IsUserVerified = (r.Name == RoleName.VerifiedUsers),
                             Description = a.Description
                         });

            return query.FirstOrDefault();
        }

        public PagedList<UserListAdsViewModel> GetAllUserAdsByLang(string lang, long userId, bool isSell, int pageNumber = 1, int pageSize = 15)
        {
            var query = (from a in _dbSet
                         join co in _uow.Set<Coin>() on a.CoinId equals co.Id
                         join cu in _uow.Set<Currency>() on a.CurrencyId equals cu.Id
                         join cou in _uow.Set<Country>() on a.CountryId equals cou.Id
                         join p in _uow.Set<PaymentMethod>() on a.PaymentMethodId equals p.Id
                         join u in _uow.Set<User>() on a.UserId equals u.Id
                         join r in _uow.Set<Role>() on u.RoleId equals r.Id
                         where
                                a.UserId == userId &&
                                a.IsSell == isSell
                         orderby a.Id descending, a.IsActive descending
                         select new UserListAdsViewModel
                         {
                             AdsId = a.Id,
                             IsSell = a.IsSell,
                             IsActive = a.IsActive,
                             CoinName = co.DisplayName,
                             CountryName = cou.Name,
                             CurrencyName = cu.Name,
                             CurrencyUnitPriceOfCoin = a.CurrencyUnitPriceOfCoin,
                             MinimumCurrencyPrice = a.MinimumCurrencyPrice,
                             MaximumCurrencyPrice = a.MaximumCurrencyPrice,
                             PaymentMethodName = (lang == "en") ? p.EnName : p.ZhName
                         });

            return query.ToPagedList(pageNumber, pageSize);
        }

        public void Add(AdsCreateViewModel model, long userId)
        {
            // prevent to create duplicate adses with same currency and same country
            var hasDuplicateAds = _dbSet.Any(c => c.UserId == userId && c.IsSell == model.IsSell && c.CountryId == model.CountryId && c.CurrencyId == model.CurrencyId);
            if (hasDuplicateAds)
            {
                throw new CustomException("Duplicate ads!. You have another ads with same country and same currency");
            }

            ValidateMinAndMaxAmount(model.MinimumCurrencyPrice, model.MaximumCurrencyPrice);

            base.Add(new Ads
            {
                CoinId = model.CoinId,
                CountryId = model.CountryId,
                CurrencyId = model.CurrencyId,
                PaymentMethodId = model.PaymentMethodId,
                Description = model.Description,
                CurrencyUnitPriceOfCoin = model.CurrencyUnitPriceOfCoin,
                MinimumCurrencyPrice = model.MinimumCurrencyPrice,
                MaximumCurrencyPrice = model.MaximumCurrencyPrice,
                IsSell = model.IsSell,
                IsActive = model.IsActive,
                UserId = userId,
                CreatedOn = DateTime.Now
            });
        }

        public Ads Edit(AdsEditViewModel model, long userId)
        {
            // prevent to create duplicate adses with same currency and same country
            var hasDuplicateAds = _dbSet.Any(c => c.Id != model.Id && c.UserId == userId && c.CountryId == model.CountryId && c.CurrencyId == model.CurrencyId);
            if (hasDuplicateAds)
            {
                throw new CustomException("Duplicate ads!. You have another ads with same country and same currency");
            }

            ValidateMinAndMaxAmount(model.MinimumCurrencyPrice, model.MaximumCurrencyPrice);

            var updatingItem = Find(model.Id);
            if (updatingItem.UserId != userId)
            {
                throw new UnauthorizedAccessException("Access denied");
            }

            updatingItem.CoinId = model.CoinId;
            updatingItem.CountryId = model.CountryId;
            updatingItem.CurrencyId = model.CurrencyId;
            updatingItem.PaymentMethodId = model.PaymentMethodId;
            updatingItem.Description = model.Description;
            updatingItem.CurrencyUnitPriceOfCoin = model.CurrencyUnitPriceOfCoin;
            updatingItem.MinimumCurrencyPrice = model.MinimumCurrencyPrice;
            updatingItem.MaximumCurrencyPrice = model.MaximumCurrencyPrice;
            updatingItem.IsActive = model.IsActive;

            return base.Edit(updatingItem);
        }

        private static void ValidateMinAndMaxAmount(decimal minPrice, decimal maxPrice)
        {
            if (minPrice >= maxPrice)
            {
                throw new CustomException("Minimum currency price must be less than Maximum currency price");
            }
        }

        public AdsEditViewModel GetEditViewModel(long id, long userId)
        {
            var model = _dbSet.FirstOrDefault(c => c.Id == id && c.UserId == userId);
            if (model == null)
            {
                return null;
            }

            return new AdsEditViewModel
            {
                Id = model.Id,
                CoinId = model.CoinId,
                CountryId = model.CountryId,
                CurrencyId = model.CurrencyId,
                PaymentMethodId = model.PaymentMethodId,
                Description = model.Description,
                CurrencyUnitPriceOfCoin = model.CurrencyUnitPriceOfCoin,
                MinimumCurrencyPrice = model.MinimumCurrencyPrice,
                MaximumCurrencyPrice = model.MaximumCurrencyPrice,
                IsActive = model.IsActive
            };
        }
    }
}
