﻿using Services.Interfaces;
using System.Linq;
using DomainModels.Entities.Trade;
using DatabaseContext.Context;

namespace Services.EfServices
{
    public class DealStatusService : IDealStatusService
    {
        #region ctor
        private readonly IUnitOfWork _uow;

        public DealStatusService(IUnitOfWork uow)
        {
            _uow = uow;
        }
        #endregion


        public DealStatus Find(long id)
        {
            return _uow.Set<DealStatus>().FirstOrDefault(c => c.Id == id);
        }
    }
}
