﻿using DatabaseContext.Context;
using DomainModels.Entities.HumanResource;
using Infrastructure.Exceptions;
using Infrastructure.IQueryableExtensions;
using Services.Base;
using Services.Interfaces;
using System.Linq;
using System.Threading.Tasks;
using ViewModels.Account;
using Infrastructure.Common;
using Infrastructure.Security;
using DomainModels.Entities.Access;
using System;
using Infrastructure.Sms.Interfaces;
using Infrastructure.Mail.Interfaces;
using Infrastructure.Pagination;
using ViewModels.HumanResource;
using System.Collections.Generic;
using ViewModels.Base;
using DomainModels.Entities.Base;
using DomainModels.Entities.Trade;

namespace Services.EfServices
{
    public class UserService : GenericService<User>, IUserService
    {

        #region props

        private readonly IUserAccountMailSender _userAcountMailSender;
        private readonly IRoleService _roleService;

        #endregion

        #region ctor

        public UserService(
            IUnitOfWork uow,
            IUserAccountMailSender userAcountMailSender,
            IRoleService roleService)
            : base(uow)
        {
            _userAcountMailSender = userAcountMailSender;
            _roleService = roleService;
        }

        #endregion


        #region override

        public override User Add(User model)
        {
            var haveSameUser = _dbSet.Any(c => c.Username == model.Username);

            if (haveSameUser)
                throw new CustomException("Duplicate username.");

            return base.Add(model);
        }

        public override Task<User> EditAsync(User model)
        {
            var haveSameUser = _dbSet.Any(c => c.Id != model.Id && c.Username == model.Username);

            if (haveSameUser)
                throw new CustomException("Duplicate username");

            return base.EditAsync(model);

            // TODO: send email to user
        }

        #endregion


        #region custom role provider

        public bool IsUserInRole(string username, string roleName)
        {
            var query = (from u in _dbSet
                         join r in _uow.Set<Role>() on u.RoleId equals r.Id
                         where
                                u.Username == username &&
                                u.AccountStatus == AccountStatus.Active &&
                                r.IdentityName == roleName
                         select u.Id);

            return query.Any();
        }

        public string[] GetRolesForUser(string username)
        {
            return (from u in _dbSet
                    join r in _uow.Set<Role>() on u.RoleId equals r.Id
                    where
                            u.Username == username &&
                            u.AccountStatus == AccountStatus.Active
                    select r.IdentityName).ToList().ToArray();
        }

        #endregion


        #region admin panel
        public void Ban(long id, string banReason)
        {
            var user = Find(id);
            user.AccountStatus = AccountStatus.Baned;
            user.BanReason = banReason;
        }

        public void Active(long id)
        {
            var user = Find(id);
            user.AccountStatus = AccountStatus.Active;
        }

        public void Deactive(long id)
        {
            var user = Find(id);
            user.AccountStatus = AccountStatus.Deactive;
        }

        public PagedList<User> GetAllByRoleNames(RoleName[] roleNames, int? pageNumber = 1, int? pageSize = 15)
        {
            return _dbSet
                .Where(c => roleNames.Contains(c.Role.Name))
                .OrderByDescending(c => c.Id)
                .ToPagedList(pageNumber.Value, pageSize.Value);
        }

        public PagedList<User> SearchInUsernameOrFullName(string searchText, RoleName roleName, int? pageNumber = 1, int? pageSize = 15)
        {
            return _dbSet
                .Where(c => c.Role.Name == roleName & (c.FullName.Contains(searchText) || c.Username.Contains(searchText)))
                .OrderByDescending(c => c.Id)
                .ToPagedList(pageNumber.Value, pageSize.Value);
        }

        public User Add(UserCreateViewModel model)
        {
            var haveSameUser = HaveSameUsername(model.Username);
            if (haveSameUser)
            {
                throw new CustomException("Duplicate user");
            }

            var newUser = new User
            {
                AccountStatus = AccountStatus.Active,
                EmailAddress = model.EmailAddress,
                FirstName = model.FirstName,
                LastName = model.LastName,
                Username = model.Username,
                Password = DataEncryptor.MultiEncrypt(model.Password),
                RoleId = model.RoleId
            };

            return base.Add(newUser);
        }

        public UserEditViewModel GetEditViewModel(long id)
        {
            var user = Find(id);

            if (user == null)
            {
                return null;
            }

            return new UserEditViewModel
            {
                FirstName = user.FirstName,
                LastName = user.LastName,
                BanReason = user.BanReason,
                RoleId = user.RoleId,
                Username = user.Username
            };
        }

        public void Edit(UserEditViewModel model)
        {
            var haveSameUser = HaveSameUsername(model.Username, model.Id);
            if (haveSameUser)
            {
                throw new CustomException("Duplicate username");
            }

            var updatingItem = Find(model.Id);

            updatingItem.EmailAddress = model.EmailAddress;
            updatingItem.FirstName = model.FirstName;
            updatingItem.LastName = model.LastName;
            updatingItem.Username = model.Username;
            updatingItem.RoleId = model.RoleId;
            updatingItem.AccountStatus = model.AccountStatus;

            if (model.AccountStatus == AccountStatus.Baned)
                updatingItem.BanReason = model.BanReason;

            if (model.WantChangePassword)
                updatingItem.Password = DataEncryptor.MultiEncrypt(model.Password);
        }

        public List<BaseDdlViewModel> GetListByRoleName(RoleName roleName)
        {
            return _dbSet
                .Join(_uow.Set<Role>(), u => u.RoleId, r => r.Id, (u, r) => new { u, r })
                .Where(x => x.r.Name == roleName)
                .OrderByDescending(c => c.u.Id)
                .Select(c => new BaseDdlViewModel { Name = c.u.FullName + " (" + c.u.Id + ")", Id = c.u.Id })
                .ToList();
        }

        #endregion


        #region panels

        public bool HaveSameUsername(string username, long userId)
        {
            return _dbSet.Any(c => c.Username == username && c.Id != userId);
        }

        public bool HaveSameUsername(string username)
        {
            return _dbSet.Any(c => c.Username == username);
        }

        public ProfileViewModel GetProfile(long id)
        {
            var vm = _dbSet
                .Select(c => new ProfileViewModel
                {
                    EmailAddress = c.EmailAddress,
                    Fullname = c.FullName,
                    Username = c.Username,
                    UserId = c.Id
                })
                .FirstOrDefault(c => c.UserId == id);

            return vm;
        }

        public void EditProfile(ProfileViewModel model)
        {
            var user = Find(model.UserId);

            // We do'nt update username or email address
            if (model.WantUpdatePassword)
            {
                if (model.Password != model.RePassword) { throw new CustomException("Password and ConfirmPassword are not the same"); }

                user.Password = DataEncryptor.MultiEncrypt(model.Password);
                _userAcountMailSender.SendEditUserEmail("Edit profile", user.EmailAddress, "en");
            }
        }

        #endregion


        #region sign up
        public void SignUp(SignUpViewModel vmSignUp, string googleAuthenticatorKey)
        {
            ValidatePasswordAndConfirmPassword(vmSignUp.Password, vmSignUp.RePassword);
            ValidateDuplicateEmailAddress(vmSignUp.EmailAddress);
            ValidateDuplicateUsername(vmSignUp.Username);

            var registeredUserRole = _roleService.GetByRoleName(RoleName.RegisteredUsers);

            var unknownStatus = GetAcceptStatusByName(AcceptStatusName.Unknown);

            var newUser = new User
            {
                FirstName = vmSignUp.FirstName,
                LastName = vmSignUp.LastName,
                EmailAddress = vmSignUp.EmailAddress.ToLowerInvariant(),
                Username = vmSignUp.Username.ToLowerInvariant(),
                Password = DataEncryptor.MultiEncrypt(vmSignUp.Password),
                GoogleAuthenticatorKey = googleAuthenticatorKey,
                Rating = 0,
                BankCardAcceptStatusId = unknownStatus.Id,
                IdentificationCardAcceptStatusId = unknownStatus.Id,
                RoleId = registeredUserRole.Id,
                AccountStatus = AccountStatus.Active,
                RowGuid = Guid.NewGuid(),
                ResetPasswordCode = Guid.NewGuid(),
                CreatedOn = DateTime.Now
            };

            Add(newUser);
        }

        private void ValidatePasswordAndConfirmPassword(string password, string confirmPassword)
        {
            if (password != confirmPassword)
            {
                throw new CustomException("Password and confirm password are not the same");
            }
        }

        private void ValidateDuplicateUsername(string username)
        {
            var lowerUsername = username.ToLowerInvariant();
            var hasSameUsername = _dbSet.Any(c => c.Username.ToLower() == lowerUsername);
            if (hasSameUsername)
            {
                throw new CustomException("Duplicate username");
            }
        }

        private void ValidateDuplicateEmailAddress(string emailAddress)
        {
            var lowerEmailAddress = emailAddress.ToLowerInvariant();
            var hasSameEmailAddress = _dbSet.Any(c => c.EmailAddress.ToLower() == lowerEmailAddress);
            if (hasSameEmailAddress)
            {
                throw new CustomException("Duplicate email address");
            }
        }
        #endregion


        #region signin

        public SignInResultViewModel SignIn(string username, string password)
        {
            var hashedPassword = DataEncryptor.MultiEncrypt(password);

            var vm = (from u in _dbSet
                      join r in _uow.Set<Role>() on u.RoleId equals r.Id
                      where
                            u.Username == username && u.Password == hashedPassword
                      select new SignInResultViewModel { User = u, Role = r })
                      .FirstOrDefault();

            if (vm == null) { throw new CustomException("Invalid username or password"); }

            ValidateUserOnSignIn(vm.User);

            return vm;
        }

        private void ValidateUserOnSignIn(User user)
        {
            if (user == null) { throw new CustomException("Invalid username or password"); }
            if (user.AccountStatus == AccountStatus.Baned) { throw new CustomException("Your account is baned. " + user.BanReason); }
            if (user.AccountStatus == AccountStatus.Deactive) { throw new CustomException("Your account is not active"); }
        }


        public void ValidateDuplicateTemporaryPassword(long userId, string temporaryPassword)
        {
            var isAuthenticateCodeUsedBefore = _uow.Set<GoogleAuthenticatorLog>().Any(c => c.UserId == userId && c.TemporaryPassword == temporaryPassword);

            if (isAuthenticateCodeUsedBefore) { throw new CustomException("Duplicate temporary password."); }

            _uow.Set<GoogleAuthenticatorLog>().Add(new GoogleAuthenticatorLog { UserId = userId, TemporaryPassword = temporaryPassword, CreatedOn = DateTime.Now });
        }

        #endregion


        #region forget password 

        public bool IsValidEmail(string email)
        {
            return _dbSet.Any(c => c.EmailAddress == email);
        }

        public ForgetPasswordSendEmail GetForgetPasswordDataByEmail(string email)
        {
            var user = _dbSet.FirstOrDefault(c => c.EmailAddress == email);

            if (user == null)
                throw new CustomException("Duplicate Email address");

            return new ForgetPasswordSendEmail { FullName = user.FullName, Username = user.Username };

        }

        #endregion


        #region mvc authorize attribute

        public bool HasAccessToResource(long userId, string resourceUrl)
        {
            var hasAccess = _dbSet
                .Any(c => c.Id == userId && c.Role.RolePermissions.Any(x => x.Permission.Path == resourceUrl));

            return hasAccess;
        }

        #endregion


        #region set password

        public bool IsValidResetPasswordCode(Guid resetPasswordCode)
        {
            return _dbSet.Any(c => c.ResetPasswordCode == resetPasswordCode);
        }

        public long GetUserIdByResetPasswordCode(Guid resetPasswordCode)
        {
            return _dbSet.First(c => c.ResetPasswordCode == resetPasswordCode).Id;
        }

        public void SetNewPassword(SetPasswordViewModel model)
        {
            var user = Find(model.UserId);
            user.Password = model.Password;

            // send email to user
            _userAcountMailSender.SendEditUserEmail("Edit Profile", user.EmailAddress, "en");
        }

        #endregion



        #region site
        /// <summary>
        /// عضویت در سایت
        /// این متد موقتی است و بعدا حذف میشه
        /// </summary>
        public User Add(SignUpViewModel vmSignUp)
        {
            var haveSameUser = HaveSameUsername(vmSignUp.Username);
            if (haveSameUser)
            {
                throw new CustomException("نام کاربری انتخابی شما در سایت موجود است، لطفا نام کاربری دیگری برگزینید");
            }

            var customerRole = _uow.Set<Role>().First(c => c.Name == RoleName.RegisteredUsers);

            var newUser = new User
            {
                FirstName = vmSignUp.FirstName,
                LastName = vmSignUp.LastName,
                Username = vmSignUp.Username,
                Password = DataEncryptor.MultiEncrypt(vmSignUp.Password),
                CreatedOn = DateTime.Now,
                EmailAddress = vmSignUp.EmailAddress,
                AccountStatus = AccountStatus.Active,
                RowGuid = Guid.NewGuid(),
                RoleId = customerRole.Id,
            };

            base.Add(newUser);

            return newUser;
        }

        #endregion



        #region member panel

        public void SetBankAccountCardPhoto(long userId, string base64PhotoContent, string lang)
        {
            var user = Find(userId);
            if (!CanSetBankCardPhoto(user))
            {
                throw new CustomException("Verification of your bank card photo is in progress. please wait fo result");
            }

            user.BankCardPhoto = base64PhotoContent;
            var pending = GetAcceptStatusByName(AcceptStatusName.Pending);
            user.BankCardAcceptStatusId = pending.Id;
        }

        public void SetIdentificationCardPhoto(long userId, string base64PhotoContent, string lang)
        {
            var user = Find(userId);
            if (!CanSetIdentificationCardPhoto(user))
            {
                throw new CustomException("Verification of your identification card photo is in progress. please wait fo result");
            }

            user.IdentificationCardPhoto = base64PhotoContent;
            var pending = GetAcceptStatusByName(AcceptStatusName.Pending);
            user.IdentificationCardAcceptStatusId = pending.Id;
        }

        private AcceptStatus GetAcceptStatusByName(AcceptStatusName name)
        {
            return _uow.Set<AcceptStatus>().First(c => c.Name == name);
        }

        private bool CanSetIdentificationCardPhoto(User user)
        {
            var unknown = GetAcceptStatusByName(AcceptStatusName.Unknown);
            var declined = GetAcceptStatusByName(AcceptStatusName.Declined);

            return (user.IdentificationCardAcceptStatusId == unknown.Id || user.IdentificationCardAcceptStatusId == declined.Id);
        }

        private bool CanSetBankCardPhoto(User user)
        {
            var unknown = GetAcceptStatusByName(AcceptStatusName.Unknown);
            var declined = GetAcceptStatusByName(AcceptStatusName.Declined);

            return (user.BankCardAcceptStatusId == unknown.Id || user.BankCardAcceptStatusId == declined.Id);
        }

        public VerifyViewModel GetIdentificationCardVerifyViewModel(long userId)
        {
            return (from u in _dbSet
                    join s in _uow.Set<AcceptStatus>() on u.IdentificationCardAcceptStatusId equals s.Id
                    where u.Id == userId
                    select new VerifyViewModel
                    {
                        AcceptStatus = s,
                        PhotoContent = u.IdentificationCardPhoto
                    }).First();
        }

        public VerifyViewModel GetBankCardVerifyViewModel(long userId)
        {
            return (from u in _dbSet
                    join s in _uow.Set<AcceptStatus>() on u.BankCardAcceptStatusId equals s.Id
                    where u.Id == userId
                    select new VerifyViewModel
                    {
                        AcceptStatus = s,
                        PhotoContent = u.BankCardPhoto
                    }).First();
        }

        public void LockBalance(Deal deal)
        {
            var salerBalance = (from u in _dbSet
                                join b in _uow.Set<UserBalance>() on u.Id equals b.UserId
                                where u.Id == deal.SalerId && b.CoinId == deal.CoinId
                                select b).FirstOrDefault();

            if (salerBalance == null)
            {
                throw new CustomException("Saler doesn't have any balance(coin) in his account");
            }

            var hasSalerEnoughBalance = (salerBalance.Amount >= deal.TotalCurrencyPrice);
            if (!hasSalerEnoughBalance)
            {
                throw new CustomException("Saler doesn't have enough balance(coin) in his account");
            }

            salerBalance.Amount -= deal.TotalCurrencyPrice; // lock
        }

        #endregion

    }
}
