﻿using Services.Base;
using Services.Interfaces;
using DatabaseContext.Context;
using DomainModels.Entities.Support;
using ViewModels.Support;
using System;
using System.Linq;

namespace Services.EfServices
{
    public class TicketReplyService : GenericService<TicketReply>, ITicketReplyService
    {

        #region ctor
        public TicketReplyService(
            IUnitOfWork uow) 
            : base(uow)
        {  }
        #endregion

        // admin
        public void Add(TicketReplyCreateViewModel model, long senderId)
        {
            var newReply = new TicketReply
            {
                Message = model.Message,
                SenderId = senderId,
                TicketId = model.TicketId
            };

            _uow.Set<TicketReply>().Add(newReply);

            var ticket = _uow.Set<Ticket>().Find(model.TicketId);
            ticket.IsOpen = model.IsOpen;
        }

        // member
        public void Add(Guid rowGuid , string message, long senderId)
        {
            var ticket = _uow.Set<Ticket>().First(c => c.RowGuid == rowGuid);
            ticket.IsOpen = true;

            _uow.Set<TicketReply>().Add(new TicketReply { TicketId = ticket.Id, Message = message, SenderId = senderId, CreatedOn = DateTime.Now });
        }
    }
}
