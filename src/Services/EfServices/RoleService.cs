﻿using DomainModels.Entities.Access;
using Services.Base;
using Services.Interfaces;
using DatabaseContext.Context;
using System.Linq;
using System.Collections.Generic;
using ViewModels.Access;
using Infrastructure.Pagination;
using DomainModels.Entities.HumanResource;

namespace Services.EfServices
{
    public class RoleService : GenericService<Role>, IRoleService
    {

        #region props
        private readonly IRolePermissionService _rolePermissionService;
        #endregion

        #region ctor

        public RoleService(
            IUnitOfWork uow, 
            IRolePermissionService rolePermissionService) 
            : base(uow)
        {
            _rolePermissionService = rolePermissionService;
        }

        #endregion


        #region custom role provider

        public string[] GetAllRoles()
        {
            return _dbSet.Select(c => c.IdentityName).ToList().ToArray(); // identityName use for custome role provider
        }

        public string[] GetUsersInRole(string roleName)
        {
            var query = (from r in _dbSet
                         join u in _uow.Set<User>() on r.Id equals u.RoleId
                         where
                                r.IdentityName == roleName // identityName use for custome role provider
                         select u.Username);

            return query.ToList().ToArray();
        }

        public bool RoleExists(string roleName)
        {
            return _dbSet.Any(c => c.IdentityName == roleName); // identityName use for custome role provider
        }

        #endregion


        public Role GetByRoleName(RoleName roleName)
        {
            return _dbSet.First(c => c.Name == roleName);
        }

        public List<Role> GetListByRoleNames(params RoleName[] roleNames)
        {
            return _dbSet
                .Where(c => roleNames.Contains(c.Name))
                .ToList();
        }

        public void Add(RoleCreateViewModel model)
        {
            var newRole = new Role
            {
                DisplayName = model.Name,
                Name = RoleName.Employees // users just can create new staff roles
            };

            _dbSet.Add(newRole);
            _rolePermissionService.Add(newRole.Id, model.PermissionIds);
        }

        public void Edit(Role model, long[] permissionIds)
        {
            var editingItem = Find(model.Id);

            if (editingItem.Name != RoleName.Administrators)
            {
                Edit(model);
                _rolePermissionService.Edit(model.Id, permissionIds);
            }
            // Nobody can edit administrators
        }

        public PagedList<Role> GetAllEditableRoles(int? pageNumber = 1, int? pageSize = 15)
        {
            return _dbSet
                .Where(c => c.Name != RoleName.Administrators)
                .OrderByDescending(c => c.Id)
                .ToPagedList(pageNumber.Value, pageSize.Value);
        }


    }
}
