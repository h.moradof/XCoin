﻿using DatabaseContext.Context;
using DomainModels.Entities.Trade;
using Services.Base;
using Services.Interfaces;
using System.Linq;
using System;
using System.Collections.Generic;
using ViewModels.Base;

namespace Services.EfServices
{
    public class CurrencyService : GenericService<Currency>, ICurrencyService
    {
        #region ctor
        public CurrencyService(IUnitOfWork uow) 
            : base(uow)
        {   }
        #endregion



        public Currency FindBySlug(string slug)
        {
            if (string.IsNullOrEmpty(slug)) { return null; }

            return _dbSet.FirstOrDefault(c => c.Slug == slug);
        }

        public List<BaseDdlBaseOnSlugViewModel> GetBaseOnSlugDdlList()
        {
            return _dbSet
                .Select(c => new BaseDdlBaseOnSlugViewModel
                {
                    Slug = c.Slug,
                    Name = c.Name
                }).ToList();
        }
    }
}
