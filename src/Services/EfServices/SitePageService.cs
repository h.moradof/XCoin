﻿using DatabaseContext.Context;
using DomainModels.Entities.Site;
using Services.Base;
using Services.Interfaces;
using System.Linq;

namespace Services.EfServices
{
    public class SitePageService : GenericService<SitePage>, ISitePageService
    {

        #region ctor

        public SitePageService(IUnitOfWork uow) 
            : base(uow)
        {  }

        #endregion


        // دریافت اطلاعات صفحه از طریق اسلاگ
        public SitePage FindBySlug(string slug)
        {
            if (string.IsNullOrEmpty(slug)) { return null; }

            return _dbSet.FirstOrDefault(c => c.Slug == slug);
        }

    }
}
