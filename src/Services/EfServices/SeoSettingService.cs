﻿using DatabaseContext.Context;
using DomainModels.Entities.Setting;
using Infrastructure.Cache.Interfaces;
using Services.Base;
using Services.Interfaces;
using System.Threading.Tasks;

namespace Services.EfServices
{
    public class SeoSettingService : GenericService<SeoSetting>, ISeoSettingService
    {
        #region props
        private readonly ISeoSettingCacheManager _siteSeoSettingCacheManager;
        #endregion


        #region ctor

        public SeoSettingService(IUnitOfWork uow, ISeoSettingCacheManager siteSeoSettingCacheManager) 
            : base(uow)
        {
            _siteSeoSettingCacheManager = siteSeoSettingCacheManager;
        }

        #endregion



        public override SeoSetting Edit(SeoSetting model)
        {
            var updatedItem = base.Edit(model);

            // refreshing cache
            _siteSeoSettingCacheManager.Cache(GetList());

            return updatedItem;
        }

        public override async Task<SeoSetting> EditAsync(SeoSetting model)
        {
            var updatedItem = await base.EditAsync(model);

            // refreshing cache
            _siteSeoSettingCacheManager.Cache(GetList());

            return updatedItem;
        }

    }
}
