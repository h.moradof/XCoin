﻿using DomainModels.Entities.Access;
using Services.Base;
using Services.Interfaces;
using DatabaseContext.Context;
using System;
using System.Linq;

namespace Services.EfServices
{
    public class RolePermissionService : GenericService<RolePermission>, IRolePermissionService
    {
        #region ctor
        public RolePermissionService(IUnitOfWork uow) 
            : base(uow)
        {  }
        #endregion

        public void Add(long roleId, long[] permissionIds)
        {
            foreach (var permissionId in permissionIds)
            {
                _dbSet.Add(new RolePermission { RoleId = roleId, PermissionId = permissionId });
            }

            var defaultPermissions = _uow.Set<Permission>().Where(c => c.IsDefault).ToList();

            foreach (var p in defaultPermissions)
            {
                _dbSet.Add(new RolePermission { RoleId = roleId, PermissionId = p.Id });
            }
        }

        public void Edit(long roleId, long[] permissionIds)
        {
            // cleaning
            _dbSet.
                Where(c => c.RoleId == roleId)
                .ToList()
                .ForEach(c => _dbSet.Remove(c));

            Add(roleId, permissionIds);
        }

        public long[] GetPermissionIdsOfRole(long roleId)
        {
            return _dbSet
                .Where(c => c.RoleId == roleId)
                .Select(c => c.PermissionId)
                .ToList()
                .ToArray();
        }
    }
}
