﻿using DatabaseContext.Context;
using DomainModels.Entities.Trade;
using Services.Interfaces;
using System.Collections.Generic;
using ViewModels.Trade;
using System.Linq;

namespace Services.EfServices
{
    public class UserBalanceService : IUserBalanceService
    {

        #region ctor

        private readonly IUnitOfWork _uow;

        public UserBalanceService(IUnitOfWork uow)
        {
            _uow = uow;
        }

        #endregion



        public List<UserBalanceDetailViewModel> GetUserBalances(long userId)
        {
            return (from ub in _uow.Set<UserBalance>()
                    join c in _uow.Set<Coin>() on ub.CoinId equals c.Id
                    where ub.UserId == userId
                    select new UserBalanceDetailViewModel { BalanceAmount = ub.Amount, CoinName = c.DisplayName })
                    .ToList();
        }
    }
}
