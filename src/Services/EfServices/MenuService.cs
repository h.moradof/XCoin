﻿using DatabaseContext.Context;
using Services.Interfaces;
using System;
using System.Data.SqlClient;
using System.Linq;

namespace Services.EfServices
{
    public class MenuService : IMenuService
    {

        #region props
        private readonly IUnitOfWork _uow;
        #endregion

        #region ctor
        public MenuService(IUnitOfWork uow)
        {
            _uow = uow;
        }
        #endregion



        public string GetPanelMenuHtml(long userId)
        {
            var menuHtml = _uow.Database.SqlQuery<string>("sp_GetPanelMenu @UserId", new SqlParameter("UserId", userId)).FirstOrDefault();

            return menuHtml;
        }


    }
}
