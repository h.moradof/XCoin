﻿using DomainModels.Entities.Trade;
using Services.Base;
using Services.Interfaces;
using DatabaseContext.Context;
using System.Linq;
using System;
using System.Collections.Generic;
using ViewModels.Base;

namespace Services.EfServices
{
    public class CountryService : GenericService<Country>, ICountryService
    {

        #region ctor
        public CountryService(IUnitOfWork uow) 
            : base(uow)
        { }
        #endregion


        public Country FindBySlug(string slug)
        {
            if (string.IsNullOrEmpty(slug)) { return null; }

            return _dbSet.FirstOrDefault(c => c.Slug == slug);
        }

        public List<BaseDdlBaseOnSlugViewModel> GetBaseOnSlugDdlList()
        {
            return _dbSet
                .Select(c => new BaseDdlBaseOnSlugViewModel
                {
                    Slug = c.Slug,
                    Name = c.Name
                }).ToList();
        }
    }
}
