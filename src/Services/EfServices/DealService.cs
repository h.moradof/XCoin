﻿using System;
using DatabaseContext.Context;
using DomainModels.Entities.Trade;
using Services.Base;
using Services.Interfaces;
using System.Linq;
using ViewModels.Trade;
using DomainModels.Entities.HumanResource;
using Infrastructure.Pagination;
using Infrastructure.Exceptions;
using System.Data.Entity;
using DomainModels.Entities.Support;
using System.Configuration;

namespace Services.EfServices
{
    public class DealService : GenericService<Deal>, IDealService
    {

        #region static fields

        static int ExpireDurationTimeInMinute = Convert.ToInt32(ConfigurationManager.AppSettings["ExpireDurationTimeInMinute"]);

        #endregion

        #region ctor
        public DealService(IUnitOfWork uow)
            : base(uow)
        {
        }
        #endregion


        public Deal Add(Offer offer)
        {
            var ads = _uow.Set<Ads>().First(c => c.Id == offer.AdsId);

            long buyerId = 0, salerId = 0;
            if (ads.IsSell)
            {
                salerId = ads.UserId;
                buyerId = offer.UserId;
            }
            else
            {
                buyerId = ads.UserId;
                salerId = offer.UserId;
            }

            var doingStatus = _uow.Set<DealStatus>().First(c => c.Name == DealStatusName.Doing);
            var coin = _uow.Set<Coin>().First(c => c.Id == ads.CoinId);

            var totalCurrencyPrice = offer.Amount * ads.CurrencyUnitPriceOfCoin;

            var feeAmount = (offer.Amount * coin.FeePercent);

            var buyerReceivedAmount = Math.Round(offer.Amount - feeAmount, 8); // rounding amount

            var newDeal = new Deal
            {
                FeePercent = coin.FeePercent,
                FeeAmount = feeAmount,
                BuyerReceivedAmount = buyerReceivedAmount,
                BuyerId = buyerId,
                SalerId = salerId,
                CoinId = ads.CoinId,
                CoinAmount = offer.Amount,
                CurrencyUnitPriceOfCoin = ads.CurrencyUnitPriceOfCoin,
                TotalCurrencyPrice = totalCurrencyPrice,
                DealStatus = doingStatus,
                CountryId = ads.CountryId,
                CurrencyId  = ads.CurrencyId,
                PaymentMethodId = ads.PaymentMethodId,
                IsBuyerPaidCurrency = false,
                IsSalerReleasedCoin = false,
                RowGuid = Guid.NewGuid(),
                CreatedOn = DateTime.Now
            };
            
            return base.Add(newDeal);
        }


        public Deal GetByRowGuid(Guid rowGuid)
        {
            var query = (from d in _dbSet
                         where d.RowGuid == rowGuid
                         select d);

            return query.First();
        }


        public DealDetailViewModel FindByRowGuid(Guid rowGuid)
        {
            var query = (from d in _dbSet
                         join co in _uow.Set<Coin>() on d.CoinId equals co.Id
                         join cu in _uow.Set<Currency>() on d.CurrencyId equals cu.Id
                         join cou in _uow.Set<Country>() on d.CountryId equals cou.Id
                         join p in _uow.Set<PaymentMethod>() on d.PaymentMethodId equals p.Id
                         join s in _uow.Set<DealStatus>() on d.DealStatusId equals s.Id
                         join buyer in _uow.Set<User>() on d.BuyerId equals buyer.Id
                         join saler in _uow.Set<User>() on d.SalerId equals saler.Id
                         where d.RowGuid == rowGuid
                         select new DealDetailViewModel
                         {
                             Deal = d,
                             Country = cou,
                             Coin = co,
                             Currency = cu,
                             PaymentMethod = p,
                             DealStatus = s,
                             Buyer = buyer,
                             Saler = saler
                         });

            return query.FirstOrDefault();
        }


        public PagedList<MemberDealsListViewModel> GetMemberDeals(long userId, long statusId, int pageNumber, int pageSize)
        {
            var query = (from d in _dbSet
                         join co in _uow.Set<Coin>() on d.CoinId equals co.Id
                         join cu in _uow.Set<Currency>() on d.CurrencyId equals cu.Id
                         join cou in _uow.Set<Country>() on d.CountryId equals cou.Id
                         join p in _uow.Set<PaymentMethod>() on d.PaymentMethodId equals p.Id
                         join s in _uow.Set<DealStatus>() on d.DealStatusId equals s.Id
                         join buyer in _uow.Set<User>() on d.BuyerId equals buyer.Id
                         join saler in _uow.Set<User>() on d.SalerId equals saler.Id
                         where (buyer.Id == userId || saler.Id == userId) && d.DealStatusId == statusId
                         orderby d.Id descending
                         select new MemberDealsListViewModel
                         {
                             Deal = d,
                             Country = cou,
                             Coin = co,
                             Currency = cu,
                             PaymentMethod = p,
                             DealStatus = s,
                             Buyer = buyer,
                             Saler = saler
                         });

            return query.ToPagedList(pageNumber, pageSize);

        }


        public NotificationsViewModel GetNofitications(long userId)
        {
            var vm = new NotificationsViewModel();

            var query = (from d in _dbSet
                         join s in _uow.Set<DealStatus>() on d.DealStatusId equals s.Id
                         where s.Name == DealStatusName.Doing && (d.BuyerId == userId || d.SalerId == userId)
                         select d);

            vm.NewDealsCount = query.Count();

            vm.TotalCount = vm.NewDealsCount; // for now

            return vm;
        }


        public void SalerReleaseCoin(Guid dealRowGuid, long salerId)
        {
            var d_c = (from d in _dbSet
                       join c in _uow.Set<Coin>() on d.CoinId equals c.Id
                       where d.RowGuid == dealRowGuid && d.SalerId == salerId
                       select new { Deal = d, Coin = c })
                       .First();

            if (d_c.Deal.IsSalerReleasedCoin)
            {
                throw new CustomException("Duplicate operation");
            }

            d_c.Deal.IsSalerReleasedCoin = true;

            // change balances
            var feePercent = d_c.Coin.FeePercent;
            var feeAmount = Math.Round(d_c.Deal.CoinAmount * feePercent, 8); // rounding amount

            var transferingBalance = Math.Round(d_c.Deal.CoinAmount - feeAmount, 8); // rounding amount

            // balances of this coin
            var salerBalance = _uow.Set<UserBalance>().First(c => c.UserId == d_c.Deal.SalerId && c.CoinId == d_c.Coin.Id);
            var buyerBalance = _uow.Set<UserBalance>().First(c => c.UserId == d_c.Deal.BuyerId && c.CoinId == d_c.Coin.Id);

            salerBalance.Amount -= d_c.Deal.CoinAmount;
            buyerBalance.Amount += transferingBalance;

            // update status
            d_c.Deal.DealStatus = _uow.Set<DealStatus>().First(c => c.Name == DealStatusName.Done);

            d_c.Deal.ReleasedDate = DateTime.Now;
        }


        public void BuyerPayCurrency(Guid dealRowGuid, long buyerId)
        {
            var now = DateTime.Now;

            var result = _dbSet
                .Where(c => c.RowGuid == dealRowGuid && c.BuyerId == buyerId)
                .Select(d => new
                {
                    Deal = d,
                    IsBuyerHasTimeToPay = (now <= DbFunctions.AddMinutes(d.CreatedOn, ExpireDurationTimeInMinute))
                })
                .First();

            if (result.Deal.IsBuyerPaidCurrency)
            {
                throw new CustomException("Duplicate operation");
            }

            if (!result.IsBuyerHasTimeToPay)
            {
                throw new CustomException("You do not any time to pay");
            }

            result.Deal.IsBuyerPaidCurrency = true;
            result.Deal.PaidDate = DateTime.Now;
        }


        public DealStateViewModel GetDealState(Guid dealRowGuid, long userId)
        {
            var now = DateTime.Now;

            var query = (   from d in _dbSet
                            join saler in _uow.Set<User>() on d.SalerId equals saler.Id
                            join buyer in _uow.Set<User>() on d.BuyerId equals buyer.Id
                            join s in _uow.Set<DealStatus>() on d.DealStatusId equals s.Id
                            join c in _uow.Set<Coin>() on d.CoinId equals c.Id
                            join cu in _uow.Set<Currency>() on d.CurrencyId equals cu.Id
                            join p in _uow.Set<PaymentMethod>() on d.PaymentMethodId equals p.Id
                            where d.RowGuid == dealRowGuid && (d.SalerId == userId || d.BuyerId == userId)
                            select new DealStateViewModel
                            {
                                CoinName = c.DisplayName,
                                CurrencyName = cu.Name,
                                PaymentMethod = p.EnName,
                                CoinAmount = d.CoinAmount,
                                TotalCurrencyPrice = d.TotalCurrencyPrice,
                                IsBuyer = (d.BuyerId == userId),
                                IsSaler = (d.SalerId == userId),
                                IsBuyerPaidCurrency = d.IsBuyerPaidCurrency,
                                IsSalerReleasedCoin = d.IsSalerReleasedCoin,
                                StatusName = s.Name.ToString(),
                                StatusDisplayName = s.DisplayName,
                                IsBuyerHasTimeToPay = (now <= DbFunctions.AddMinutes(d.CreatedOn, ExpireDurationTimeInMinute)),
                                BuyerPayEndTime = DbFunctions.AddMinutes(d.CreatedOn, ExpireDurationTimeInMinute).Value,
                                SalerUsername = saler.Username,
                                BuyerUsername = buyer.Username,
                                BuyerRemainTimeToPayInMinute = (now <= DbFunctions.AddMinutes(d.CreatedOn, ExpireDurationTimeInMinute)) ? DbFunctions.DiffMinutes(now, DbFunctions.AddMinutes(d.CreatedOn, ExpireDurationTimeInMinute)).Value : 0,
                                CreatedOn = d.CreatedOn,
                                PaidDate = d.PaidDate,
                                ReleasedDate = d.ReleasedDate,
                                JudgeTicketRowGuid = d.JudgeTicketRowGuid
                            });

            var result = query.First();

            return result;
        }


        public void CancelingDeal(Guid rowGuid, long userId)
        {
            var d_s = (from d in _dbSet
                       join s in _uow.Set<DealStatus>() on d.DealStatusId equals s.Id
                       where d.RowGuid == rowGuid && d.BuyerId == userId
                       select new { Deal = d, Status = s }).First();
            
            if (d_s.Status.Name != DealStatusName.Doing) { throw new CustomException("You can't cancel the deal"); }

            // update status
            d_s.Deal.DealStatus = _uow.Set<DealStatus>().First(c => c.Name == DealStatusName.Canceled);
        }


        public void JudgeDeal(Guid rowGuid, long userId)
        {
            var d_s = (from d in _dbSet
                       join buyer in _uow.Set<User>() on d.BuyerId equals buyer.Id
                       join saler in _uow.Set<User>() on d.SalerId equals saler.Id
                       join s in _uow.Set<DealStatus>() on d.DealStatusId equals s.Id
                       where d.RowGuid == rowGuid && (d.BuyerId == userId || d.SalerId == userId)
                       select new { Deal = d, Status = s, Buyer = buyer, Saler = saler }).First();

            if (d_s.Status.Name != DealStatusName.Doing) { throw new CustomException("You can't judge the deal"); }

            var judgeRequester = 
                (d_s.Buyer.Id == userId) ? 
                    "'" + d_s.Buyer.Username + "' (buyer)" 
                    : 
                    "'" + d_s.Saler.Username + "' (saler)";

            // create judge ticket
            var ticketGuid = Guid.NewGuid();
            _uow.Set<Ticket>().Add(new Ticket
            {
                Subject = "Judge request for deal #" + rowGuid + " from " + judgeRequester,
                Message = judgeRequester +" sent judge request",
                RowGuid = ticketGuid,
                SenderId = userId,
                IsOpen = true,
                CreatedOn = DateTime.Now
            });

            // update status
            d_s.Deal.DealStatus = _uow.Set<DealStatus>().First(c => c.Name == DealStatusName.Judge);
            d_s.Deal.JudgeTicketRowGuid = ticketGuid;
        }

        public void CancelNotPaidDeals()
        {
            var now = DateTime.Now;

            var notPaidDeals = (from d in _dbSet
                                join s in _uow.Set<DealStatus>() on d.DealStatusId equals s.Id
                                join saler in _uow.Set<User>() on d.SalerId equals saler.Id
                                join salerBalance in _uow.Set<UserBalance>() on new { SalerId = saler.Id, CoinId = d.CoinId } equals new { SalerId = salerBalance.UserId, CoinId = salerBalance.CoinId }
                                where 
                                    s.Name == DealStatusName.Doing &&
                                    !d.IsBuyerPaidCurrency && // buyer don't pay currency
                                    (now > DbFunctions.AddMinutes(d.CreatedOn, ExpireDurationTimeInMinute)) // pay time finished
                                select new { Deal = d, SalerBalance = salerBalance })
                                .ToList();

            foreach (var notPaidDeal in notPaidDeals)
            {
                // un freeze (return) saler balance
                notPaidDeal.SalerBalance.Amount += notPaidDeal.Deal.CoinAmount;

                // update status
                notPaidDeal.Deal.DealStatus = _uow.Set<DealStatus>().First(c => c.Name == DealStatusName.Canceled);
            }
        }
    }
}
