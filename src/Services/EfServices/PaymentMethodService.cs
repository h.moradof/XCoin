﻿using DomainModels.Entities.Trade;
using Services.Base;
using Services.Interfaces;
using DatabaseContext.Context;
using System.Linq;
using System;
using System.Collections.Generic;
using ViewModels.Base;

namespace Services.EfServices
{
    public class PaymentMethodService : GenericService<PaymentMethod>, IPaymentMethodService
    {
        #region ctor
        public PaymentMethodService(IUnitOfWork uow) 
            : base(uow)
        {  }
        #endregion



        public PaymentMethod FindBySlug(string slug)
        {
            if (string.IsNullOrEmpty(slug)) { return null; }

            return _dbSet.FirstOrDefault(c => c.Slug  == slug);
        }

        public List<BaseDdlBaseOnSlugViewModel> GetBaseOnSlugDdlList(string lang)
        {
            return _dbSet
                .Select(c => new BaseDdlBaseOnSlugViewModel
                {
                    Slug = c.Slug,
                    Name = (lang == "en") ? c.EnName : c.ZhName
                }).ToList();
        }
    }
}
