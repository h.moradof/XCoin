﻿using System;
using DatabaseContext.Context;
using DomainModels.Entities.Trade;
using Services.Base;
using Services.Interfaces;
using System.Linq;
using System.Collections.Generic;
using Infrastructure.Exceptions;
using ViewModels.Trade;
using DomainModels.Entities.HumanResource;

namespace Services.EfServices
{
    public class ChatService : GenericService<Chat>, IChatService
    {

        #region ctor
        public ChatService(IUnitOfWork uow) 
            : base(uow)
        {
        }
        #endregion


        public Chat Add(string message, long userId, Guid dealRoWGuid)
        {
            if (string.IsNullOrEmpty(message)) { throw new CustomException("Please write a message"); }
            var d_s =  (from d in _uow.Set<Deal>()
                        join s in _uow.Set<DealStatus>() on d.DealStatusId equals s.Id
                        where d.RowGuid == dealRoWGuid
                        select new { d,s }).FirstOrDefault();
            
            if (d_s == null) { throw new ArgumentException("dealRoWGuid is invalid"); }

            if (d_s.s.Name != DealStatusName.Doing)
            {
                throw new CustomException("This deal is not in progress, so you can't chat anymore");
            }

            return base.Add(new Chat
            {
                DealId = d_s.d.Id,
                Message = message,
                SenderId = userId,
                CreatedOn = DateTime.Now
            });
        }

        public List<ChatListViewModel> GetMessages(long userId, Guid dealId)
        {
            return (from ch in _dbSet
                    join d in _uow.Set<Deal>() on ch.DealId equals d.Id
                    join u in _uow.Set<User>() on ch.SenderId equals u.Id
                    where d.RowGuid == dealId && (d.SalerId == userId || d.BuyerId == userId)
                    select new ChatListViewModel
                    {
                        Chat = ch,
                        SenderFullName = u.Username
                    }).ToList();
        }
    }
}
