﻿using NUnit.Framework;
using Infrastructure.Security;
using System;

namespace Infrastruture.Test.Security
{
    [TestFixture]
    public class DataEncryptorTests
    {
        [Test]
        public void AES_Encryptor_Should_Encrypt_And_Decrypt_Correctly()
        {
            var data = "7e15";

            var encryptedData = DataEncryptor.EncryptTpl(data);
            var decryptedData = DataEncryptor.DecryptTpl(encryptedData);

            Assert.AreEqual(data, decryptedData);
        }




    }
}
